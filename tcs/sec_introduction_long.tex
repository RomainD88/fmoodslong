\section{Introduction}

%\paragraph{Monitoring asynchronous networks}
One of the main engineering challenges for distributed systems is the
comprehensive verification of distributed software without relying on
ad-hoc and expensive testing techniques.
%
Multiparty session types (MPST) is a typing discipline for
communication programming, originally developed in the $\pi$-calculus~
\cite{HYC08,BettiniCDLDY08,BHTY10,DY11,DY12,tcc:thesis} towards tackling
this challenge.
%%%%%%%%%%%%%%%%%%%
The idea is that applications are built starting from units of design
called sessions.  Each type of session, involving multiple roles, is
first modelled from a global perspective ({\em global type}) and then
projected onto {\em local types}, one for each role involved. As a
verification method, the existing MPST systems focus on static type
checking of endpoint processes against local types. The standard
properties enjoyed by well-typed processes are communication safety
(all processes conform to globally agreed communication protocols) and
freedom from deadlocks.

The direct application of the theoretical MPST techniques to the
current practice, however, presents a few obstacles.  Firstly, the
existing type systems are targeted at calculi with first class
primitives for linear communication channels and
communication-oriented control flow; the majority of mainstream
engineering languages would need to be extended in this sense to be
suitable for syntactic session type checking.  Unfortunately, it is
not always straightforward to add these features to the specific host
languages (e.g. linear resource typing for a very liberal language
like C).  Furthermore, the executable processes in a distributed
system may be implemented in different languages.  Secondly, for
domains where dynamically typed or untyped languages are popular
(e.g., Web programming), or in multi-organizational scenarios, the
introduction of static typing infrastructure to support MPST may not
be realistic.

This paper proposes a theoretical system addressing the above issues 
by enabling both static \emph{and} dynamic verification of
communicating processes. 
% to be formally combined in one framework.  
 The aim is to capture the decentralised nature of distributed application
development, providing better support for heterogeneous distributed
systems by allowing components to be independently implemented, using
different languages, libraries and programming techniques, as well as
being independently verified, either statically or dynamically, while
retaining the strong global safety properties of statically verified
homogeneous systems.

This work is motivated in part by our ongoing collaboration
%\footnote{\url{https://confluence.oceanobservatories.org/display/
%CIDev/Conversations+and+Commitments}} 
with the Ocean Observatories Initiative (OOI) \cite{OOI}, a project to
establish cyberinfrastructure for the delivery, management and
analysis of scientific data from a large network of ocean sensor
systems.  Their architecture relies on the combination of high-level
protocol specifications (to express how the infrastructure services
should be used) and distributed run-time monitoring to regulate the
behaviour of third-party applications in the system. 


\paragraph{A formal theory for static/dynamic verification}
Our framework is based on the idea that,
if each endpoint is \emph{independently} verified (statically or dynamically) to
conform to their local protocols, then the global protocol is respected \emph{as a whole}. 
To this goal, we propose
a new formal model and bisimulation theories of heterogeneous networks of monitored
and unmonitored
processes. 
% Compared to \cite{TGC11}, where all local processes are
%dynamically verified through the protections of system monitors,
%% and thus the global safety and session fidelity of a network are dynamically ensured,
%this paper integrates statically and dynamically verified local processes into one network,
%and formally states the theorems of the combination.
%%%%%%%%%%%%%%%%%%%%%%
%Our theory captures the notions of distributed components
%and decentralised dynamic verification centring on session types.

For the first time, we make explicit the \emph{routing mechanism}
implicitly present inside the MPST framework: in a session, messages
are sent to abstract roles (e.g.\ to a Seller) and the router, a
dynamically updated component of the network, translates these roles
into actual addresses. 

By taking this feature into account when designing novel equivalences,
%for the first time taking into account the routing mechanism, 
our formal model can relate networks built in different ways (through
different distributions or relocations of services) but offering the
same \emph{interface} to an external observer. The router, being in
charge of associating roles with principals, hides to an external user
the internal composition of a network: what distinguishes two networks
is not their structure but the services they are able to perform, or
more precisely, the local types they offer to the outside.

We formally define a satisfaction relation to express when the
behaviour of a network conforms to a global specification and we prove
a number of properties of our model. {\em Local safety} states that a
monitored process respects its local protocol, i.e.\ that dynamic
verification by monitoring is sound, while {\em local transparency}
states that a monitored process has equivalent behaviour to an
unmonitored but well-behaved process, e.g.~statically verified against
the same local protocol.  {\em Global safety} states that a system
satisfies the global protocol, provided that each participant behaves
as if monitored, {while {\em global transparency} states that a fully
  monitored network has equivalent behaviour to an unmonitored but
  well-behaved network, i.e. in which all local processes are
  well-behaved against the same local protocols.  {\em Session
    fidelity} states that, as all message flows of a network satisfy
  global specifications, whenever the network changes because some
  local processes take actions, all message flows continue to satisfy
  global specifications.}  Together, these properties justify our
framework for decentralised verification by allowing monitored and
unmonitored processes to be safely mixed while preserving protocol
conformance for the entire network.
%%%%%%%%%%%%%%%%%%%%%%
Technically, these properties also ensure the coherence of our theory,
by relating the satisfaction relations with the semantics and static
validation procedures.
%, and support our implementation, by
%giving a strong formal basis to
%%strengthening the formal basis of
%the Scribble suite.

%%In this way, our framework is able assure global communication
%%safety for the system through a combination of static and dynamic
%%verification.
%...This is possible because the local protocols, used to flexibly guide and verify diverse implementations, are derived from the
%%uniform,
%central abstraction of the global protocol by projection that decomposes while preserving safety...
%%
%%- any strong statement about the system not being "corrupted" if a monitored process does something bad?
%%- monitor is not part of the runtime, it's an external ("asynchronous") monitor---we didn't really motivate *asynchronous* monitors for dynamic typing, only motivated dynamic typing

%The formal model provides a compact and viable reference semantics for
%implementations of our framework. Our implementation uses the Scribble
%protocol language \cite{scribble}, an engineering language for protocol
%specification based on MPST theory, with compatible session
%programming APIs for Java and Python. Scribble supports the
%description of interaction between session participants through
%asynchronous message passing sequences, protocol branches and
%recursion, and also logic-based annotations for fine-grained
%behavioural constraints \cite{BHTY10}, such as assertions on message
%values.  Our current session endpoint monitor implementation, written
%in Python, supports all of the above features. Our monitor projects
%the Scribble global protocol for the endpoint being monitored, and
%generates an FSM from the local protocol to verify the observed
%message trace. Performance benchmarks show that sessions can be
%dynamically verified by monitoring with little overhead.


\vspace{.5ex}
\paragraph{Paper summary and contributions}
\REF{sec:formal} introduces the formalisms for protocol specifications (\REF{sec:specification}) and networks (\REF{sec:calculus}) used to provide a formal framework for monitored networks based on $\pi$-calculus processes and protocol-based
run-time enforcement through monitors.
 %
\REF{sec:monitoring} introduces: a semantics for specifications (\REF{sec:envs}), 
a novel behavioural theory for compositional reasoning over monitored networks 
through the use of equivalences (bisimilarity and barbed congruence) and 
the satisfaction relation (\REF{sec:RT}). 
%
\REF{sub:sessionfidelityandsafety} establishes key properties of monitored networks,
namely local/global safety, transparency, and session fidelity.
%
\LB{We discuss future and related work in \REF{sec:conclusion}. 
The proofs can be found in the Appendices.}

%the Appendices~\REF{app:safety} and
%We have also implemented existing Web service use cases from~\cite{Jboss}
%to demonstrate the expressiveness of Scribble.
%These use cases and the full source code with benchmark results
%are also available from \cite{SeTMon}.

\begin{comment}
Starting from a formal global protocol
specification through MPST as the central abstraction, localised behaviour
specifications for each endpoint are mechanically derived, which may be
used to statically verify each endpoint as well as assisting the
programming task. We believe that decentralisation of the implementation
\emph{and} verification concerns is necessary for practical distributed
system development, as a distributed system is often characterised
not only
by the
%% decomposition
distribution of its constituent parts, but also by the heterogeneous
nature of those parts. As such, our proposed framework is designed to
promote \emph{independent} verification of each component. This means
that each component may be independently implemented in different
languages using different libraries and programming techniques,
as well as getting verified,
while retaining
global safety % and deadlock freedom
properties (see \REF{sub:sessionfidelityandsafety}). %% Moreover,
Independent verification allows different verification techniques,
both static and dynamic,
to be applied to each component, while
uniformly
assuring
safety properties for a system as a whole
(formally demonstrated through Theorem~\ref{ABCAAA}.\ref{item:prop:deux} later).

Starting from a formal global protocol specification through MPST as
the central abstraction, localised behaviour specifications for each
endpoint are mechanically derived, which may be used to statically
verify each endpoint independently as well as assisting the
programming task. The term distributed system is often understood to
mean a collaborative execution between separately located components.
a rigorous assurance framework for distributed systems must also
support independent verification... Decentralisation of the
development process and assurance for distributed systems must also
%
%In other words, the distributed system is characterised not only by the notion of component locality, but by the decentralisation of the system specification and implementation process while preserving the strong safety guarantees afforded by centralised static verification techniques. Safe decomposition in this manner is particularly important in distributed systems implementation, where the behaviour of the system as a whole crucially depends on the correct interaction between \emph{heterogeneous} components. ...Heterogeneity arises naturally in distributed systems in many ways...dynamic verification is best for distributed systems in many ways...
%
%For these reasons, we believe that an assurance framework for distributed application development must support component heterogeneity as a fundamental feature. This is the motivation for dynamic verification.
\end{comment}


\begin{comment}
Comprehensive verification of decentralised distributed systems,
i.e. systems where several units of computation communicate with each
other without relying on central coordination, is both a major
direction in information technology, with many direct examples in web
services~\cite{...}, manycores~\cite{FOS}, arrays of
sensors~\cite{OOI} and a challenging domain in computer
science. Indeed, it may seem \emph{a priori} difficult to ensure
methodically that a whole network of applications or services, which
may be written in different languages and running at different places
on different hardware, perform as described in a global specification
when the only way of controlling interactions is at the local
processes. Key properties of such networks are often associated with
arbitrarily complex interaction scenarios, which are hard to enforce
for general applications since these scenarios are formally
represented (if at all) only at the design stage in the form of
e.g. sequence diagrams.  Even though several existing works do exist,
they do not offer a comprehensive framework
for %% capturing, for example,
formal assurance of safety properties for a wide range of interaction
scenarios.  We need a formal foundation which underpins a general
top-down framework which can formally assure a general class of
properties for a wide range of applications.

% Something as simple as ensuring that one
% shopper using an online market received the required clearance from
% his bank before making a purchase is challenging if we are to
% capture arbitrary such scenarios.
% We want s generally known as {\em choreography
% Most existing solutions~\cite{...} have relied on
% \emph{ad hoc} mechanisms, designed toward specific, short-sighted
% goals. Hopefully, general top-down methods have been introduced.

\paragraph{Session Types} In the past ten years, the theory of
\emph{multi-party session-types} (MPST)~\cite{HYC08,BettiniCDLDY08}
has proposed a unified framework for decentralised protocol
design. Interactions are considered through the point of view of
\emph{sessions} seen as unit of conversation. Agents in the networks,
called \emph{principals}, engage in multiple sessions, in which they
play different \emph{roles}. Figure~\ref{...} gives insight at the way
of proceeding: one starts by designing a global specification, called
\emph{global type} for a session by describing end-to-end
interactions, control flow (such as parallel execution, loop, choice)
as well as governance properties (such as required conditions to
perform a given action). An automated projection procedure split this
global type into several \emph{local types}, such that conformance by
all principals to the local types of the corresponding roles implies
the conformance of the whole network to the initial global
specification. There exist two ways to verify local conformance:
either by dynamically monitoring programs, using an external entity
which checks and approves messages sent to and by the principal, or by
statically checking the program against the local type. We propose in
this paper a new formal theory integrating monitors with
inside the session-type theory.

\paragraph{Safety Assurance Framework} Many recent
works~\cite{...} address static verification of programs (often abstracted in
the formal languages of $\pi$-calculi) with respect to session-type
specifications. We will not treat it in this paper, but will instead
focus on two intertwined axes: the theoretical formalisation of
networks, with monitored and unmonitored principals, asynchronous
global transports and routing operators and the elaboration of a
general, implementation-independent method for protocol design. The
latter includes the introduction of a protocol language
\emph{Scribble}, which allows one to write complex specifications in a
natural way. We provide tools performing projection to local types,
and automatic generation of monitors for verification of programs.
Our aim is to provide better support for heterogeneous distributed
systems by allowing each component to be independently implemented,
using different languages, libraries and programming techniques, as
well as being independently verified, either statically or
dynamically, while retaining the strong global safety expected of a
statically verified, homogenous system.  In Figure~\cite{...}, the
right-hand process is implemented in a language that supports static
session typing~\cite{HYC08}, and we can assume conformance to its
local protocol is verified this way. The other two processes, however,
are implemented in standard Java and Python using simple session
programming APIs (see \REF{sec:design} for an example) and may not
be amenable to static typing. To ensure that these processes also
conform to the intended protocol and to protect the successfully
verified portion of the system, our framework uses network monitors to
dynamically verify their participation in the session. The monitors
are positioned at the network access points where all incoming and
outgoing messages to these processes can be observed and appropriate
actions can be taken to handle bad behaviour.

\paragraph{Integrated Theory} We present in this paper a theory which
capture the notion of distributed components and decentralised dynamic
verification centring on session types, with formal operational
semantics for programs, monitors, networks and specifications, type
systems, satisfaction relations and behavioural equivalences, as a
variant of the $\pi$-calculus, where routing and asynchronous
transport of messages are made explicit. Satisfactions relations allow
one to express formally when a whole asynchronous network actually
conforms to a global specification, equivalences give ways to equate
networks that are built differently, but offer the same services. We
prove in the following several properties: local safety states that a
monitored process respects its local protocol, i.e.\ that dynamic
verification by monitoring is sound, while local transparency states
that a monitored process has equivalent behaviour to an unmonitored
but well-behaved process, e.g.\ statically verified against the same
local protocol. Global safety states that a system satisfies the
global protocol provided that each each participant behaves as if
monitored. These properties both ensure coherence of our theory,
relating satisfaction relations and equivalences with the semantics
and the static validation procedure, and justify our implementation,
by giving strong formal basis to the Scribble suite.

\paragraph{OOI}

This work is motivated in part by our ongoing collaboration with the
Ocean Observatories Initiative (OOI) \cite{OOI}, a project to
establish cyberinfrastructure for the delivery, management and
analysis of scientific data from a large network of ocean sensor
systems. The architecture relies on the combination of high-level
conversation specifications (to express how the infrastructure
services should be used) and distributed run-time monitoring to
regulate the behaviour of third-party applications within the
system. A formally founded, distributed run-time monitoring mechanism
centred on conversations is needed for the system to reach its
requirements in terms of safety (conversation specifications are
accurately enforced), longevity (conversations are written down and
just have to be followed) and scalability (the enforcement is
decentralised). The theory presented in the paper offers a foundation
for its architecture, including an architectural framework and its
semantic properties, as well as monitoring and other algorithms which
are provably correct.  The paper shows how distributed applications of
OOI, implemented by a dynamically typed language Python, can be
runtime verified through a prototype monitoring architecture built
on the basis of the theory.

% Hence dynamic verification of communication sessions through
% network monitoring is essential for scalable means of integrating our
% framework into their infrastructure.


\paragraph{Contributions}

We summarise the technical contributions of this paper:

\begin{description}
\item[\REF{sec:examples}]
	illustrates the new safety assurance framework for distributed
applications and the underlying development framework,
illustrated through motivating examples. The framework
uses the Scribble
protocol language \cite{scribble}, an engineering language for protocol
specification based on MPST theory, that can then be
used for static or dynamic verification. The example shows how
we can use Scribble for protocol specifications and how we can
program using protocols and conversations.

\item[\REF{sec:dynamicmonitoring}]
	The design of a run-time verification and enforcement system
through a monitoring mechanism which efficiently enforces
behavioural conformance, including the satisfiability of
logical constraints over the content of the exchanged messages.

\item[\REF{sec:formal},\ref{sec:monitoring}] The formal foundation of
  the proposed framework given as a theory of the $\pi$-calculus with
  protocol-based run-time enforcement through monitors. We establish
  key properties of monitored networks, including local/global safety
  and transparency wrt given specifications. In particular, we
  introduce behavioural equivalences theory that offer a
  compositional reasoning technique over monitored networks.

\item[\REF{sec:prototype}]
An implementation of the proposed safety assurance framework,
and an evaluation of its practicality by benchmark results.
\end{description}

We discuss related work in \REF{sec:related} and future work in
\REF{sec:conclusion}.  We have implemented existing Web service use
cases from, e.g.~\cite{...} to demonstrate the expressiveness of
Scribble.
These use cases and the full source code with benchmark results are
available from \cite{SeTMon}.
The full proofs and more examples and related work can be found in
the full version.


===================================
\end{comment}


