\subsection{Network Satisfaction and Equivalences}
\label{sec:theory}

Based on the formal representations of monitored networks, we now
introduce the key formal tools for analysing their behaviour. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%We use equivalences to semantically compare networks. 
Concretely, we introduce two different equivalences to semantically compare networks: {\em bisimulation} and {\em barbed congruence} (the latter relying on the notion of {\em interface}, also given in this section). The {two} equivalences allow us to
compare networks using different granularities. On the one side,
bisimilarity addresses mainly partial networks, and gives an
equivalence that distinguishes two networks containing different
components. On the other side, (barbed) congruence addresses networks
(including global transport) from the point of view of an external
observer; thus, two networks built from different components but offering
the same service will be equated.  \RD{We choose to give two
  equivalences in order to give the theory a way to compare
  session networks from two different points of views: bisimulation
  allows designers to equate networks whose structures are similar,
  whereas barbed congruence allows users to equate networks, seen
  as black boxes, which provide the same service.}
Both equivalences are compositional, as proved in Proposition~\ref{prop:sim:cong}.
%%%
Finally, using the definition of congruence, we define {\em the
  satisfaction relation} $\models \NLNWgood{N}{\mathsf{M}}$, used
in~\S\ref{sec:safety} and~\S\ref{sec:fidelity} to prove the properties
of our framework.


\paragraph{\bf Bisimulations}

We first define semantics for networks of components, or partial
networks, on which we define bisimulation: we use $\sN,\sN',...$ for a
\emph{partial network}, that is a network without a
global transport\LB{, hence allowing the global observation of
  interactions.}  The labelled transition relation for processes and
partial networks $\sN$ is defined in
Fig.~\ref{fig:LTS:network:unlinked}.
%\LB{below.}  
\input{fig_transition_network_unlinked}
%
%by Fig. \ref{fig:LTS:network:unlinked}. 
%
%We omit the parallel rule for networks.
% which follows the standard rule for processes, based on an action of a
% single party (we have no rule which induces $\tau$-action from
% complementary actions).
% In the last line,
In \srule{ctx},
$\n{\ell}$ indicates the names occurring in $\ell$ while
$\bn{\EConly}$ indicates binding induced by $\EConly$.
% In \srule{bout}, $\ell^\uparrow$ indicatesw
% $\ell$ is an output.
In \srule{res}, $\sbj{\ell}$ denotes the subject of $\ell$. \LB{In
  $\srule{tau}$ the axiom is obtained either from the reduction rules
  for dynamic networks given in \S~\ref{sec:calculus} (only those not
  involving the global transport), or from the corresponding rules for
  monitored networks (which have been omitted in \S~\ref{sec:RT}).}

Hereafter we write  $\TAUTRANS{}$ for $\TRANS{\tau}^\ast$, $\TAUTRANS{\ell}$ for $\TAUTRANS{} \TRANS{\ell}
\TAUTRANS{}$, and $\TAUTRANS{\hat{\ell}}$ for $\TAUTRANS{}$ if $\ell =
\tau$ and $\TAUTRANS{\ell}$ otherwise. 

% For capturing the effect of hiding,
% we also use
% %% in order to define powerful equivalences, we require restriction,
% the
% operator to {\em delete $a$ from $\ell$} as: ${\keyword{new}(s)\{a_i:
%      \LA_i[\p_i]\}_{i\in I}}\setminus a_j = {\keyword{new}(s)\{a_i:
%      \LA_i[\p_i]\}_{i\in I\setminus j}}$ and otherwise $\ell\setminus a
% = \ell$.
% Since we do not include shared channel in values to be passed in
% communications (though we could have with simple extension of the theory),
% and because,
% in \srule{news},
% the action
% $\newsLabel{a_i\!}{\!\LA_i}{\p_i}{i\in I}$
% makes $s$ free (intuitively we can consider this rule
% registers $s$ in the global transport),
% we do not need the standard bound output rule.

\begin{DEF}[Bisimulation over partial networks] \label{def:sim:WB}
  A binary relation $\CAL{R}$ over partial networks is a {\em weak
    bisimulation} when $\sN_1 \CAL{R} \sN_2$ implies: whenever
  $\sN_1\TRANS{\ell}\sN'_1$ such that
  $\bn{\ell}\cap\fn{\sN_2}=\emptyset$, we have
  $\sN_2\TAUTRANS{\hat{\ell}}\sN'_2$ such that
  ${\sN'_1}\CAL{R}{\sN'_2}$, and the symmetric case.  We write
  $\sN_1\WB \sN_2$ if $(\sN_1,\sN_2)$ are in a weak bisimulation.
\end{DEF}

%%%%% comment by TZU 7/7 2012 you can recover if you want %%%
{
%%\COMMENT{
\paragraph{\bf Interface}
As stated above, we build another model where two different
implementations of the same service are equated.  Bisimilarity is too
strong for this aim as shown in further Example~\ref{sec:equi_ex}.  We
therefore introduce a contextual congruence (barbed reduction-closed
congruence \cite{HondaKYoshida95}) $\cong$ for networks.  Intuitively,
two networks are barbed-congruent when they are indistinguishable for
any principal that connects to them. In this case we say that the two (barbed-congruent) networks propose
the same \emph{interface} to the exterior. More precisely, two networks are
related with $\cong$ when, composed with the same third network, they
offer the same \emph{barbs} (i.e., the messages to external principals in
the respective global queues are on the same channels), and this
property is preserved under reduction.  

We say that a message $m$ is {\em routed for $\alpha$ in $\dN$} if
$\dN=(\nu \VEC{n})(\sN_0\mid \GQ{\RInfo}{\GQueue})$, $m \in \GQueue$,
either $m = \AOUTPUT{a}{s}{\rr}{\LA}{}$ and $\RInfo(a)=\alpha$ or
$m = \SELECTdef{s}{\rro}{\rrt}{l}{e}$ and $\RInfo(s[\rrt])=\alpha$.
\begin{DEF}[Barb]
  We write $\dN\downarrow_a$ when the global queue of $\dN$
  contains a message $m$ to free $a$, and $m$ is routed for a
  principal not in $\dN$. We write $\dN\Downarrow_a$
  if $\dN\RED^\ast \dN'\downarrow_a$.
\end{DEF}

\NI
We denote $\principals{\dN}$  for
the set of principals in $\dN$, that is 
%i.e.~$\principals{[\PRG_1]_{\alpha_1} | \cdots | [\PRG_n]_{\alpha_n}} =\ASET{\alpha_1,...,\alpha_n}$.
$\principals{\prod [\PRG_i]_{\alpha_i}}
=\ASET{\alpha_1,...,\alpha_n}$.  We say $\dN_1$ and $\dN_2$ are {\em
  composable} when
$\principals{\dN_1}\cap\principals{\dN_2}=\emptyset$, the union of
their routing tables remains a function, and their free session names
are disjoint.  If $\dN_1$ and $\dN_2$ are composable, we define
$\dN_1\diamond\dN_2 = (\nu \VEC{n}_1,\VEC{n}_2)(\sN_1 \PAR \sN_2 \PAR
\GQ{\RInfo_1\cup \RInfo_2}{\GQueue_1\cdot\GQueue_2})$ where $N_i=(\nu
\VEC{n}_i)(\sN_i\PAR \GQ{\RInfo_i}{\GQueue_i})$ ($i=1,2$).
%composition (queues are simply concatenated noting they are
%permutable, thanks to the disjointness of sessions).

\begin{DEF}[Barbed reduction-closed congruence]\label{def:sim:cong}
A relation $\CAL{R}$ on networks with the same principals is a {\em
  barbed r.c.\ congruence} \cite{HondaKYoshida95} if the following
holds: whenever $\dN_1\CAL{R}\dN_2$ we have: (1) for each composable
$N$, $\dN\diamond\dN_1\CAL{R}\dN\diamond\dN_2$; (2) $\dN_1\RED\dN'_1$
implies $\dN_2\RED^\ast\dN'_2$ s.t. $\dN'_1\CAL{R}\dN'_2$ again, and
the symmetric case; (3) $\dN'_1\Downarrow_a$ iff $\dN'_2\Downarrow_a$.
We write $\dN_1\cong\dN_2$ when $\dN_1$ and $\dN_2$ are related by a barbed
r.c. congruence.
\end{DEF}

%\NI The bisimulation is compositional.

\paragraph{\bf Properties}

The following result states that composing two bisimilar partial
networks with the same network -- implying the same router and global
transport -- yields two undistinguishable networks.

\begin{PRO}[Congruency]\label{prop:sim:cong}
If $\sN_1\WB \sN_2$, then {\rm (1)} $\sN_1|\sN\WB \sN_2|\sN$ for each
composable partial network $\sN$; and {\rm (2)} $\sN_1|\dN\cong \sN_2|\dN$ for
each composable network $\dN$.
\end{PRO}
} %%%%%%%%%%%%% end the comment 7/7 2012
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{proof}
  \NI For (1) we show that the relation
\small  $$
  \CAL{R} 
  = 
  \{
  (\sN_1 | \sN,~\sN_2 | \sN)
  ~
  | 
  ~  \sN_1 \WB \sN_2,~
  \text{$\sN$ composable with $\sN_1$ and $\sN_2$}
  \}
  $$ 
\normalsize  is a bisimulation. 
  Suppose $(\sN_1 | \sN) \CAL{R} (\sN_2 | \sN)$ and $\sN_1 | \sN
  \TRANS{\ell} \sN^1$. We discuss the shape of $\sN^1$:
  \begin{itemize}
  \item If $\sN^1= \sN'_1|\sN$, it means that $\sN_1 \TRANS{\ell}
    \sN'_1$. By definition of $\CAL{R}$, $\sN_2 \TAUTRANS{\hat{\ell}}
    \sN'_2$ and $\sN'_1 \WB \sN'_2$, we conclude.
  \item If $\sN^1 = \sN_1|\sN'$, it means that $\sN \TRANS{\ell}
    \sN'$. It is easy to conclude.
  \end{itemize}
  By examining the reduction rule associated to parallel composition,
  we observe that no reduction is induced through interactions between
  the two networks. Hence we have covered all cases. 
  The symmetric case (when $\sN_2 |\sN \TRANS{\ell} \sN^2$) is similar. 

  To prove (2) we proceed by showing that
 \small  $$\CAL{R} =
  \{ ( (\nu \VEC{n})(\sN_1 |{\dN}),(\nu \VEC{n})(\sN_2 |{\dN}))\ 
  |
\sN_1 \WB \sN_2, 
\text{
${\dN}$ composable with $\sN_1$ and $\sN_2$}
\}
$$\normalsize is a barbed congruence. 
% This can be done directly remarking that
% ${\dN}' \diamond (\sN_i | {\dN}) = \sN_i | ({\dN'} \diamond
%   {\dN})$. As $\sN_1 \cong \sN_2$, we conclude.
First, $\CAL{R}$ is clearly a congruence since it is closed under composition.
Second, for $(2)$, we take a composable ${\dN}'$. We have ${\dN}' \diamond
  (\sN_i | {\dN}) = \sN_i | ({\dN'} \diamond {\dN})$ for $i\in\{1,2\}$. We use the
  definition of $\CAL{R}$ to conclude.  For $(3)$, assume
  $\sN_1 | {\dN} \RED {\dN}^1$.
  \begin{itemize}
  \item If ${\dN}^1= \sN_1 | {\dN}'$, it means that ${\dN} \RED
    {\dN'}$. We use the definition of $\CAL{R}$ to conclude.
  \item If ${\dN}^1= \sN_1' | {\dN}'$, it means that ${\dN}= \sN_0 |
    \GQ{\RInfo}{\ell\CD H}$, ${\dN} = \sN_0' | \GQ{\RInfo}{H}$ and $\sN_1
    \TRANS{\ell} \sN'_1$. We deduce ${\dN}^2= \sN_2' | {\dN}'$, with
    ${\dN}= \sN_0 | \GQ{\RInfo}{\ell\CD H}$, ${\dN} = \sN_0' |
    \GQ{\RInfo}{H}$ and $\sN_2 \TRANS{\ell} \sN'_2$. We use the
    definition of $\CAL{R}$ to conclude.
  \item If the reduction is induced by interaction between $M_1$ and
    $N$, then $M_2$ has the corresponding action, hence we can
reason in the same way, hence done.
  \end{itemize}
  For $(2)$, we suppose that $ (\sN_1 | {\dN})\Downarrow_{\ell}$. Two
  cases can occur:
  \begin{itemize}
  \item Either ${\dN}\Downarrow_{\ell}$ and it follows directly that
    $(\sN_2 | {\dN})\Downarrow_{\ell}$.
  \item or $\sN_1 \TRANS{\ell} \sN'_1$ and by definition of $\CAL{R}$,
    $\sN_2 \TAUTRANS{\ell} \sN'_2$, meaning that $(\sN_2 |
    {\dN})\Downarrow_a$.
  \end{itemize}
  The symmetric case is similar.
\end{proof}
\NI By definition this shows $\WB \subset \cong$.



\begin{example}[Example of Equivalence]
\label{sec:equi_ex}
We give here an example illustrating our equivalences of
networks. Consider the following networks:

$$
\begin{array}{lcl}
\sN'_0 & = & \quad~
\namedPmo{a_1(y_1[\CLIENTROLE]:\LA_\CLIENTROLE).\SELECTdef{y_1}{\CLIENTROLE}{\AUTHROLE}{}{\lab{Login}{}(x_i)}.y_1[\AUTHROLE,\CLIENTROLE]?
  \lab{LoginOK}{}().P_{\mathtt{LOOP},\CLIENTROLE} }{\alpha_1}\\ \sN'_1 &
= & \quad~
\namedPmo{a_2(y_2[\SERVERROLE]:\LA_\SERVERROLE).P_{\mathtt{LOOP},\SERVERROLE}}{\alpha_2}
\\ & & ~\mid~ \namedPmo{ (\nu s)~
  \AOUTPUT{a_1}{s}{\CLIENTROLE}{\LA_{\CLIENTROLE}}{} \mid
  \AOUTPUT{a_2}{s}{\SERVERROLE}{\LA_{\SERVERROLE}}{} \mid
  \AOUTPUT{a_3}{s}{\AUTHROLE}{\LA_{\AUTHROLE}}{} \\ & & \quad~
 \mid
  a_3(y_3[\AUTHROLE]:\LA_\AUTHROLE).   y_3[\CLIENTROLE,\AUTHROLE]?(\lab{Login}{}(x_i)).\SELECTdef{y_3}{\AUTHROLE}{\SERVERROLE}{}{\lab{LoginOk}{}()}.P_{\mathtt{LOOP},\AUTHROLE}
}{\beta}


\\ \sN'_2 & = &\quad~
\namedPmo{a_2(y_2[\SERVERROLE]:\LA_\SERVERROLE).P_{\mathtt{LOOP},\SERVERROLE}
}{\alpha_2} \\
& & ~\mid~ \namedPmo{a_4(y_4[\DB]:\LA_\DB).y_4[\AUTHROLE,\DB]?(\lab{Query}{}).
  \SELECTdef{y_4}{\DB}{\AUTHROLE}{}{\lab{Answer}{}}}{\gamma} \\
 & & ~\mid~ \namedPmo{(\nu s)~(
  \AOUTPUT{a_1}{s}{\CLIENTROLE}{\LA_{\CLIENTROLE}}{} \mid
  \AOUTPUT{a_2}{s}{\SERVERROLE}{\LA_{\SERVERROLE}}{} \mid
  \AOUTPUT{a_3}{s}{\AUTHROLE}{\LA_{\AUTHROLE}}{} \mid
  \AOUTPUT{a_4}{s}{\DB}{\LA_{\DB}}{}) 
\\ & & \quad~\mid~  
a_3(y_3[\AUTHROLE]:\LA'_\AUTHROLE).
y_3[\CLIENTROLE,\AUTHROLE]?(\lab{Login}{}(x_i)). \\ & & \quad\quad\SELECTdef{y_3}{\AUTHROLE}{\DB}{}{\lab{Query}{}}.
y_3[\DB,\AUTHROLE]?(\lab{Answer}{}).P_{\mathtt{LOOP},\AUTHROLE}
}{\beta} \\ \dN'_1 & = & \quad~ \sN'_1 ~\mid~ \GQ{a_1 \mapsto
\alpha_1, a_2 \mapsto \alpha_2, a_3 \mapsto \beta}{\emptyset}
\\\dN'_2 & = & \quad~ (\nu a_4)~(\sN'_2 ~\mid~ \GQ{a_1 \mapsto
\alpha_1, a_2 \mapsto \alpha_2, a_3 \mapsto \beta, a_4 \mapsto
\gamma}{\emptyset})
\end{array}
$$

Our networks implement the ATM example defined in~\ref{ex_global}. For
the sake of clarity, we have to take the following shortcuts: (1) we
only consider the login phase of the protocol, the $\mathtt{LOOP}$
phase is abstracted into three processes
$P_{\mathtt{LOOP},\CLIENTROLE}$, $P_{\mathtt{LOOP},\AUTHROLE}$,
$P_{\mathtt{LOOP},\SERVERROLE}$ for the three different roles, (2) to
lighten the notations, we do not make the logical annotations
explicit, (3) as a result of (2), we do not implement login validation
and only write the case were the login succeeds.

We present two different networks $\dN'_1$ and $\dN'_2$, both are
implementing the Server-Authenticator part of the ATM protocol. The
Server part is the same in both processes (executed at principal
$\alpha_2$), but the Authenticator part (executed at $\beta$) is
different: $\dN'_1$ implements straightforwardly the protocol while
$\dN'_2$ contains another indirection involving a fourth participant
(executed at $\gamma$): the Authenticator sends a query to a
\emph{Database} to retrieve additional information required in the
login process, and the Database answers.

Thus, the protocols implemented in both networks are different, as one
involves three participants and the other one four. Yet, the query to the
Database is $\dN'_2$ is unobservable from the outside, and an external
client, such as $\dN'_0$ cannot distinguish between $\dN'_1$ and $\dN'_2$.

This is captured by our equivalences: the two partial
networks $\sN'_1$ and $\sN'_2$ do not contain the same components, and
as a result, are not bisimilar: after some steps, $\sN'_2$ is able to
emit on the channel $a_4$, which is impossible for $\sN'_1$. However,
when encapsulated into dynamic networks $\dN'_1$ and $\dN'_2$, they
are barbed r.c. congruent: they will offer it the same interface to the same external client. 
%We have: $\sN'_1 \WB \sN'_2 \quad \dN'_1 \cong \dN'_2$
\end{example}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% Local IspellDict: british
%%% Local IspellPersDict: ./.ispell
%%% End:


