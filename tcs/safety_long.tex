\section{Monitored networks enjoy safety assurance and session fidelity}
\label{sub:sessionfidelityandsafety}

%\paragraph{\bf Session fidelity}
%The first main result states that monitored networks correctly follow
%a global choreography: if a monitored network conforms to a
%specification, it does not create bad messages that cannot be received
%by the specification and global transport.
%%and that, at any time, the different local types
%%correspond to a global choreography.
%The formulation of session fidelity requires
%the detailed definitions.
%The formal version and its proof can be found in
%Appendix~\ref{app:fidelity}. Here we state the theorem informally:

%\begin{THM}[Session fidelity] \label{thm:fidelity}
%Suppose an initial network $N$ conforms to a specification $\Sigma$,
%a projection from well-formed global type,
%and contains $\GQ{\RInfo}{\emptyset}$.
%Then, with the messages which $N$ exchanges follow the specification,
%the dynamics of the network witnesses the validity of specifications.
%\end{THM}
%\begin{proof}
%The formal description of session fidelity and its proofs please see Appendix~\ref{app:fidelity}.
%\end{proof}

In this section, we present
the properties underpinning safety assurance
in the proposed framework from different perspectives.


Theorem~\ref{thm:safetrans} \LB{shows} local safety/transparency, and global safety/transparency 
for fully monitored networks. A network $\mN$ is {\em fully monitored wrt $\Sigma$} when all its principals are 
monitored and the collection of the monitors is congruent to $\Sigma$.   

\begin{THM}[Safety and Transparency] \label{thm:safetrans} \rm
\begin{enumerate}
\item\label{item:prop:localsafe} {\bf (Local Safety)} 
$\models \NLNWgood{\namedP{\M}{P}{\alpha}}{ {\alpha: \AssEnv}}$ with
$\M=\AT{\alpha}{\AssEnv}$.

\item\label{item:prop:localtrans} {\bf (Local Transparency)} 
If $\models \NLNWgood{\NP{P}{\alpha}}{\alpha: {\AssEnv}}$, then
$\NP{P}{\alpha} \WB (\namedP{\M}{P}{\alpha})$ with
$\M={\alpha:\AssEnv}$.

\item\label{item:prop:globalsafe} {\bf (Global Safety)} 
If $\mN$ is fully monitored w.r.t. $\Sigma$, then
$\models \NLNWgood{\mN}{\Sigma}$.

\item\label{item:prop:globaltrans} {\bf (Global Transparency)}
Assum $\mN$ and $\dN$ have the same global transport $\GQ{\RInfo}{\GQueue}$.
If $\mN$ is fully monitored w.r.t. $\Sigma$ and
$\dN = \sN \mid \GQ{\RInfo}{\GQueue}$ is unmonitored but
$\models \NLNWgood{\sN}{\Sigma}$,
then we have $\mN \sim \dN$.
\end{enumerate}
\end{THM}

Local safety~(\ref{thm:safetrans}.\ref{item:prop:localsafe}) states
that a monitored process always behaves well with respect to the specification. 
Local transparency~(\ref{thm:safetrans}.\ref{item:prop:localtrans}) 
states that a monitored process behaves as an unmonitored process 
when the latter is well-behaved (e.g., it is statically checked).  
Global safety~(\ref{thm:safetrans}.\ref{item:prop:globalsafe}) states 
that a fully monitored network behaves well with respect to 
the given global specification.  
This property is closely related to session fidelity, 
introduced later in Theorem \ref{thm:sf}.
%
Global transparency~(\ref{thm:safetrans}.\ref{item:prop:globaltrans})
states that a monitored network and 
an unmonitored network have equivalent behaviour
when the latter is well-behaved with respect to the same (collection of) specifications.
%
%Global transparency can be easily derived from 
%local transparency via 

By Proposition~\ref{prop:sim:cong}  
and (\ref{thm:safetrans}.\ref{item:prop:localtrans}), 
we derive Corollary~\ref{cor:cong:localtrans}
stating that weakly bisimilar static networks 
combined with the same global transport are congruent.
\begin{COR}[\textbf{Local transparency}] \label{cor:cong:localtrans} \rm
If $\models \NLNWgood{\NP{P}{\alpha}}{\AT{\alpha}{\AssEnv}}$, then for
  any $\GQ{\RInfo}{\GQueue}$, we have
  $(\NP{P}{\alpha} \mid \GQ{\RInfo}{\GQueue}) \cong
  (\namedP{\M}{P}{\alpha} \mid \GQ{\RInfo}{\GQueue})$ with
  $\M=\AT{\alpha}{\AssEnv}$.
\end{COR}

%
By Theorem~\ref{thm:safetrans}, 
we can {\em mix} unmonitored principals 
with monitored principals still obtaining the desired safety assurances. 

In the following, we refer to a pair $\Sigma; \GQ{\RInfo}{\GQueue}$ of
a specification and a global transport as a \emph{configuration}. 
%
The labelled transition relation for configurations, denoted by
$\GTRANS{\ell}$, defined in Figure \ref{fig:LTSCF} in Appendix~\ref{app:fidelity}. 
%\LB{is relegated to~\cite{report}}. 
Here it is sufficient to notice that the transitions of a configuration define
the correct behaviours (with respect to $\Sigma$) in terms of the observation
of inputs and outputs from/to the global transport
$\GQ{\RInfo}{\GQueue}$. 
%
We write that a configuration $\Sigma;\GQ{\RInfo}{\GQueue}$ is {\em
  configurationally consistent} if all of its multi-step input
  transition derivatives are receivable and the resulting
  specifications $\Sigma$ is consistent. 

We also use $\GTRANS{\ell}$ to model 
globally visible transitions of networks (i.e., those locally visible transitions 
of a network that can be observed by its global transport).  
  %
 Below, we  state  that a message
  emitted by a valid output action is always receivable.

\begin{LEM} \label{lem:emptyreceive} \rm
Assume a network $\dN \Cong \sN|\GQ{\RInfo}{\GQueue}$ 
conforming to $\Sigma; \GQ{\RInfo}{\GQueue}$
which is configurationally consistent,
if $\dN \GTRANS{\ell} \dN'$ such that
$\ell$ is an output and
$\Sigma;\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue \CD m}$
%$\dN' \Cong \sN' | \GQ{\RInfo}{\GQueue \CD m}$,
then $\GQueue\cdot m$ is receivable to $\Sigma'$.
\end{LEM}
Also, we state that, as $\dN \equiv \sN \mid H$ and
$\models \NLNWgood{\sN}{\Sigma}$,
the satisfaction relation of $\sN$ and $\Sigma$ is preserved by transitions.
\begin{LEM}\label{lem:nsatisfiesM} \rm
Assume $\dN \equiv \sN \mid H$ and $\models \NLNWgood{\sN}{\Sigma}$.
If $\dN \GTRANS{\ell} \dN' \equiv \sN' \mid H'$ 
and $\Sigma \TRANS{\ell} \Sigma'$,
then $\models \NLNWgood{\sN'}{\Sigma'}$.
\end{LEM}


%Session fidelity shows that, 
%as long as a network dynamically conforms 
%to a collection of global specifications,
%as session interactions happen, 
%the network still dynamically conforms to the original specifications.

%In order to state session fidelity, whose proof and auxiliary
%definitions are relegated to Appendix \ref{app:fidelity}, we use a LTS
%$\GTRANS{\ell}$ for configurations, which is straightforwardly defined.
{\begin{THM}[Session Fidelity] \label{thm:sf} \rm Assume configuration
$\Sigma; \GQ{\RInfo}{\GQueue}$ is configurationally consistent, and
network $\dN\Cong \sN|\GQ{\RInfo}{\GQueue}$
%$\dN\Cong (\nu\ n)(\sN_0|\GQ{\RInfo}{\GQueue})$ 
conforms to configuration
$\Sigma; \GQ{\RInfo}{\GQueue}$.
%% , which is configurationally consistent.
For any $\ell$, whenever we have 
$\dN \GTRANS{\ell} \dN'$ s.t. 
$\Sigma; \GQ{\RInfo}{\GQueue} \GTRANS{\ell} \Sigma'; \GQ{\RInfo'}{\GQueue'}$,
it holds that 
$\Sigma'; \GQ{\RInfo'}{\GQueue'}$ is configurationally consistent and
$\dN'$ conforms to $\Sigma'; \GQ{\RInfo'}{\GQueue'}$.
\end{THM}
By session fidelity, if all session message exchanges in a monitored/unmonitored network behave well with respect to the specifications (as communications occur), then this network exactly follows the original global specifications.
% all session message exchanges in a session in a
% fully monitored network exactly follow the original global session type.  

