\section{Safety Assurance in Partial Networks} \label{app:safety} \label{sec:safety}
In this section, we present the properties underpinning safety assurance in partial networks, that are networks without a global transport. By considering partial networks we focus on the properties of principals (and their respective monitors) with respect to specifications, abstracting from the routing mechanisms. The routing mechanisms will be taken into account in later sections. We first consider networks consisting of single monitored principals ({\em local} safety) and then extend the results to partial networks in general ({\em global} safety).

Recall that: partial networks are networks without global transport; $\sN$ denotes an unmonitored partial network; $\dN$ denotes an unmonitored network; $\mN$ denotes a (monitored or unmonitored) network. Monitors, ranged over by $\M$, are specifications (of the form $\alpha: \AssEnv$) used for dynamic verification. See Table~\ref{tab:sum} for a summary of the notation. 

\begin{table}
\centering
\begin{tabular}{ c | c | c }
   $\M$ & monitor & specification used for monitoring\\
   \hline
   $\sN$ & unmonitored partial network & without $\M$, without $\GQ{\RInfo}{\GQueue}$ \\
    \hline
    $\dN$ & unmonitored network & without $\M$, with $\GQ{\RInfo}{\GQueue}$ \\
\hline
  $\mN$ & network & with or without $\M$, with $\GQ{\RInfo}{\GQueue}$
\vspace{2mm}\end{tabular}
\label{tab:sum}
\caption{Networks: summary of the notations}
\end{table}

The partial network composed by a principal guarded by its monitor can
take any action expected by the specification:

\begin{LEM} \label{def:semantics_monnetwork} \rm
For any principal $[P]_{\alpha}$, specification $\alpha: \ENCan{\Gamma, \Delta}$, and action $\ell$,
if $\alpha: \ENCan{\Gamma, \Delta} \TRANS{\ell} \alpha : \ENCan{\Gamma', \Delta'}$ and $[P]_{\alpha} \TRANS{\ell} [P']_{\alpha}$,
then $[P]_{\alpha} \mid \alpha: \ENCan{\Gamma, \Delta} \TRANS{\ell} [P']_{\alpha} \mid \alpha : \ENCan{\Gamma', \Delta'}$.
\end{LEM}
\begin{proof}
Direct, as no interaction can \TZ{appear} between $[P]_{\alpha}$ and
its monitor with specification $\alpha: \ENCan{\Gamma, \Delta}$ when $\ell$ is performed.  $\qedhere$
\end{proof}

Local safety ensures that a monitored process always behaves well with respect to the 
specification used to define its monitor. 

\begin{THM}[Local safety] \label{item:prop:localsafe} \label{app:safetyproof} \rm
$\models \NLNWgood{\namedP{\M}{P}{\alpha}}{ {\alpha: \AssEnv}}$ with
$\M=\AT{\alpha}{\AssEnv}$.
\end{THM}
\begin{proof}
We define a relation $R$ as:
$$R = \{ (\namedP{\M}{P}{\alpha},~\alpha: \AssEnv) \ \mid \ \M=\AT{\alpha}{\AssEnv} \}$$
Assume $(\namedP{\M_0}{P_0}{\alpha'},  \alpha': \ENCan{\Gamma_0, \Delta_0}) \in R$:
\begin{enumerate}
\item For an input $\ell$,
because $\M_0 = \alpha': \ENCan{\Gamma_0, \Delta_0}$ by assumption, 
%that $(\namedP{\M_0}{P_0}{\alpha'},  \alpha': \ENCan{\Gamma_0, \Delta_0}) \in R$,
that $\alpha': \ENCan{\Gamma_0, \Delta_0} \TRANS{\ell} \alpha': \ENCan{\Gamma'_0, \Delta'_0}$ and
$\namedP{\M_0}{P_0}{\alpha'}$ having an input at $\sbj{\ell}$
together imply that 
$ [P_0]_{\alpha'} \TRANS{\ell} [P'_0]_{\alpha'}$,
thus by Lemma \ref{def:semantics_monnetwork},
we have $[P_0]_{\alpha'} \mid \alpha': \ENCan{\Gamma_0, \Delta_0} \TRANS{\ell} [P'_0]_{\alpha'} \mid \M'_0$,
and $\M'_0 = \alpha': \ENCan{\Gamma'_0, \Delta'_0} $.
Thus we have $(\namedP{\M'_0}{P'_0}{\alpha'}, \alpha': \ENCan{\Gamma'_0, \Delta'_0} ) \in R$.

\item For an output $\ell$,
$\namedP{\M_0}{P_0}{\alpha'}\TRANS{\ell} \namedP{\M'_0}{P'_0}{\alpha'}$ implies
$\M_0 = \alpha': \ENCan{\Gamma_0, \Delta_0}\TRANS{\ell} \alpha': \ENCan{\Gamma'_0, \Delta'_0} =\M'_0$.
Thus we have $(\namedP{\M'_0}{P'_0}{\alpha'}, \alpha': \ENCan{\Gamma'_0, \Delta'_0} ) \in R$.

\item For an $\tau$,
$\namedP{\M_0}{P_0}{\alpha'}\TRANS{\tau}\namedP{\M_0}{P_0}{\alpha'}$ implies that 
$\M_0 = \alpha': \ENCan{\Gamma_0, \Delta_0}\TRANS{\tau} \alpha': \ENCan{\Gamma_0, \Delta_0}= \M_0$.
\end{enumerate}
Therefore, by Definition \ref{def:partial}, $R$ is a satisfaction
relation and $\models \NLNWgood{\namedP{\M}{P}{\alpha}}{ {\alpha:
    \AssEnv}}$ with $\M=\AT{\alpha}{\AssEnv}$.  $\qedhere$
\end{proof}

%We proceed by analysis of the cases of Definition \ref{def:partial}: (1) each step of $\AT{\alpha}%{\ENCan{\Gamma;\Delta}}$ can be mimicked by partial network ${\namedP{\M}{P}{\alpha}}$ since $\M=\AT{\alpha}{\ENCan{\Gamma;\Delta}}$\lau{by which rules?}; (2,3) each step of   

We define a safety property for partial networks that may include multiple principals.
\RD{It describes the fact that a monitored network satisfies its specification.} 
\begin{DEF}[Network global safety] \label{def:globalsafe} \rm
$\sN \mid \M$ is globally safe with respect to $\Sigma$ if and only if  $\models \NLNWgood{\sN \mid \M}{\Sigma}$.
\end{DEF}

We introduce a condition on the structure of a network and on its
monitors, which guarantees global safety. A partial network is {\em
  fully monitored w.r.t. $\Sigma$} when all its principals are
monitored and the collection of the monitors is weakly bisimilar to
$\Sigma$. Formally, $\sN\mid \M$ is fully monitored w.r.t. $\Sigma$
when $\sN\mid \M\equiv [P_1]_{\alpha_1}\mid \M_1 \mid \ldots \mid
[P_n]_{\alpha_n}\mid \M_n$ for some $n\geq 0$ and
$\M_1,\ldots,\M_n{\WB}\Sigma$. 
%
By Theorem \ref{item:prop:globalsafe} a {\em fully monitored} network
is globally safe.
\RD{Theorem~\ref{item:prop:globalsafe} justifies
  monitoring by ensuring that fully monitored systems behave as
  expected.}
%This property is closely related to session fidelity, 
%introduced later in Theorem \ref{thm:sf}.
\begin{THM}[Global safety] \label{item:prop:globalsafe} \rm
If $\sN \mid \M$ is fully monitored w.r.t. $\Sigma$, then
$\models \NLNWgood{\sN \mid \M}{\Sigma}$.
\end{THM}
\begin{proof}
Assume $\mN$ is composed by 
monitored endpoints $\namedP{\M_i}{P_i}{\alpha_i}, i \in \ASET{1, ..., n}$. 
$$\sN \mid \M \Cong \namedP{\M_1}{P_1}{\alpha_1} \PAR ... \PAR  \namedP{\M_n}{P_n}{\alpha_n} 
                 $$
where $\M_i=\AT{\alpha_i}{\ENCan{\Gamma_i;\Delta_i}}$ for $i = \ASET{1, ..., n}$,
$\Sigma=\M_1 , ... , \M_n$. 
Based on Theorem \ref{app:safetyproof}, for each $i \in \ASET{1, ..., n}$, 
$$\models \NLNWgood{\namedP{\M_i}{P_i}{\alpha_i}}{\AT{\alpha_i}{\ENCan{\Gamma_i; \Delta_i}}}$$ 
with $\M_i=\AT{\alpha_i}{\ENCan{\Gamma_i;\Delta_i}}$.
By Definition \ref{def:partial} and induction, 
we have
$$\NLNWgood{\namedP{\M_1}{P_1}{\alpha_1}\ \ \PAR ... \PAR\ \  \namedP{\M_n}{P_n}{\alpha_n}}
{\AT{\alpha_1}{\ENCan{\Gamma_1; \Delta_1}}, ... , \AT{\alpha_n}{\ENCan{\Gamma_n; \Delta_n}}}$$
so that
$\models \NLNWgood{\sN \mid \M}{\Sigma}$.
$\qedhere$
\end{proof}


\section{Transparency of Monitored Networks}
\label{sec:transparency}
Whereas safety assurance focuses on preventing violations from the principals, transparency ensures that monitors do not affect the behaviour of well-behaved principals. We first consider transparency for partial networks consisting of one single principal (\emph{local transparency}) and then extend the result to monitored networks with global transport. 

\begin{THM}[Local transparency]  \label{item:prop:localtrans} \rm
If $\models \NLNWgood{\NP{P}{\alpha}}{\alpha: {\AssEnv}}$, then
$\NP{P}{\alpha} \WB (\namedP{\M}{P}{\alpha})$ with
$\M={\alpha:\AssEnv}$.
\end{THM}

\RD{That is, a correct participant is not impaired by monitoring.}

\begin{proof}
Define a relation $R$ as: 
\[ R = \ASET{ (\NP{P}{\alpha}, \namedP{\M}{P}{\alpha}) ~ \mid ~
                       \models \NLNWgood{\NP{P}{\alpha}}{\AT{\alpha}{\ENCan{\Gamma;\Delta}}} }\]
 
Assume $(\NP{P}{\alpha}, \namedP{\M}{P}{\alpha}) \in R$,
\begin{itemize}
\item for an output $\ell$ (the case for $\tau$ is similar),
         $\NP{P}{\alpha} \TRANS{\ell} \NP{P'}{\alpha}$ implies 
         $\M \TRANS{\ell} \M'$ due to $\models \NLNWgood{\NP{P}{\alpha}}{\M}$; 
         by Lemma \ref{def:semantics_monnetwork}, 
         we have $\namedP{\M}{P}{\alpha} \TRANS{\ell} \namedP{\M'}{P'}{\alpha}$;
         
\item for an input $\ell$, 
         $\NP{P}{\alpha} \TRANS{\ell} \NP{P'}{\alpha}$ 
         only when $\M \TRANS{\ell} \M'$, which together imply that,
         by Lemma \ref{def:semantics_monnetwork}, 
         $\namedP{\M}{P}{\alpha} \TRANS{\ell} \namedP{\M'}{P'}{\alpha}$.
\end{itemize}
By Definition \ref{def:partial}, 
we have $\models \NLNWgood{\NP{P'}{\alpha}}{\M'}$, 
so that $(\NP{P'}{\alpha}, \namedP{\M'}{P'}{\alpha}) \in R$.
\\\\
%%%%%%%%%
Symmetrically, 
since, by Theorem \ref{item:prop:localsafe}, we have 
$\models\NLNWgood{\namedP{\M}{P}{\alpha}}{\AT{\alpha}{\AssEnv}}$ with 
$\M=\AT{\alpha}{\ENCan{\Gamma;\Delta}}$,
\begin{itemize}
\item for an output $\ell$ or $\tau$,
         $\namedP{\M}{P}{\alpha} \TRANS{\ell} \namedP{\M'}{P'}{\alpha}$
         implies $\M \TRANS{\ell} \M'$ whenever
         $\NP{P}{\alpha} \TRANS{\ell} \NP{P'}{\alpha}$;
         
\item for an input $\ell$, 
         $\namedP{\M}{P}{\alpha} \TRANS{\ell} \namedP{\M'}{P'}{\alpha}$
         says $\M \TRANS{\ell} \M'$, which implies
         $\NP{P}{\alpha} \TRANS{\ell} \NP{P'}{\alpha}$.
\end{itemize}
By Definition \ref{def:partial}, 
we have $\models \NLNWgood{\namedP{\M'}{P'}{\alpha}}{\M'}$,
so that  $( \namedP{\M'}{P'}{\alpha}, \NP{P'}{\alpha}) \in R$.
By Definition \ref{def:sim:WB}, $\NP{P}{\alpha} \WB
(\namedP{\M}{P}{\alpha})$ with $\M=\AT{\alpha}{\AssEnv}$.
$\qedhere$
\end{proof}


By Proposition~\ref{prop:sim:cong} and Theorem
\ref{item:prop:localtrans}, we derive
Corollary~\ref{cor:cong:localtrans} stating that weakly bisimilar
static networks combined with the same global transport are weakly
bisimilar; i.e. monitoring does not affect routing of information
  to and from a correct principal.
\begin{COR}[\textbf{Bisimilarity}] \label{cor:cong:localtrans} \rm
If $\models \NLNWgood{\NP{P}{\alpha}}{\AT{\alpha}{\AssEnv}}$, then for
  any $\GQ{\RInfo}{\GQueue}$, we have
  $(\NP{P}{\alpha} \mid \GQ{\RInfo}{\GQueue}) \lau{\WB}
  (\namedP{\M}{P}{\alpha} \mid \GQ{\RInfo}{\GQueue})$ with
  $\M=\AT{\alpha}{\AssEnv}$.
\end{COR}


Global transparency~(Theorem \ref{item:prop:globaltrans}) states a
collection of specifications (monitors) does not alter the behaviour
of a well-behaved networks.  We consider networks \emph{with global
  transport} to ensure that the correctness of the network is not
altered during the routing of messages.
%
Observe that the reduction relation for networks introduced in Fig. \ref{fig:red:network} models interactions with the global transport as {\em invisible} actions. 
%
In order to enable the observation of the behaviour of a network together with the dynamics of its global transport $\GQueue$ we introduce a new set of rules for the labelled transitions of networks, denoted by $\GTRANS{\ell}$, and presented in Fig. \ref{fig:LTS:network}.
%Example~\ref{ex:globaltrans} motivates the introduction of the rules in Fig.~\ref{fig:LTS:network}.
%
%\begin{example}[Transparency and transport observation] \label{ex:globaltrans} \rm
%Assume a process $\namedPmo{\PRG}{\alpha}$ = $\namedPmo{\nEC{\SELECTdef{s}{\rr_1}{\rr_2}{l_j}{v}} ; %\PRG'}{\alpha}$ 
%and $\namedPmo{\PRG}{\alpha} \TRANS{\ell} \namedPmo{\PRG'}{\alpha}$ 
%by taking a sending action $\ell =s[\rr_1,\rr_2]!l_j\ENCan{v}$.
%For $\namedPmo{\PRG}{\alpha}$ itself (locally), the action is clearly the one of 
%$\ell = s[\rr_1,\rr_2]!l_j\ENCan{v}$; 
%Fig. \ref{fig:LTS:network:unlinked} defines it as a {\em local} visible action.
%However, for the network (globally), 
%this action is marginal and is not observable {\em unless} 
%the message $s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$, generated by $\ell$, 
%enters the global transport $\GQ{\RInfo}{\GQueue}$;
%Fig. \ref{fig:red:network} defines this {\em invisibility} w.r.t the {\em network}. 
The transitions in Fig.  \ref{fig:LTS:network} allow us to globally 
observe, for example, that a message sent by $\namedPmo{\PRG}{\alpha}$ 
{\em enters} the global transport:
$$
\GQ{\RInfo}{\GQueue} \GTRANS{s[\rr_1,\rr_2]!l_j\ENCan{v}} 
\GQ{\RInfo}{\GQueue \CD s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}}
$$
Similarly, the parallel composition of a principal sending $s[\rr_1,\rr_2]!l_j\ENCan{v}$
and the global transport is made visible as follows:
$$\namedPmo{\nEC{\SELECTdef{s}{\rr_1}{\rr_2}{l_j}{v}} ; \PRG'}{\alpha}\mid \GQ{\RInfo}{\GQueue}
 \GTRANS{s[\rr_1,\rr_2]!l_j\ENCan{v}} \namedPmo{\PRG'}{\alpha}\mid\GQueue \CD s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}
$$

%If there is a principal $\namedPmo{\PRGQ}{\beta} = 
%\namedPmo{s[\rr_1, \rr_2]? \{ l_i (x_i). \PRGQ_i \}_{i \in I}}{\beta}$ and $j \in I$
%which is ready to receive the message $s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$,
%then for $\namedPmo{\PRGQ}{\beta}$ (locally), 
%the action is clearly the one of $\ell' = s[\rr_1,\rr_2]?l_j\ENCan{v}$,
%which is defined by Fig. \ref{fig:LTS:network:unlinked} as a {\em local} visible action.
%However, again, for the network (globally), 
%this action is not observable {\em unless} the global transport
%observes that the message $s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$ {\em leaves} it.
%Fig. \ref{fig:red:network} defines this invisibility w.r.t the {\em network}. 
%We thus use a global observable notation $\GTRANS{\ell}$
%$$
%\GQ{\RInfo}{\GQueue \CD s \ENCan{\rr_1, \rr_2, l_j\ENCan{v}}}\GTRANS{s[\rr_1,\rr_2]?l_j\ENCan{v}} 
%\GQ{\RInfo}{\GQueue}  
%$$
%to show what the global transport observes: the message leaves the global queue.
%\end{example}


We define $\mathsf{dest}$ as 
a partial function mapping a label, which representing an action,
to its destination as: 
\begin{eqnarray} \label{pi:eq:dest} \nonumber
\mathsf{dest} &::=& 
                   \AOUT{a}{s}{\rr\!}{\!\LA} \mapsto a
                   \grmor
                   \AIN{a}{s}{\rr\!}{\!\LA} \mapsto a
                   \\ \nonumber
                   & \grmor&
                   \LInter{s}{\rr_1,\rr_2}! \TZ{l}\ENCan{v}\mapsto s[\rr_2]
                   \grmor
                   \LInter{s}{\rr_1,\rr_2}? \TZ{l}\ENCan{v}\mapsto s[\rr_2] 
\end{eqnarray}
The notation of global observable transition $\GTRANS{\ell}$, 
used to denote {\em globally observable action} $\ell$, is defined by the rules in Fig.~\ref{fig:NW:LTS}.
\input{fig_transition_network}
%%%%%%%%%%%%%%%%%%%%
Rules $\lprule{req}$ and $\lprule{acc}$ (resp. $\lprule{sel}$ and $\lprule{bra}$) are for inserting and removing invitation messages (resp. messages in established sessions) from the global transport.
%
Rules $\lprule{acc}$ and $\lprule{bra}$ represent that,
as a message leaves the global queue, there should be a local principal receiving it as an input. 
%By $\lprule{In}$ there is a local process in $\sN$ going to receive a message from the global queue,
%thus the destination of action $\ell$ should be inside $\sN$.
%
Similarly, rules $\lprule{req}$ and $\lprule{sel}$ represent that,
as a message enters the global queue, there should be a local principal outputting it to the queue. 
%are generalised as rule $\lprule{Out}$.
%
%As we explained above, the visible actions in these rules
%are observable in the global queue. 
%By $\lprule{Out}$ there is a local process in $\sN$ outputting a message
%to the global queue, thus the destination of action $\ell$
%should be outside $\sN$.
%
By $\lprule{Net}$ for unmonitored networks, as $\dN \GTRANS{\ell} \dN'$, 
it means $\exists \NP{P}{\alpha} \in \dN$,
$\NP{P}{\alpha} \TRANS{\ell} \NP{P'}{\alpha}$ (i.e. locally visible) such that 
$\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \GQ{\RInfo'}{\GQueue'}$ (i.e. globally visible).
%
Rule $\lprule{tau}$ summarizes the reduction rules defined in Section \ref{sec:calculus}.
Rule $\lprule{res}$ and $\lprule{str}$ are standard.
Rule $\lprule{par}$ says that, the bound names of action $\ell$
should not be any free name appearing in network $\dN_2$,
and it should not be absorbed by any process in network $\dN_2$
(i.e. its destination is not in $\dN_2$).
%
By rule $\lprule{Mon}$ for monitored networks, 
$\mN \GTRANS{\ell} \mN'$ means
$\exists \namedP{\M}{P}{\alpha} \in \mN$,
$\NP{P}{\alpha} \TRANS{\ell} \NP{P'}{\alpha}$ and $\M \TRANS{\ell} \M'$ (i.e. locally visible)
such that 
$\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \GQ{\RInfo'}{\GQueue'}$ (i.e. globally visible).



\begin{THM}[Global transparency] \label{item:prop:globaltrans} \rm
Assume $\mN$ and $\dN$ have the same global transport $\GQ{\RInfo}{\GQueue}$.
If $\mN$ is fully monitored w.r.t. $\Sigma$ and
$\dN = \sN \mid \GQ{\RInfo}{\GQueue}$ is unmonitored but
$\models \NLNWgood{\sN}{\Sigma}$,
then we have $\mN \WB \dN$.
\end{THM}

\begin{proof}
Define a relation $R$: 
$$ R = \ASET{ \mN, \dN ~ \grmor ~
                     \dN = \sN \mid \GQ{\RInfo}{\GQueue}\ \text{and}\
                     \models \NLNWgood{\sN}{\Sigma}} $$
We prove that $R$ is a standard strong bisimilar relation
over $\GTRANS{\ell}$.
Note that, $\NLNWgood{\sN}{\Sigma}$ means
$\forall \NP{P_i}{\alpha_i} \in \sN$, 
we have $\alpha_i: \ENCan{\Gamma_i ; \Delta_i} \in \Sigma$
and $\models \NP{P_i}{\alpha_i} : \alpha_i: \ENCan{\Gamma_i ; \Delta_i}$.

\begin{enumerate}
\item As $\mN \GTRANS{\ell} \mN'$, it implies 
$\exists \namedP{\M_j}{P_j}{\alpha_j} \in \mN$,
$\NP{P_j}{\alpha_j}\TRANS{\ell} \NP{P'_j}{\alpha_j}$ and $\M_j \TRANS{\ell} \M'_j$ such that 
$\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \GQ{\RInfo'}{\GQueue'}$, 
and other monitored processes in $\mN$ are not affected.
%%%%%%%%%%%%
When $\ell$ is an input, by Definition \ref{def:partial},
since $\models \NLNWgood{\sN}{\Sigma}$, we should have
$\NP{P_j}{\alpha_j}\TRANS{\ell} \NP{P'_j}{\alpha_j}$; 
when $\ell$ is an output or a $\tau$ action,
by Definition \ref{def:partial},
the transition of 
$\NP{P_j}{\alpha_j}\TRANS{\ell} \NP{P'_j}{\alpha_j}$
is able to take place.
Both cases lead to $\sN \TRANS{\ell} \sN'$ and
$\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \GQ{\RInfo'}{\GQueue'}$ 
so that $\dN = \sN \mid \GQ{\RInfo}{\GQueue} \GTRANS{\ell} 
\sN' \mid \GQ{\RInfo'}{\GQueue'} = \dN'$, and
$\models \NP{P'}{\alpha_j} : \alpha_j : \ENCan{\Gamma'_j ; \Delta'_j}$
by Definition \ref{def:partial}.
$\alpha_j : \ENCan{\Gamma'_j ; \Delta'_j}$ is the resulting new configuration
of $\alpha_j$ in $\Sigma$. 
Other specifications 
$\ASET{\alpha_i : \ENCan{\Gamma_i ; \Delta_i}}_{i \in I \setminus \ASET{j}} \in \Sigma$ 
are not affected.
Let $\Sigma' = 
\alpha_j : \ENCan{\Gamma'_j ; \Delta'_j}, 
\ASET{\alpha_i : \ENCan{\Gamma_i ; \Delta_i}}_{i \in I \setminus \ASET{j}}$. 
Therefore, for the resulting new network
$\dN' = \sN' \mid \GQ{\RInfo'}{\GQueue'}$,
we have $\models \NLNWgood{\sN'}{\Sigma'}$.
Thus we have $(\mN', \dN') \in R$.

\item For the symmetric case, as $\dN \GTRANS{\ell} \dN'$, it implies 
$\exists \NP{P_j}{\alpha_j} \in \dN$,
$\NP{P_j}{\alpha_j}\TRANS{\ell} \NP{P'_j}{\alpha_j}$ such that 
$\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \GQ{\RInfo'}{\GQueue'}$
and other processes in $\dN$ are not affected.
Since $\models \NLNWgood{\sN}{\Sigma}$,
without loss of generality, 
let $\M_j = \alpha_j: \ENCan{\Gamma_j ; \Delta_j}$, then we have,
for any $\ell$,
$\namedP{\M_j}{P_j}{\alpha_j} \TRANS{\ell} \namedP{\M'_j}{P'_j}{\alpha_j}$,
where $\M'_j = \alpha_j : \ENCan{\Gamma'_j ; \Delta'_j}$. 
It makes
$\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \GQ{\RInfo'}{\GQueue'}$, 
so that $\mN \GTRANS{\ell} \mN'$.
Since $\mN'$ is a fully monitored network, 
its static part (i.e. the part when the global transport is taken off from $\mN'$),
say $\NP{P_i}{\alpha_i} \mid \ASET{\M_i}_{i \in I}$ where 
$\ASET{\M_i}_{i \in I} = 
\alpha_j : \ENCan{\Gamma'_j ; \Delta'_j}, 
\ASET{\alpha_i : \ENCan{\Gamma_i ; \Delta_i}}_{i \in I \setminus \ASET{j}}$,
$\models 
\NP{P_i}{\alpha_i} \mid \ASET{M_i}_{i \in I} : 
\Sigma'
$
where $\Sigma' = \alpha_j : \ENCan{\Gamma'_j ; \Delta'_j}, 
\ASET{\alpha_i : \ENCan{\Gamma_i ; \Delta_i}}_{i \in I \setminus \ASET{j}}$.
Thus we have $(\dN', \mN') \in R$. 
$\qedhere$
\end{enumerate}
\end{proof}

By Theorems~\ref{item:prop:globalsafe} and~\ref{item:prop:globaltrans}, 
we can {\em mix} unmonitored principals 
with monitored principals still obtaining global safety assurance:

\begin{COR}[Mixed Network] \label{cor:mixnet} \rm
If $\sN \mid \M$ is fully monitored with respect to $\Sigma$, 
$\models \NLNWgood{\sN'}{\Sigma}$, and 
$\principals{\sN} \cap \principals{\sN'} = \emptyset$,
then
$\models \NLNWgood{(\sN \mid \M) \mid \sN'}{\Sigma}$.
\end{COR}

In the above corollary, untyped $\sN$ is monitored by $\M$ which
specifies $\Sigma$, while $\sN'$ is unmonitored but statically checked
to conform to $\Sigma$. The result shows that they can safely be
composed.


