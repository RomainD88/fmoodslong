\section{Introduction}
Comprehensive verification of decentralised distributed systems,
i.e. systems where several units of computation communicate with each
other without relying on central coordination, is both a major
direction in information technology, with many direct examples in web
services~\cite{...}, manycores~\cite{FOS}, arrays of
sensors~\cite{OOI} and a challenging domain in computer
science. Indeed, it may seem \emph{a priori} difficult to ensure
methodically that a whole network of applications or services, which
may be written in different languages and running at different places
on different hardware, perform as described in a global specification
when the only way of controlling interactions is at the local
processes. Key properties of such networks are often associated with
arbitrarily complex interaction scenarios, which are hard to enforce
for general applications since these scenarios are formally
represented (if at all) only at the design stage in the form of
e.g. sequence diagrams.  Even though several existing works do exist,
they do not offer a comprehensive framework
for %% capturing, for example,
formal assurance of safety properties for a wide range of interaction
scenarios.  We need a formal foundation which underpins a general
top-down framework which can formally assure a general class of
properties for a wide range of applications.

% Something as simple as ensuring that one
% shopper using an online market received the required clearance from
% his bank before making a purchase is challenging if we are to 
% capture arbitrary such scenarios.
% We want s generally known as {\em choreography 
% Most existing solutions~\cite{...} have relied on
% \emph{ad hoc} mechanisms, designed toward specific, short-sighted
% goals. Hopefully, general top-down methods have been introduced.

\paragraph{Session Types} In the past ten years, the theory of 
\emph{multi-party session-types} (MPST)~\cite{HYC08,BettiniCDLDY08}
has proposed a unified framework for decentralised protocol
design. Interactions are considered through the point of view of
\emph{sessions} seen as unit of conversation. Agents in the networks,
called \emph{principals}, engage in multiple sessions, in which they
play different \emph{roles}. Figure~\ref{...} gives insight at the way
of proceeding: one starts by designing a global specification, called
\emph{global type} for a session by describing end-to-end
interactions, control flow (such as parallel execution, loop, choice)
as well as governance properties (such as required conditions to
perform a given action). An automated projection procedure split this
global type into several \emph{local types}, such that conformance by
all principals to the local types of the corresponding roles implies
the conformance of the whole network to the initial global
specification. There exist two ways to verify local conformance:
either by dynamically monitoring programs, using an external entity
which checks and approves messages sent to and by the principal, or by
statically checking the program against the local type. We propose in
this paper a new formal theory integrating monitors with 
inside the session-type theory.

\paragraph{Safety Assurance Framework} Many recent 
works~\cite{...} address static verification of programs (often abstracted in
the formal languages of $\pi$-calculi) with respect to session-type
specifications. We will not treat it in this paper, but will instead
focus on two entertwined axes: the theoretical formalisation of
networks, with monitored and unmonitored principals, asynchronous
global transports and routing operators and the elaboration of a
general, implementation-independent method for protocol design. The
latter includes the introduction of a protocol language
\emph{Scribble}, which allows one to write complex specifications in a
natural way. We provide tools performing projection to local types,
and automatic generation of monitors for verification of programs.
Our aim is to provide better support for heterogeneous distributed
systems by allowing each component to be independently implemented,
using different languages, libraries and programming techniques, as
well as being independently verified, either statically or
dynamically, while retaining the strong global safety expected of a
statically verified, homogenous system.  In Figure~\cite{...}, the
right-hand process is implemented in a language that supports static
session typing~\cite{HYC08}, and we can assume conformance to its
local protocol is verified this way. The other two processes, however,
are implemented in standard Java and Python using simple session
programming APIs (see \S~\ref{sec:design} for an example) and may not
be amenable to static typing. To ensure that these processes also
conform to the intended protocol and to protect the successfully
verified portion of the system, our framework uses network monitors to
dynamically verify their participation in the session. The monitors
are positioned at the network access points where all incoming and
outgoing messages to these processes can be observed and appropriate
actions can be taken to handle bad behaviour.

\paragraph{Integrated Theory} We present in this paper a theory which
capture the notion of distributed components and decentralised dynamic
verification centring on session types, with formal operational
semantics for programs, monitors, networks and specifications, type
systems, satisfaction relations and behavioural equivalences, as a
variant of the $\pi$-calculus, where routing and asynchronous
transport of messages are made explicit. Satisfactions relations allow
one to express formally when a whole asynchronous network actually
conforms to a global specification, equivalences give ways to equate
networks that are built differently, but offer the same services. We
prove in the following several properties: local safety states that a
monitored process respects its local protocol, i.e.\ that dynamic
verification by monitoring is sound, while local transparency states
that a monitored process has equivalent behaviour to an unmonitored
but well-behaved process, e.g.\ statically verified against the same
local protocol. Global safety states that a system satisfies the
global protocol provided that each each participant behaves as if
monitored. These properties both ensure coherence of our theory,
relating satisfaction relations and equivalences with the semantics
and the static validation procedure, and justify our implementation,
by giving strong formal basis to the Scribble suite.

\paragraph{OOI}

This work is motivated in part by our ongoing collaboration with the
Ocean Observatories Initiative (OOI) \cite{OOI}, a project to
establish cyberinfrastructure for the delivery, management and
analysis of scientific data from a large network of ocean sensor
systems. The architecture relies on the combination of high-level
conversation specifications (to express how the infrastructure
services should be used) and distributed run-time monitoring to
regulate the behaviour of third-party applications within the
system. A formally founded, distributed run-time monitoring mechanism
centred on conversations is needed for the system to reach its
requirements in terms of safety (conversation specifications are
accurately enforced), longevity (conversations are written down and
just have to be followed) and scalability (the enforcement is
decentralised). The theory presented in the paper offers a foundation
for its architecture, including an architectural framework and its
semantic properties, as well as monitoring and other algorithms which
are provably correct.  The paper shows how distributed applications of
OOI, implemented by a dynamically typed language Python, can be
runtime verified through a prototype monitoring architecture built
on the basis of the theory.
 
% Hence dynamic verification of communication sessions through
% network monitoring is essential for scalable means of integrating our
% framework into their infrastructure.

\paragraph{Contributions}

We summarise the technical contributions of this paper:

\begin{description}
\item[\REF{sec:examples}]
	illustrates the new safety assurance framework for distributed
applications and the underlying development framework,
illustrated through motivating examples. The framework
uses the Scribble
protocol language \cite{scribble}, an engineering language for protocol
specification based on MPST theory, that can then be
used for static or dynamic verification. The example shows how
we can use Scribble for protocol specifications and how we can
program using protocols and conversations.

\item[\REF{sec:dynamicmonitoring}]
	The design of a run-time verification and enforcement system
through a monitoring mechanism which efficiently enforces
behavioural conformance, including the satisfiability of
logical constraints over the content of the exchanged messages.

\item[\REF{sec:formal},\ref{sec:monitoring}] The formal foundation of
  the proposed framework given as a theory of the $\pi$-calculus with
  protocol-based run-time enforcement through monitors. We establish
  key properties of monitored networks, including local/global safety
  and transparency wrt. given specifications. In particular, we
  introduce behavioural equivalences theory that offer a
  compositional reasoning technique over monitored networks.

\item[\REF{sec:prototype}]
An implementation of the proposed safety assurance framework,
and an evaluation of its practicality by benchmark results.
\end{description}

We discuss related work in \REF{sec:related} and future work in
\REF{sec:conclusion}.  We have implemented existing Web service use
cases from \cite{...}  to demonstrate the expressiveness of Scribble.
These use cases and the full source code with benchmark results are
available from \cite{...}.


\begin{figure}[t]
\includegraphics[scale=0.4]{Framework_overview.png}
\caption{A framework for safe distributed infrastructures\label{fig:overview}}
\end{figure}


%% ===================================

%% %- there is a thing called multiparty session types
%% %- what is multiparty session types, why useful: current state of the art (static typing)
%% %- (don't motivate session types itself too much, this work is already done and accepted)
%% One of the main engineering challenges for distributed systems is
%% the comprehensive verification
%% %and governance
%% of distributed
%% %applications
%% software
%% without relying on ad-hoc and expensive testing techniques.
%% %
%% Multiparty session types (MPST) is a
%% typing discipline for communication programming originally developed
%% in the $\pi$-calculus~\cite{HYC08,BettiniCDLDY08} towards tackling this challenge.
%% As a
%% %As the main
%% verification method, the existing MPST systems focus on static
%% %, syntactic
%% type checking
%% of protocol specifications against endpoint process. The standard properties enjoyed by well-typed processes engaged in a
%% communication session are
%% communication safety (all processes conform to a globally agreed communication
%% protocol) and freedom from deadlocks.

%% %- background/motivation for this paper (what we should consider that the current state of the art in MPST doesn't yet; mainly going for practical reasons here---for application of session types ideas to practice)
%% %- static session typing typically designed for new languages (or rather new languages designed to support session typing); in practice, extensions are required for MPST to work with existing languages

%% Direct
%% %The
%% application of
%% %these ideas
%% the theoretical MPST techniques
%% to the current practice, however,
%% %is hindered by a few concerns.
%% faces a few obstacles.
%% Firstly, the type systems in these works are typically targetted at purpose-built process or object calculi with first class
%% %primitives
%% features
%% for treating linear communication channels and communication-oriented control flow. This means that corresponding extensions are needed to statically perform the same manner of syntactic session type checking in
%% the majority of
%% %all
%% %existing
%% mainstream engineering
%% languages.
%% Unfortunately, it is not easy to develop such extensions for all languages, either due to technical reasons or because the necessary features are not suited to the host language.
%% %(e.g. linear resource typing for a very liberal language like C).
%% Secondly,
%% %distributed systems are not just about processes at different locations, decoupled implementation work, heterogeneous systems (protocol is interface between heterogenous components).
%% the nature of distributed systems in practice is often not only decentralised in terms of the executable processes, but also in the sense that different languages and techniques may be used by separate parties to implement the various components of the system. Indeed, for domains, such as Web programming, where dynamically typed (or simply untyped) languages are popular, the introduction of static typing infrastructure
%% to support MPST
%% is not realistic.
%% %Another issue is the use of third-party libraries for which the source code is not available due to ...legal/legacy...
%% %
%% %- the second obstacle compounds the first because people want to use different languages, so we should make not just one new language but many new languages---not very "scalable"
%% %- it is natural and desirable to have heterogeneous systems, we want to be able to use different languages for differents parts of the system (that have different purposes and functionality)
%% %- the mpst framework in this paper is not about proposing one implementation language for everyone; it is about starting from one specification language, then going into different languages different with different accompanying verification techniques (although these can be reduced to just one general dynamic technique)
%% %- language extensions and new languages can be better for structured communications programming, but that's not the issue here...

%% %Large-scale distributed systems are however rarely amenable to static verification as a whole in practice, since they are usually built from heterogeneous components with either unsupported technologies (languages, libraries), or unavailable source code (legacy software, third-party components), or both.
%% % Moreover, some protocols by which components

%% %- first topic of this paper: dynamic session typing = formal and general network monitoring
%% %- get formal safety guaratees while immediately supporting existing languages, just to need to use familiar looking apis
%% %- second topic of this paper: framework for combining statically and dynamically verified endpoint code while retaining global safety assurance (dynamic complements static)

%% This paper studies two interrelated topics towards addressing the above issues. The first is the development of a dynamic verification technique for MPST through \emph{network monitoring of sessions}.
%% The second is a specification, implementation and verification framework for MPST that enables both static \emph{and} dynamic verification of communicating processes to be combined in one framework. The aim is to
%% %better support the decentralised nature of distributed application development
%% provide better support for heterogeneous distributed systems
%% by allowing each component to be independently implemented, using different
%% languages, libraries and programming techniques,
%% as well as being independently verified, either statically or dynamically,
%% while retaining
%% the strong global safety % and deadlock freedom
%% properties expected of a statically verified, homogenous system.
%% %
%% %- projection is the decentralisation tool; was just for distribution before, but now also for heterogeneous implementation and verification

%% This work is motivated in part by our ongoing collaboration with the Ocean Observatories
%% Initiative (OOI) \cite{OOI}, a project to establish cyberinfrastructure for
%%  the delivery,
%% management and analysis of scientific data
%% from a large network of ocean sensor systems.
%% %The infrastructure is
%% %planned for being active and used during the next 30 years, and should
%% %notably support distributed applications written by scientists.
%% The architecture
%% relies on the combination of high-level conversation specifications (to
%% express how the infrastructure services should be used) and distributed
%% run-time monitoring to regulate the behaviour of third-party applications
%% within the system. A formally founded, distributed run-time monitoring
%% mechanism
%% centred on conversations
%% is needed for the system to
%% reach its requirements in terms of safety (conversation
%% specifications are enforced), longevity (conversations are written down and
%% just have to be followed) and scalability (the enforcement is decentralised).
%% The applications of OOI are implemented by
%% a dynamically typed language, Python.
%% Hence dynamic verification of communication sessions through
%% network monitoring is essential for scalable means of
%% integrating our framework into their infrastructure.

%% %The OOI ...IT part of the project... is based on on ...stuff written
%% %in Python... However, the ...OOI project... has also identified that
%% %rigorous specification and verification of communication protocols
%% %based on MPST as a necessary element for such a large-scale
%% %distributed infrastructure to be robust. As Python is a dynamically
%% %typed language, dynamic verification of communication sessions through
%% %network monitoring is the more suited and scalable means of
%% %integrating our framework into their infrastructure.
%% %Appendix~\ref{...} gives a representative OOI use case expressed in
%% %our framework.

%% %We list the practical advantages of network monitoring for dynamic
%% %verification of session endpoints as follows:

%% %The practical advantages of network monitoring for dynamic
%% %verification of session endpoints follow:
%% %\begin{itemize}
%% %\item
%% %	Monitoring verifies distributed system components directly at the level of their concrete message passing actions. This enables verification of components implemented in languages or using techniques that are not amenable to static verification, or where the source code is not fully available (e.g.\ %external
%% %	pre-compiled
%% %	libraries). The same external monitor implementation can be used to verify components implemented in any language.
%% %	%
%% %	%- latter point is true provided the components are using the same session runtime protocols

%% %% interact can be
%% %% added, amended or removed during the lifetime of the system,
%% %% requiring (in the case of static verification) a specification
%% %% re-definition, followed by component re-certification.
%% %	% -- well, this is a bit the same for all monitors, the point here is
%% %        % our monitoring is the same as run-time \emph{typing}, it is part of
%% %        % a general typing system with formal properties---so what we're
%% %        % saying is that \emph{dynamic session typing} is good (because this
%% %        % is something not done in previous session types works)

%% %\item
%% %	Monitors serve to both protect the network from unsafe and malicious components and to protect the component from bad network I/O. A monitor can check that statically verified components are correctly deployed and have not been subsequently compromised.

%% %\item
%% %	Dynamic verification allows programmers to implement session-oriented software and obtain formal type safety guarantees through simple and familiar APIs in existing languages. This reduces the burden on both programmers using this framework, in terms of accessibility and ease of integration into existing development environments, and on the framework provider, in terms of tool chain support and because API design is lighter than a full language extension.
%% %\end{itemize}
%% %
%% %- advantages of monitors from session types view:
%% %- monitoring also gives security that statically pre-checked endpoint code doesn't (also monitoring can be controlled by the system/network administrator better than just separate programs)
%% %- monitoring is also a way to factor security concerns out of the endpoint code

%% %Another key point is that ...formal basis and rigourously strucured/guided starting from the same specification, regardless of the different eventual implementation languages and techniques, which allows global safety to be guaranteed for general applications despite independent implementation and verification... single global specification as starting point, but framework is general and flexible for local implementations ...general (not ad hoc) framework...

%% %[- mention some advantages of session types from view of related monitoring works: (or could leave to the related work)
%% %- session types for monitoring gives a formal and general framework (not ad hoc), decentralised verification of components
%% %- the key is automated global-to-local projection that still gives global safety]
%% %- more details about the formal work
%% %- contributions

%% %This paper presents a formal theory of the proposed framework and an implementation.  To prove the safety properties guaranteed by our framework, we develop a new formal model of heterogeneous networks of monitored
%% %%(dynamically verified)
%% %and unmonitored
%% %%(but well-behaved)
%% %processes. The key properties are \emph{local safety}, \emph{local transparency} and \emph{global safety}. Local safety states that a monitored process respects its local protocol, i.e.\ that dynamic verification by monitoring is sound, while local transparency states that a monitored process has equivalent behaviour to an unmonitored but well-behaved process, e.g.\ statically verified against the same local protocol. Global safety states that a system satisfies the global protocol provided that each each participant behaves as if monitored. Together, these properties justify our framework for decentralised verification by allowing monitored and unmonitored processes to be mixed while preserving protocol conformance for the entire network.
%% %%
%% %%- static type system not in this paper

%% %\subsection{Framework overview}
%% \paragraph{Framework and monitoring overview}
%% Figure~\ref{fig:overview} illustrates the proposed framework for a distributed application involving three participants. As
%% %the prior
%% standard in
%% MPST \cite{HYC08,BettiniCDLDY08}, the first stage is to specify the \emph{global protocol} that describes how the participants should interact
%% %to carry out the application.
%% from the global perspective of the system. The global protocol is then
%% mechanically \emph{projected} to generate \emph{local protocols} for
%% each participant, whithe key to ourch specify the communication behaviour expected
%% of each endpoint.
%% %In our framework, a global
%% %protocol is written in
%% %a choreography description language,
%% %\scribble~\cite{scribble}.
%% Each endpoint implementation is deployed as a
%% network process supported by the session communication runtime (a
%% lightweight middleware layer) for the relevant platform, e.g.~a Java
%% or Python session runtime.

%% %
%% %- global protocol is something general; the framework is general

%% In previous MPST systems, local protocols are used to statically type check endpoint processes implemented in a specific target language. In this paper, we
%% %expand the MPST framework to
%% choose to abstract from the details of type checking for specific languages. Instead, our framework simply considers an endpoint process as either statically verified before deployment, or else as a process that requires dynamic verification
%% %after deployment
%% during execution. The right-hand process in the figure is implemented
%% in a language that supports static session typing~\cite{HYC08},
%% and we can assume conformance to its local
%% protocol is verified this way. The other two processes, however, are
%% implemented in standard Java and Python using simple session
%% programming APIs (see \S~\ref{sec:design} for an example) and may not
%% be amenable to static typing. To ensure that these processes also
%% conform to the intended protocol and to protect the successfully
%% verified portion of the system, our framework uses network monitors to
%% dynamically verify their participation in the session. The monitors
%% are positioned at the network access points where all incoming and
%% outgoing messages to these processes can be observed and appropriate
%% actions can be taken to handle bad behaviour.

%% - mention monitoring framework is general and automatic from the global type and roles


%% \paragraph{Formal theory for the framework and monitoring}

%% ...need to make the usefulness of the properties more clear...

%% The key to our framework is that the underlying theory guarantees that
%% if each endpoint is independently verified to conform to their local
%% protocols, then the global protocol for the system is respected as a
%% whole. The contribution of this paper is to propose
%% a new formal model and bisimulation theories
%% of heterogeneous networks of monitored
%% %(dynamically verified)
%% and unmonitored
%% %(but well-behaved)
%% processes.
%% To cralify the safety properties guaranteed by our framework,
%% we establish three novel properties for dynamic monitoring,
%% \emph{local safety}, \emph{local
%%   transparency} and \emph{global safety}, which have
%% not been, as far as we have known, formalised and proved in the literature.
%% Local safety states that a
%% monitored process respects its local protocol, i.e.\ that dynamic
%% verification by monitoring is sound, while local transparency states
%% that a monitored process has equivalent behaviour to an unmonitored
%% but well-behaved process, e.g.\ statically verified against the same
%% local protocol. Global safety states that a system satisfies the
%% global protocol provided that each each participant behaves as if
%% monitored. Together, these properties justify our framework for
%% decentralised verification by allowing monitored and unmonitored
%% processes to be mixed while preserving protocol conformance for the
%% entire network. In addition, the formal model provides a compact and
%% viable reference semantics for implementations of our framework.


%% %%In this way, our framework is able assure global communication safety for the system through a combination of static and dynamic verification.
%% %...This is possible because the local protocols, used to flexibly guide and verify diverse implementations, are derived from the
%% %%uniform,
%% %central abstraction of the global protocol by projection that decomposes while preserving safety...
%% %%
%% %%- any strong statement about the system not being "corrupted" if a monitored process does something bad?
%% %%- monitor is not part of the runtime, it's an external ("asynchronous") monitor---we didn't really motivate *asynchronous* monitors for dynamic typing, only motivated dynamic typing

%% %The formal model provides a compact and viable reference semantics for
%% %implementations of our framework. Our implementation uses the Scribble
%% %protocol language \cite{scribble}, an engineering language for protocol
%% %specification based on MPST theory, with compatible session
%% %programming APIs for Java and Python. Scribble supports the
%% %description of interaction between session participants through
%% %asynchronous message passing sequences, protocol branches and
%% %recursion, and also logic-based annotations for fine-grained
%% %behavioural constraints \cite{BHTY10}, such as assertions on message
%% %values.  Our current session endpoint monitor implementation, written
%% %in Python, supports all of the above features. Our monitor projects
%% %the Scribble global protocol for the endpoint being monitored, and
%% %generates an FSM from the local protocol to verify the observed
%% %message trace. Performance benchmarks show that sessions can be
%% %dynamically verified by monitoring with little overhead.


%% \paragraph{Paper summary and contributions}

%% We summarise the technical contributions of this paper:

%% \begin{description}
%% \item[\REF{sec:examples}]
%% 	illustrates the new safety assurance framework for distributed
%% applications and the underlying development framework,
%% illustrated through motivating examples. The framework
%% uses the Scribble
%% protocol language \cite{scribble}, an engineering language for protocol
%% specification based on MPST theory, that can then be
%% used for static or dynamic verification. The example shows how
%% we can use Scribble for protocol specifications and how we can
%% program using protocols and conversations.

%% \item[\REF{sec:dynamicmonitoring}]
%% 	The design of a run-time verification and enforcement system
%% through a monitoring mechanism which efficiently enforces
%% behavioural conformance, including the satisfiability of
%% logical constraints over the content of the exchanged messages.

%% \item[\REF{sec:formal},\ref{sec:monitoring}]
%% The formal foundation of the proposed framework given as a
%% theory of the $\pi$-calculus with protocol-based run-time enforcement
%% through monitors. We establish key properties of
%% monitored networks, including local/global safety and transparency
%% wrt. given specifications. In particular, we introduce a
%% novel bisimulation (behavioural equivalence) theory that offers
%% a compositional reasoning technique over monitored networks.

%% \item[\REF{sec:prototype}]
%% An implementation of the proposed safety assurance framework,
%% and an evaluation of its practicality by benchmark results.
%% \end{description}

%% We discuss related work in \REF{sec:related} and future work in
%% \REF{sec:conclusion}.
%% We have implemented existing Web service use cases from \cite{...}
%% to demonstrate the expressiveness of Scribble.
%% These use cases and the full source code with benchmark results
%% are available from \cite{}.



%% \begin{comment}
%% Starting from a formal global protocol
%% specification through MPST as the central abstraction, localised behaviour
%% specifications for each endpoint are mechanically derived, which may be
%% used to statically verify each endpoint as well as assisting the
%% programming task. We believe that decentralisation of the implementation
%% \emph{and} verification concerns is necessary for practical distributed
%% system development, as a distributed system is often characterised
%% not only
%% by the
%% %% decomposition
%% distribution of its constituent parts, but also by the heterogeneous
%% nature of those parts. As such, our proposed framework is designed to
%% promote \emph{independent} verification of each component. This means
%% that each component may be independently implemented in different
%% languages using different libraries and programming techniques,
%% as well as getting verified,
%% while retaining
%% global safety % and deadlock freedom
%% properties (see \S~\ref{sub:sessionfidelityandsafety}). %% Moreover,
%% Independent verification allows different verification techniques,
%% both static and dynamic,
%% to be applied to each component, while
%% uniformly
%% assuring
%% safety properties for a system as a whole
%% (formally demonstrated through Theorem~\ref{ABCAAA} later).

%% Starting from a formal global protocol specification through MPST as the central abstraction, localised behaviour specifications for each endpoint are mechanically derived, which may be used to statically verify each endpoint independently as well as assisting the programming task. The term distributed system is often understood to mean a collaborative execution between separately located components.  a rigorous assurance framework for distributed systems must also support independent verification... Decentralisation of the development process and assurance for distributed systems must also
%% %
%% %In other words, the distributed system is characterised not only by the notion of component locality, but by the decentralisation of the system specification and implementation process while preserving the strong safety guarantees afforded by centralised static verification techniques. Safe decomposition in this manner is particularly important in distributed systems implementation, where the behaviour of the system as a whole crucially depends on the correct interaction between \emph{heterogeneous} components. ...Heterogeneity arises naturally in distributed systems in many ways...dynamic verification is best for distributed systems in many ways...
%% %
%% %For these reasons, we believe that an assurance framework for distributed application development must support component heterogeneity as a fundamental feature. This is the motivation for dynamic verification.
%% \end{comment}


