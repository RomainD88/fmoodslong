\section{Introduction}

%\paragraph{Monitoring asynchronous networks}
One of the main challenges in the engineering of distributed systems is the
comprehensive verification of distributed software without relying on
ad-hoc and expensive testing techniques.
%
Multiparty session types (MPST) is a typing discipline for
communication programming, that was originally developed in the $\pi$-calculus~
\cite{HYC08,BettiniCDLDY08,BHTY10,DY11,DY12,tcc:thesis} towards tackling
this challenge.
%%%%%%%%%%%%%%%%%%%
The idea is that applications are built starting from units of design
called sessions.  Each type of session, involving multiple roles, is
first modelled from a global perspective ({\em global type}) and then
projected onto {\em local types}, one for each role involved. As a
verification method, the existing MPST systems focus on static type
checking of endpoint processes against local types. The standard
properties enjoyed by well-typed processes are communication safety
(all processes conform to globally agreed communication protocols) and
freedom from deadlocks.

The direct application of the theoretical MPST techniques to the
current practice, however, presents a few obstacles.  First, the
existing type systems are targeted at calculi with first class
primitives for linear communication channels and
communication-oriented control flow; the majority of mainstream
engineering languages would need to be extended in this sense to be
suitable for syntactic type checking using session types.  Unfortunately, it is
not always straightforward to add these features to the specific host
languages. 
%(e.g. linear resource typing for a very liberal language like C).  
Furthermore, the executable processes in a distributed
system may be implemented in different languages.  Second, for
domains where dynamically typed or untyped languages are popular
(e.g., Web programming), or in multi-organisational scenarios, the
introduction of static typing infrastructure to support MPST may not
be realistic.

\paragraph*{\bf Development of Heterogeneous Systems based on MPSTs} 
This article proposes a theoretical framework addressing the issues discussed above, 
by supporting the combination of static \emph{and} dynamic verification of processes communicating
in a network. Fig.~\ref{fig:projectionoverview} illustrates the proposed framework. 
As standard in MPST~\cite{HYC08,BettiniCDLDY08}, the first stage is to specify a global protocol as a \emph{global type}, describing how the participants should interact in a multiparty session. 
%Many well-organised developments already have these global protocols
%in some similar form (e.g.\ message sequence charts).
\begin{figure*}
\begin{center}
\includegraphics[scale=0.4]{projection.pdf}
\caption{\label{fig:projectionoverview} 
Static/dynamic verification through
global types and projection}
\end{center}
\end{figure*}
The global type is then mechanically \emph{projected} to generate {local protocols}, as \emph{local types}, specifying the communication behaviour expected of each role in the session.
The global type in Fig.~\ref{fig:projectionoverview} involves three roles, yielding three local types upon projection. 
Next, each principal in a network implements one (or possibly more) local types. We call these implementations endpoint processes, or simply processes.  
We aim to capture the decentralised nature of distributed application
development, providing support for heterogeneous distributed
systems by allowing components to be independently implemented, using
different languages, libraries and programming techniques.
Assume that (1) the process on the right-hand side of Fig.~\ref{fig:projectionoverview} is implemented
in a language that supports static verification with session typing techniques, and that conformance to the implemented local type is verified this way, and 
%network process supported by the session communication runtime (a
%lightweight middleware layer) for the relevant platform, e.g.~a Java
%or Python session runtime.
(2) the other two processes are implemented in standard Java and Python, respectively, using simple session programming APIs and are not amenable to static typing. 
To ensure that the composition of these three processes conforms to the intended protocol we wrap the processes that cannot be statically verified with dedicated distributed {\em monitors}, that 
dynamically verify their participation in the session. 
In other words, our framework allows processes to be independently verified, either
statically during deployment, or dynamically during execution, while retaining the strong 
global safety properties of statically verified systems.

This work is motivated in part by our ongoing collaboration
with the Ocean Observatories Initiative (OOI) \cite{OOI}, a project to
establish cyberinfrastructure for the delivery, management and
analysis of scientific data from a large network of ocean sensor
systems.  Their architecture relies on the combination of high-level
protocol specifications (to express how the infrastructure services
should be used) and distributed run-time monitoring to regulate the
behaviour of third-party applications in the system. An implementation of the framework in Fig.~\ref{fig:projectionoverview} is currently integrated into the OOI infrastructure. In this implementation, processes are specified using Scribble~\cite{Scribble,YHNN2013,scribble10,HHNCDDY2012} (a practical incarnation of MPST) and processes are implemented in the Python programming language and dynamically monitored \cite{NYH2013,HNYD2013,DHHNY2015}.   

\paragraph*{\bf Monitored Networks} 
Networks are organised as follows: a group of principals run processes
communicating via asynchronous message passing; dedicated trusted
monitors (one for each principal) guard the run-time behaviour of both the environment and that principal, 
through the evaluation of incoming and outgoing messages. The aim is to protect the principal
from violations by other principals, and also to prevent the principal
from committing violation (this can be used e.g. for
debugging). Monitors regulate (1) the initiation, by principals, of
new sessions, each specified by a well-defined global type, and (2) the
movement of messages within each session.
Fig.~\ref{fig:implementation} illustrates the architecture of a network
with three principals ($\alpha 1$, $\alpha 2$ and $\alpha 3$); all
principals are monitored except $\alpha 3$, namely we assume the
processes run by $\alpha 3$ have been statically checked hence its
monitor can be switched-off (indeed all outgoing and incoming messages
can pass through without dynamic checking); each principal is
associated with one or more \emph{shared queues}, on which all other
principals can send invitations to join new sessions.  The messages
exchanged within a session are all associated to {\em one common
  session ID}, and the exchange of messages in a session is regulated
by verifying that the causality of messages follows the specification
(roughly, the ensemble of local types) of that session.  In Fig.~\ref{fig:implementation} each principal is associated with exactly one
shared queue, and we denote with $a i$ the queue associated with
$\alpha i$, with $i\in\{1,2,3\}$; principal $\alpha 1$ is currently
playing role \emph{Alice} in two sessions with session IDs $s$ and
$s'$, whereas $\alpha 2$ and $\alpha 3$ are playing \emph{Bob} and
\emph{Carol}, respectively, in just one session $s$ (e.g., the invitations to join
$s'$ have not yet been received by them).
%% \TZC{Note that, queues for session channels 
%% (e.g. $s[\text{Alice}]$, $s'[\text{Alice}]$, etc.)
%% are only generated once a session starts; 
%% while queues for shared channels
%% (e.g. $a 1$, $a_2$, etc.)
%% exist with their corresponding principal (e.g., $\alpha 1$, $\alpha 2$, etc.)
%% independently from which sessions are ongoing.
%% }
 \begin{figure*}
    \includegraphics[scale=0.6]{architecture}
    \caption{\label{fig:architecture}
    Architecture of monitored/unmonitored networks}
    \label{fig:implementation}
  \end{figure*}



\paragraph*{\bf A Formal Theory for Dynamic Verification}
Our theory is based on the idea that, if the endpoint processes in a
system are \emph{independently} verified (either statically or
dynamically) to conform a local type, then the corresponding global
protocol is respected \emph{as a whole}.
%In this article we show the conformance of verified processes to a \emph{specification}, which is the composition of local protocols. This, together with the semantic correspondence of global and local types given in~\cite{DY12} yields conformance to global protocols.} 
To this goal, we propose a new formal model and a bisimulation theory for heterogeneous networks of monitored and unmonitored processes. 
% Compared to \cite{TGC11}, where all local processes are
%dynamically verified through the protections of system monitors,
%% and thus the global safety and session fidelity of a network are dynamically ensured,
%this article integrates statically and dynamically verified local processes into one network,
%and formally states the theorems of the combination.
%%%%%%%%%%%%%%%%%%%%%%
%Our theory captures the notions of distributed components
%and decentralised dynamic verification centring on session types.

For the first time,  
we model dynamic verification based on types for the $\pi$-calculus. 
%
We provide an explicit account of the \emph{routing mechanism} that is 
implicitly present inside the MPST framework: in a session, messages
are sent to abstract roles (e.g.\ to a Seller), and a router (a
dynamically updated component of the network) translates these roles
into actual addresses. 

%\lau{ROMAIN NEEDS TO PUT AN EMPHASIS}
Our approach also aims at giving a semantical equivalence 
for a collection of protocols (and networks), by reaching a formal criterion
for equating services. By taking the routing feature into account when
designing novel equivalences, our formal model can relate networks
built in different ways (through different distributions or
relocations of services) but offering the same \emph{interface} to an
external observer. The router, being in charge of associating roles
with principals, hides to an external user the internal composition of
a network: what distinguishes two networks is not their structure but
the services they are able to provide, or more precisely, the local
types they offer to the outside. We prove that bisimulation is compositional (Proposition~\ref{prop:sim:cong}) and that equivalent networks satisfy the same  specification (Proposition~\ref{pro:cong:satisfaction}).

We formally define a satisfaction relation to express when the
behaviour of a network conforms to a global specification and we prove
a number of properties of our model: 
%
{\em local safety} (Theorem \ref{item:prop:localsafe}) states that a monitored process respects its local protocol, i.e.\ that dynamic verification by monitoring is sound; 
%
{\em global safety} (Theorem \ref{item:prop:globalsafe}) extends local safety to networks involving multiple principals; 
%states that a system
%satisfies the specification, provided that each participant behaves
%as if monitored; 
%
{\em local transparency} (Theorem \ref{item:prop:localtrans})
states that a monitored process has equivalent behaviour to an 
unmonitored but well-behaved (e.g.~statically typed) process; and 
%
{\em global transparency} (Theorem \ref{item:prop:globaltrans}) 
states that a network where each principal is monitored has equivalent behaviour to an unmonitored but well-behaved network.  

Finally, we introduce a stronger property than global safety, {\em
  session fidelity} (Theorem \ref{thm:sf:monitored}), which not only
guarantees conformance of each monitored process in a network to the
ensemble of local specifications, but also requires that the overall
flow of messages throughout the router is correct.  
%% For instance, by
%% session fidelity if a role sends a value then the receiver role of the
%% same interaction will receive the same value. 
In this way, session fidelity shows the correspondency between the
behaviour of a monitored system and the behaviour specified by a global
protocol.
%Session fidelity states that as all message flows of a network satisfy
%  global specifications, whenever the network changes because some
%  local processes take actions, all message flows continue to satisfy
%  global specifications.}  
Together, these properties justify our
framework for decentralised verification by allowing monitored and
unmonitored processes to be safely mixed while preserving protocol
conformance for the entire network.
%%%%%%%%%%%%%%%%%%%%%%
Technically, these properties also ensure the coherence of our theory,
by relating the satisfaction relations with the semantics and static
validation procedures.
%, and support our implementation, by
%giving a strong formal basis to
%%strengthening the formal basis of
%the Scribble suite.

%%In this way, our framework is able assure global communication
%%safety for the system through a combination of static and dynamic
%%verification.
%...This is possible because the local protocols, used to flexibly guide and verify diverse implementations, are derived from the
%%uniform,
%central abstraction of the global protocol by projection that decomposes while preserving safety...
%%
%%- any strong statement about the system not being "corrupted" if a monitored process does something bad?
%%- monitor is not part of the runtime, it's an external ("asynchronous") monitor---we didn't really motivate *asynchronous* monitors for dynamic typing, only motivated dynamic typing

%The formal model provides a compact and viable reference semantics for
%implementations of our framework. Our implementation uses the Scribble
%protocol language \cite{scribble}, an engineering language for protocol
%specification based on MPST theory, with compatible session
%programming APIs for Java and Python. Scribble supports the
%description of interaction between session participants through
%asynchronous message passing sequences, protocol branches and
%recursion, and also logic-based annotations for fine-grained
%behavioural constraints \cite{BHTY10}, such as assertions on message
%values.  Our current session endpoint monitor implementation, written
%in Python, supports all of the above features. Our monitor projects
%the Scribble global protocol for the endpoint being monitored, and
%generates an FSM from the local protocol to verify the observed
%message trace. Performance benchmarks show that sessions can be
%dynamically verified by monitoring with little overhead.

  Our theory is more involved than most of the existing works in the
  domain of session verification~\cite{HYC08} as, for the first time,
  both networks and monitoring are made explicit. Our abstract model
  for session networks describe the evolution of the network at a
  lower-level; for instance, we introduce dynamic update of routing
  information: a participant taking part in a session does not send a
  message to another participant, but sends a message to a role which
  is then routed by the networks to the corresponding participant.

\paragraph{\bf Contributions and Outline}
This work is an extended version of~\cite{BocchiCDHY13} that
includes: the definitions omitted in~\cite{BocchiCDHY13}, additional
examples, and full proofs. Specifically, we extended~\cite{BocchiCDHY13} by including the
following additional material:
\begin{itemize}
\item the formal definition of monitorability, a consistency condition
  on global types, together with a discussion on its relevance and a
  statement of its decidability (\REF{sec:monitorability});
\item the detailed definitions, full formal statement and proofs 
of session fidelity and its relationship with
  global safety (in this introduction, \REF{sec:safety} and
  \REF{sec:fidelity}), which is only outlined in \cite{BocchiCDHY13};
\item a simpler but less restrictive semantics of networks 
(e.g., a principal is now allowed to engage as different participants in the same session); 
\item a detailed formalisation for behavioural equivalences (\REF{sec:theory})
\item a formal statement on global safety in mixed (i.e., monitored \emph{and} unmonitored) networks (Corollary~\ref{cor:mixnet});
\end{itemize}
\REF{sec:formal} and \REF{sec:calculus} introduce the formalisms for
protocol specifications and networks, respectively. \REF{sec:calculus}
provides a formal framework for monitored networks based on
$\pi$-calculus processes and protocol-based run-time enforcement
through monitors.
 % 
\REF{sec:monitoring} introduces: a semantics for specifications
(\REF{sec:envs}), a novel behavioural theory for compositional
reasoning over monitored networks through the use of equivalences
(bisimilarity and barbed congruence) and the satisfaction relation
(\REF{sec:RT}).
%
Local and global safety are stated and proved in~\REF{sec:safety},
transparency in~\REF{sec:transparency}, and session fidelity
in~\REF{sec:fidelity}.
%
Related works are discussed in~\REF{sec:related} and future works in~\REF{sec:conclusion}. 

%the Appendices~\REF{app:safety} and
%We have also implemented existing Web service use cases from~\cite{Jboss}
%to demonstrate the expressiveness of Scribble.
%These use cases and the full source code with benchmark results
%are also available from \cite{SeTMon}.

\begin{comment}
Starting from a formal global protocol
specification through MPST as the central abstraction, localised behaviour
specifications for each endpoint are mechanically derived, which may be
used to statically verify each endpoint as well as assisting the
programming task. We believe that decentralisation of the implementation
\emph{and} verification concerns is necessary for practical distributed
system development, as a distributed system is often characterised
not only
by the
%% decomposition
distribution of its constituent parts, but also by the heterogeneous
nature of those parts. As such, our proposed framework is designed to
promote \emph{independent} verification of each component. This means
that each component may be independently implemented in different
languages using different libraries and programming techniques,
as well as getting verified,
while retaining
global safety % and deadlock freedom
properties (see \REF{sub:sessionfidelityandsafety}). %% Moreover,
Independent verification allows different verification techniques,
both static and dynamic,
to be applied to each component, while
uniformly
assuring
safety properties for a system as a whole
(formally demonstrated through Theorem~\ref{ABCAAA}.\ref{item:prop:deux} later).

Starting from a formal global protocol specification through MPST as
the central abstraction, localised behaviour specifications for each
endpoint are mechanically derived, which may be used to statically
verify each endpoint independently as well as assisting the
programming task. The term distributed system is often understood to
mean a collaborative execution between separately located components.
a rigorous assurance framework for distributed systems must also
support independent verification... Decentralisation of the
development process and assurance for distributed systems must also
%
%In other words, the distributed system is characterised not only by the notion of component locality, but by the decentralisation of the system specification and implementation process while preserving the strong safety guarantees afforded by centralised static verification techniques. Safe decomposition in this manner is particularly important in distributed systems implementation, where the behaviour of the system as a whole crucially depends on the correct interaction between \emph{heterogeneous} components. ...Heterogeneity arises naturally in distributed systems in many ways...dynamic verification is best for distributed systems in many ways...
%
%For these reasons, we believe that an assurance framework for distributed application development must support component heterogeneity as a fundamental feature. This is the motivation for dynamic verification.
\end{comment}


\begin{comment}
Comprehensive verification of decentralised distributed systems,
i.e. systems where several units of computation communicate with each
other without relying on central coordination, is both a major
direction in information technology, with many direct examples in web
services~\cite{...}, manycores~\cite{FOS}, arrays of
sensors~\cite{OOI} and a challenging domain in computer
science. Indeed, it may seem \emph{a priori} difficult to ensure
methodically that a whole network of applications or services, which
may be written in different languages and running at different places
on different hardware, perform as described in a global specification
when the only way of controlling interactions is at the local
processes. Key properties of such networks are often associated with
arbitrarily complex interaction scenarios, which are hard to enforce
for general applications since these scenarios are formally
represented (if at all) only at the design stage in the form of
e.g. sequence diagrams.  Even though several existing works do exist,
they do not offer a comprehensive framework
for %% capturing, for example,
formal assurance of safety properties for a wide range of interaction
scenarios.  We need a formal foundation which underpins a general
top-down framework which can formally assure a general class of
properties for a wide range of applications.

% Something as simple as ensuring that one
% shopper using an online market received the required clearance from
% his bank before making a purchase is challenging if we are to
% capture arbitrary such scenarios.
% We want s generally known as {\em choreography
% Most existing solutions~\cite{...} have relied on
% \emph{ad hoc} mechanisms, designed toward specific, short-sighted
% goals. Hopefully, general top-down methods have been introduced.

\paragraph{Session Types} In the past ten years, the theory of
\emph{multi-party session-types} (MPST)~\cite{HYC08,BettiniCDLDY08}
has proposed a unified framework for decentralised protocol
design. Interactions are considered through the point of view of
\emph{sessions} seen as unit of conversation. Agents in the networks,
called \emph{principals}, engage in multiple sessions, in which they
play different \emph{roles}. Fig.~\ref{...} gives insight at the way
of proceeding: one starts by designing a global specification, called
\emph{global type} for a session by describing end-to-end
interactions, control flow (such as parallel execution, loop, choice)
as well as governance properties (such as required conditions to
perform a given action). An automated projection procedure split this
global type into several \emph{local types}, such that conformance by
all principals to the local types of the corresponding roles implies
the conformance of the whole network to the initial global
specification. There exist two ways to verify local conformance:
either by dynamically monitoring programs, using an external entity
which checks and approves messages sent to and by the principal, or by
statically checking the program against the local type. We propose in
this article a new formal theory integrating monitors with
inside the session-type theory.

\paragraph{Safety Assurance Framework} Many recent
works~\cite{...} address static verification of programs (often abstracted in
the formal languages of $\pi$-calculi) with respect to session-type
specifications. We will not treat it in this article, but will instead
focus on two intertwined axes: the theoretical formalisation of
networks, with monitored and unmonitored principals, asynchronous
global transports and routing operators and the elaboration of a
general, implementation-independent method for protocol design. The
latter includes the introduction of a protocol language
\emph{Scribble}, which allows one to write complex specifications in a
natural way. We provide tools performing projection to local types,
and automatic generation of monitors for verification of programs.
Our aim is to provide better support for heterogeneous distributed
systems by allowing each component to be independently implemented,
using different languages, libraries and programming techniques, as
well as being independently verified, either statically or
dynamically, while retaining the strong global safety expected of a
statically verified, homogenous system.  In Fig.~\cite{...}, the
right-hand process is implemented in a language that supports static
session typing~\cite{HYC08}, and we can assume conformance to its
local protocol is verified this way. The other two processes, however,
are implemented in standard Java and Python using simple session
programming APIs (see \REF{sec:design} for an example) and may not
be amenable to static typing. To ensure that these processes also
conform to the intended protocol and to protect the successfully
verified portion of the system, our framework uses network monitors to
dynamically verify their participation in the session. The monitors
are positioned at the network access points where all incoming and
outgoing messages to these processes can be observed and appropriate
actions can be taken to handle bad behaviour.

\paragraph{Integrated Theory} We present in this article a theory which
capture the notion of distributed components and decentralised dynamic
verification centring on session types, with formal operational
semantics for programs, monitors, networks and specifications, type
systems, satisfaction relations and behavioural equivalences, as a
variant of the $\pi$-calculus, where routing and asynchronous
transport of messages are made explicit. Satisfactions relations allow
one to express formally when a whole asynchronous network actually
conforms to a global specification, equivalences give ways to equate
networks that are built differently, but offer the same services. We
prove in the following several properties: local safety states that a
monitored process respects its local protocol, i.e.\ that dynamic
verification by monitoring is sound, while local transparency states
that a monitored process has equivalent behaviour to an unmonitored
but well-behaved process, e.g.\ statically verified against the same
local protocol. Global safety states that a system satisfies the
global protocol provided that each each participant behaves as if
monitored. These properties both ensure coherence of our theory,
relating satisfaction relations and equivalences with the semantics
and the static validation procedure, and justify our implementation,
by giving strong formal basis to the Scribble suite.

\paragraph{OOI}

This work is motivated in part by our ongoing collaboration with the
Ocean Observatories Initiative (OOI) \cite{OOI}, a project to
establish cyberinfrastructure for the delivery, management and
analysis of scientific data from a large network of ocean sensor
systems. The architecture relies on the combination of high-level
conversation specifications (to express how the infrastructure
services should be used) and distributed run-time monitoring to
regulate the behaviour of third-party applications within the
system. A formally founded, distributed run-time monitoring mechanism
centred on conversations is needed for the system to reach its
requirements in terms of safety (conversation specifications are
accurately enforced), longevity (conversations are written down and
just have to be followed) and scalability (the enforcement is
decentralised). The theory presented in the article offers a foundation
for its architecture, including an architectural framework and its
semantic properties, as well as monitoring and other algorithms which
are provably correct.  The article shows how distributed applications of
OOI, implemented by a dynamically typed language Python, can be
runtime verified through a prototype monitoring architecture built
on the basis of the theory.

% Hence dynamic verification of communication sessions through
% network monitoring is essential for scalable means of integrating our
% framework into their infrastructure.


\paragraph{Contributions}

We summarise the technical contributions of this article:

\begin{description}
\item[\REF{sec:examples}]
	illustrates the new safety assurance framework for distributed
applications and the underlying development framework,
illustrated through motivating examples. The framework
uses the Scribble
protocol language \cite{scribble}, an engineering language for protocol
specification based on MPST theory, that can then be
used for static or dynamic verification. The example shows how
we can use Scribble for protocol specifications and how we can
program using protocols and conversations.

\item[\REF{sec:dynamicmonitoring}]
	The design of a run-time verification and enforcement system
through a monitoring mechanism which efficiently enforces
behavioural conformance, including the satisfiability of
logical constraints over the content of the exchanged messages.

\item[\REF{sec:formal},\ref{sec:monitoring}] The formal foundation of
  the proposed framework given as a theory of the $\pi$-calculus with
  protocol-based run-time enforcement through monitors. We establish
  key properties of monitored networks, including local/global safety
  and transparency wrt given specifications. In particular, we
  introduce behavioural equivalences theory that offer a
  compositional reasoning technique over monitored networks.

\item[\REF{sec:prototype}]
An implementation of the proposed safety assurance framework,
and an evaluation of its practicality by benchmark results.
\end{description}

We discuss related work in \REF{sec:related} and future work in
\REF{sec:conclusion}.  We have implemented existing Web service use
cases from, e.g.~\cite{...} to demonstrate the expressiveness of
Scribble.
These use cases and the full source code with benchmark results are
available from \cite{SeTMon}.
The full proofs and more examples and related work can be found in
the full version.

\end{comment}


