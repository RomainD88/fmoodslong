\section{Session Fidelity}
\label{app:fidelity} \label{sec:fidelity}

The property of session fidelity says that, whenever all the
principals in a static network conform to their specifications, then
all of the derivatives of this static network conform to evolutions of
the initial global specification.

\paragraph*{\bf Global Safety vs Session Fidelity}
Recall that global safety (Definition \ref{def:globalsafe}) only
ensures that in a network where principals are well-behaved with
respect to their local types, all interactions conform to the
collection of these local types. Session fidelity is a stronger
property than global safety (Definition \ref{def:globalsafe}) as
illustrated in Example~\ref{ex:fidelitybetter}.
%and the theorem of {\em global transparency} (Theorem \ref{item:prop:globaltrans})
%says that, also from the local point of view, a network whose endpoints are monitored
%is bisimilar to the one whose endpoints are well-behaved to specifications;
\begin{EX} \label{ex:fidelitybetter} \rm
Consider a simple global type
\[
\begin{array}{rcl}
\GA = \GInter{\ROLE{r}_1}{\ROLE{r}_2} : \ASET{l_1(x_1)\ASET{ x_1>9}. \GA_1, l_2(x_2)\ASET{x_2<10}. \GA_2} 
\end{array}
\]
and processes $\PRG$ and $\PRGQ$ implementing roles $\ptp{r}_1$ and $\ptp{r}_2$ in established session $s$
\[
\begin{array}{lll}
\PRG & = & \SELECTdef{s}{\ROLE{r}_1}{\ROLE{r}_2}{l_1}{10} . \PRG'\\
\PRGQ & = & \BRANCHcp{s}{\ROLE{r}_1}{\ROLE{r}_2}{l_i}{x_i}{\PRGQ_i}{{i \in \ASET{1,2}}}
\end{array}
\]
Suppose that during runtime $\PRG$ sends out message $s \ENCan{\ROLE{r}_1, \ROLE{r}_2, l_1 \ENCan{10}}$
but, $\PRGQ$ receives a message, perhaps revised by an attack, 
$s \ENCan{\ROLE{r}_1, \ROLE{r}_2, l_2 \ENCan{8}}$.
These actions satisfy global safety since satisfy the specifications of $\PRG$ and $\PRGQ$, namely 
they are {\em locally} well-behaved.
This scenario (i.e., the content of the message being modified between a send and a corresponding receive action) does not conform to the intended global protocol. We define a property, session fidelity, that rules out the scenario above (Definition \ref{def:sf}) and prove (Theorem \ref{thm:sf:monitored}) that fully monitored networks with global transport satisfy session fidelity. 
This is due to the fact that: (1) $s$ is a private session ID that can be viewed only by the participants in the session, (2) all principals are guarded by monitors hence all messages reaching the global transport are valid, and (3) the global transport preserves the values of the messages it gets. 
%In other words, the case of Example \ref{ex:fidelitybetter} will not happen
%since we assume that no attack knows a secret session ID
%and all local endpoints are guarded by monitors. 
%However, the action of receiving $s \ENCan{\ROLE{r}_1, \ROLE{r}_2, l_2 \ENCan{8}}$
%does not satisfy session fidelity since it violates the derivatives of the global specification.
\end{EX}
%while session fidelity, from the global point of view, gives a {\em dynamical} interpretation of 
%{\em global} specification conformance
%through capturing the dynamical changes of processes and specifications.
%In other words, the property of session fidelity says that
%every interaction among multiple endpoints
%progresses exactly as what global session types (i.e. specification) describes.

%the rules that chatacterize the dynamic movements 
%of messages in the network 
%(i.e. messages leave/enter the global queue/principal(s)).


\paragraph*{\bf Configurations} We define session fidelity after giving 
the labelled transition relation for \emph{configurations}, and a few auxiliary definitions.  
%routing tables, and the properties of consistency and conformance of specifications,
%then we state auxiliary definition and lemmas for
%{\em  receivability, consistency and conformance} of configurations.

\begin{DEF}[Configuration] \label{monitor:def:configuration}
A configuration is denoted by $\Phi=\Sigma; \GQ{\RInfo}{\GQueue}$, 
where 
%the group of monitors correspond to $\GQueue$.
%In other words, 
all messages corresponding to the actions
guarded by $\Sigma$ are in $\GQueue$.
\end{DEF}
A configuration guides the global behaviours in a network.  
\LB{By including the global queue, we let configuration capture the \emph{global} behaviour in a network, 
which accounts also for the correct routing and dispatch of messages.} 
\LB{Before giving the semantics of configurations, it will be useful to define when and how configurations can be composed.}
Let $\principals{\Phi}$ be the set of principals involving in $\Phi$. 
\begin{DEF}[Parallel composition of configurations] \label{def:CFComposable} \rm
Let $\Phi_1=\Sigma_1 ; \GQ{\RInfo_1}{\GQueue_1}$ and 
$\Phi_2=\Sigma_2\ ;\ \GQ{\RInfo_2}{\GQueue_2}$ be configurations. 
We say that $\Phi_1$ and $\Phi_2$ are {\em composable} whenever
$\principals{\Phi_1}\cap\principals{\Phi_2}=\emptyset$ and 
the union of their routing tables remains a function. 
%and $\Sigma_1$ and $\Sigma_2$ are composable.  %(they are always composable) 
If $\Phi_1$ and $\Phi_2$ are composable, then we define the composition of $\Phi_1$ and $\Phi_2$ as:
$\Phi_1 \odot \Phi_2 = 
\Sigma_1, \Sigma_2\ ;\ 
\GQ{\RInfo_1\cup \RInfo_2}{\GQueue_1\CD \GQueue_2}$.
%\LB{Note that, since $\principals{\Phi_1}\cap\principals{\Phi_2}=\emptyset$ then considering $\GQueue_1\CD \GQueue_2$ is equivalent to 
%considering $\GQueue_2\CD \GQueue_1$.}

The formal semantics of configurations is defined by the LTS in Fig. \ref{fig:LTSCF}. 
% has been introduced in the LTS for monitored networks, 
% defined in Fig. \ref{fig:monitor:network:LTS}.
\input{fig_LTSConfiguration}
The behaviour of each principal in a network is guided by the
specification $\Sigma$, and is observed by the global transport
$\GQ{\RInfo}{\GQueue}$. Except rules $\mbox{[Acc]}$ and $\mbox{[Par]}$, all rules are straightforward
from the LTS of specifications (defined in Section \ref{sec:envs})
%(Fig. \ref{fig:LTS:spec:unlinked})
and the one of dynamic networks (Fig. \ref{fig:NW:LTS}). We comment below on the interesting rules.
\begin{enumerate}
\item 
Rule $\mbox{[Acc]}$ indicates that, only when the invitation has been
(internally) accepted by a principal in the network, the routing
information registers $s[\rr] \mapsto \alpha$.  When we observe the
global transport (externally), we only observe that an invitation is
moved out from the global queue (which implies that it has been
accepted). However, we do not know {\em who} accepts it.  Only
$\Sigma$ tells which principal accepts this invitation, 
so that we can register it in the routing information using $\alpha$. 
\item
Rule $\mbox{[Par]}$ says if
$\Phi_1$ and $\Phi_3$ are composable 
(Definition \ref{def:CFComposable}), after $\Phi_1$ becomes as $\Phi_2$, they are
still composable.
\end{enumerate}




\begin{comment}
\begin{DEF}[Full satisfaction (network satisfaction)]
\label{def:full}
  A relation $\CAL{R}$ from specifications to networks is a
  {\em full satisfaction} if, whenever
  $\OSPECi{0}\CAL{R}\dN_0$, the following conditions hold:
  \begin{enumerate}
  \item\label{case:satisfd:one} If $\OSPECi{0}\TRANS{\ell}\OSPECi{1}$ for
    an output $\ell$, $\dN_0\TAUTRANS{\TZC{\dual(\ell)}}\dN_1$ s.t.
    $\OSPECi{1}\CAL{R}\dN_1$.
  \item \label{case:satisfd:two}
If $\dN_0\TRANS{\ell}\dN_1$ for an output
%%%%% input
    $\ell$, $\destination{\ell}\in\dom{\OSPECi{0}}$
    and $\OSPECi{0}\TAUTRANS{\dual(\ell)}\OSPECi{1}$ s.t.
    $\OSPECi{1}\CAL{R}\dN_1$.
  \item If $\dN_0\TRANS{\tauact}\dN_1$ then $\OSPECi{0}\CAL{R}\dN_1$.
  \end{enumerate}
  When $\OSPEC\CAL{R}\dN$ for a full satisfaction $\CAL{R}$,
  we say {\em $\dN$ satisfies $\OSPEC$}, writing $\models
  \NWgood{\OSPEC}{\dN}$
\end{DEF}

Note that $\Sigma \TRANS{\ell} \Sigma'$ corresponds to 
$\GQ{\RInfo}{\GQueue} \TRANS{\dual(\ell)} \GQ{\RInfo'}{\GQueue'}$
because the viewpoints of local and global are dual:
when a local process (which obeys local specification, i.e. $\Sigma$) 
{\em outputs} a message {\em to} the global queue,
from the viewpoint of global queue, it {\em inputs} a message.
Similarly, when a local process {\em inputs} a message {\em from} the global queue,
from the viewpoint of global queue, it {\em outputs} a message.

To simplify and clarify the relationship above, we use {\em configuration}, 
the pair of $\Sigma$ and $\GQ{\RInfo}{\GQueue}$,
to illustrate the basic idea of session fidelity.
Configuration is written $\Phi=\Sigma; \GQ{\RInfo}{\GQueue}$, 
which guides and captures the behaviours {\em inside} 
the network.  
The labelled transition relations of $\Phi$ (configurations)
are defined in Fig. \ref{fig:LTSCF}.  
\end{comment}
\end{DEF}

\LB{Our framework relies on two structural (well-formedness) properties on specifications: \emph{consistency} and \emph{coherence}.} \RD{Consistent specifications are the ones corresponding to well-formed concrete systems }\LB{(i.e., where the session initiation procedures are well-regulated, and where the active sessions correspond to projections of some well-formed global type).}
\begin{DEF}[Consistent and coherent specifications] \label{def:consistent} \label{monitor:def:groupMconsistent} 
$\Sigma = \{\AT{\alpha_i}{\langle\Gamma_i;\Delta_i\rangle}\}_{i \in I}$ is {\em consistent} when
\begin{enumerate}
\item there is \TZ{one and only one} 
  %at most one 
  $i$ such that $\Gamma_i \vdash
  a:\Imode(T[\rr])$, and
\item as long as $a:\Omode(T[\rr])$ exists in some $\Gamma_i$,
$\exists \Gamma_j$ such that $a:\Imode(T[\rr]) \in \Gamma_j$; and
% ^{\capabilities}$ with $\mathtt{I}\in\capabilities$,
%  $\capabilities = \mathtt{I}
%  \uplus \capabilities'$,
\item for any $s$ appearing in any $\Delta_j$, 
if $\{ s[\rr_k]:T_k \}_{1 \leq k \leq n}$ is a collection 
appeared in $\{ \Delta_i \}_{i \in I}$, 
there exists well-formed 
  $\GA$ such that $\ROLES{\GA}=\ASET{\rr_1, .., \rr_n}$
and $\Proj{\GA}{\rr_i}=T_i$. 
\end{enumerate}
Two specifications $\Sigma_1$ and $\Sigma_2$ are {\em coherent} when
their union is a consistent specification.
\end{DEF}

Next, we define {\em receivability}, {\em configurational consistency} and {\em conformance} for configurations, which are based on the LTS of configurations and dynamic networks. \RD{Receivability entices the ability for a message in transit to reach its destination.}
\begin{DEF}[Receivable configuration]
Receivability of a configuration $\Sigma;\GQ{\RInfo}{\GQueue}$ is defined 
by the following induction:
\begin{enumerate}
\item If $\GQueue$ is empty then $\Sigma;\GQ{\RInfo}{\GQueue}$ is
  receivable. %% to $\Sigma$,
\item If % $\GQueue$ is non-empty and 
  $\GQueue\LITEQ m\CD \GQueue'$, 
  then $\Sigma;\GQ{\RInfo}{\GQueue}$ is receivable
  when we have $\Sigma; \GQ{\RInfo}{m\CD \GQueue'}
  \GTRANS{\ell} \Sigma';\GQ{\RInfo'}{\GQueue'}$, 
  where $\ell$ corresponding to $m$, 
  and $\Sigma';\GQ{\RInfo'}{\GQueue'}$ is receivable. %% to $\Sigma'$.
\end{enumerate}
\end{DEF}

  A configuration $\Sigma;\GQ{\RInfo}{\GQueue}$ 
  is {\em configurationally consistent} if all of its 
  multi-step global input transition derivatives 
  can be performed and the resulting specifications $\Sigma$ is consistent (according to Definition \ref{monitor:def:groupMconsistent}).

\begin{DEF}[Configurational consistency] \label{def:congconsist} \rm 
  A configuration $\Phi=\Sigma; \GQ{\RInfo}{\GQueue}$ is
  {\em configurationally consistent} whenever
\begin{enumerate}
\item $\GQueue$ is empty and $\Sigma$ is consistent, or
\item $\GQueue$ is not empty, $\Sigma;\GQ{\RInfo}{\GQueue}$ is receivable, and after receiving all messages in $\GQueue$ 
  with $\Sigma \TRANS{\ell_1  \ldots \ell_n} \Sigma'$ (by the LTS in Figure~\ref{fig:LTS:spec:unlinked}), 
  where $\ell_i, i=\ASET{1, ..., n}$ are inputs
  and, $\forall \globalm \in \GQueue$,  
  $\exists \ell \in \ell_1  \ldots \ell_n$
  such that $\ell$ corresponds to $\globalm$, 
  we have $\Sigma'$ is consistent.
\end{enumerate}
\end{DEF}
\NI In other words, $\Sigma;\GQ{\RInfo}{\GQueue}$ 
  is configurationally consistent if, in each of its derivatives, 
  all messages in the transport can be ``received'' 
  by some monitors in $\Sigma$ and, after absorbing all these messages,
  the resulting $\Sigma'$ is still consistent. \RD{Conformance links networks and configurations.}
 

\begin{DEF}[Conformance to a configuration]\label{def:qconform} \rm
  Assume a network 
  $\dN\Cong \sN \mid \GQ{\RInfo}{\GQueue}$ is given.
  We say that $\dN$ conforms to $\Sigma;\GQ{\RInfo}{\GQueue}$ when:

  \begin{enumerate}
  \item $\GQueue$ is empty, 
   $\models \NLNWgood{\sN}{\Sigma}$ and 
   $\Sigma$ is consistent, or 

   \item $\GQueue$ is not empty, and the following conditions hold
   \begin{enumerate}
   \item $\models \NLNWgood{\sN}{\Sigma}$, 
   \item all messages in $\GQueue$ are receivable to $\sN$, and  
   \item as $\Sigma; \GQ{\RInfo}{\GQueue} 
      \GTRANS{{\ell_1}  \ldots {\ell_n}} 
      \Sigma' ; \GQ{\RInfo'}{\emptyset}$ so that
      $\sN \mid {\GQueue} \GTRANS{\ell_1  \ldots \ell_n}
      \sN' \mid {\emptyset}$ 
    where each $\ell_i, i=\ASET{1, ..., n}$ is an input,  
    $\Sigma'$ is consistent.
   \end{enumerate}
\end{enumerate}
\end{DEF}


\begin{comment}
Corresponding to the transitions of configurations,
we re-state the network transition as global transition, 
$\GTRANS{\ell}$,
which directly describes the overall behaviours of local processes in network, 
as a dual to the the LTS defined in Fig. \ref{fig:NW:LTS}.

\begin{DEF}[Global network transition] \label{def:gtrans}\rm 
  Let $\dN \Cong \sN | \GQ{\RInfo}{\emptyset}$ 
  conform to $\Sigma$.  
  \TZC{
  If $\dN \TAUTRANS{\hat{\ell_1}} \cdots \dN_{n-1} \TAUTRANS{\hat{\ell_n}} \dN_n$,
  then the transitions (some of the actions can be $\tauact$)
  have the corresponding transitions from configuration transition which is written
  $\Sigma;\GQ{\RInfo}{\emptyset} 
  \GTRANS{\dual(\ell_1)}\cdots \GTRANS{\dual(\ell_n)}
  \Sigma_n;\GQ{\RInfo'}{\GQueue'}$.
Then we write
$\dN
\GTRANS{\dual(\ell_1)}
\cdots
\dN_{n-1}
\GTRANS{\dual(\ell_n)}
\dN_n$,
}
which we call {\em global transitions}.
\end{DEF}
\end{comment}

\paragraph*{\bf Session Fidelity}
\RD{Session fidelity describes the relation between a network and the
  configuration specifying it: all evolutions of the network should
  correspond to expected evolutions of the configuration which does
  not lead to ill-formed configurations.} We now give the formal
definition of session fidelity.
\begin{DEF}[Session fidelity] \label{def:sf} \rm
Assume configuration $\Sigma; \GQ{\RInfo}{\GQueue}$ is configurationally consistent.
We say that $\dN$ satisfies session fidelity w.r.t. $\Sigma; \GQ{\RInfo}{\GQueue}$
if and only if, for any $\ell$, 
$\dN \GTRANS{\ell} \dN'$ implies
$\Sigma; \GQ{\RInfo}{\GQueue} \GTRANS{\ell} \Sigma'; \GQ{\RInfo'}{\GQueue'}$
and $\Sigma'; \GQ{\RInfo'}{\GQueue'}$ is configurationally consistent and
$\dN'$ satisfies session fidelity w.r.t. $\Sigma'; \GQ{\RInfo'}{\GQueue'}$.
\end{DEF}
%The theorem of session fidelity states: 
%assume network $\dN \Cong \sN|\GQ{\RInfo}{\emptyset}$ is given, 
%suppose that $\sN$ satisfies
%$\Sigma$.  If $\Sigma$ is consistent and if $\RInfo=\ri{\Sigma}$, then
%we say $\dN$ {\em conforms to} $\Sigma$.  
%Session fidelity ensures that, if the messages that $\dN$ exchanges follow the specification, 
%then the dynamics of the network witnesses the validity of specifications.
%{\em In the follows, we always assume $\Sigma$ is consistent}  %unless otherwise stated.

Before proving session fidelity for our monitored framework we give a few auxiliary lemmas.
%
Lemma \ref{lem:emptyreceive} states that, as a network conforms to some configurationally consistent configuration, 
the evolution of the configuration must be able to consume an output occurrence in the network:
\begin{LEM} \label{lem:emptyreceive} \rm
Assume a network $\dN \Cong \sN|\GQ{\RInfo}{\GQueue}$ 
conforms to $\Sigma; \GQ{\RInfo}{\GQueue}$, and that $\Sigma; \GQ{\RInfo}{\GQueue}$ 
is configurationally consistent. If $\dN \GTRANS{\ell} \dN'$ with $\ell$ being an output and 
$\Sigma;\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue \CD m}$,
then $\Sigma'; \GQ{\RInfo}{\GQueue \CD m}$ is receivable.
%$\GQueue\cdot m$ is receivable to $\Sigma'$.
\end{LEM}
\begin{proof}
We only show the interesting case.
When $\ell = \AOUTfree{a}{s}{\rr}{\LA}$,
since $\Sigma$ is consistent, by Definitions \ref{def:consistent}, 
there exists $a:\Imode(T[\rr])$ in some $\Gamma$ of $\Sigma$.
Because $\ell$ does not affect the existence of $a:\Imode(T[\rr])$,
it remains in $\Gamma$ of $\Sigma'$, thus
invitation $m = \AOUTfree{a}{s}{\rr}{\LA}$ is receivable to $\Sigma'$.

Let $\alpha_i = \ENCan{\Gamma_i, \Delta_i}$.
When $\ell = s[\rr_1, \rr_2] ! l_j \ENCan{v}$, 
by Definitions \ref{def:consistent} and \ref{def:qconform},
since $\models \NLNWgood{\sN}{\Sigma}$
and $\Sigma$ is consistent, 
$\exists \alpha_s, \alpha_r \in \Sigma$, 
$\exists \GA$ is well-formed of the form
$$\GA = \GInter{\rr_1}{\rr_2}: \ASET{l_i(x_i: \sort_i)\ASET{A_i}. \GA_i}_{i \in I}$$
such that $s$ obeys to $\GA$:
\begin{eqnarray} \label{eqglobalproject}
\Delta_s(s[\rr_1]) &=& 
\Proj{\GA}{\rr_1} = 
\rr_2 ! \ASET{l_i(x_i: \sort_i)\ASET{A_i}. \Proj{\GA_i}{\rr_1}}_{i \in I} \nonumber
\\
\Delta_r(s[\rr_2]) &=& 
\Proj{\GA}{\rr_2} =
\rr_1 ? \ASET{l_i(x_i: \sort_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}
\end{eqnarray}
As action $s[\rr_1, \rr_2] ! l_j \ENCan{v}$ fires,
Equation \ref{eqglobalproject} changes to 
\[\begin{array}{lll} 
\Delta_s(s[\rr_1]) &=& 
\Proj{\GA_j}{\rr_1} \nonumber
\\
\Delta_r(s[\rr_2]) &=& 
\Proj{\GA}{\rr_2} =
\rr_1 ? \ASET{l_i(x_i: \sort_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}
\end{array}
\]
the receiving capability of $\rr_1?$ still remains in $\Delta_r(s[\rr_2])$, 
where $\alpha_r \in \Sigma'$, 
thus $m = s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$ is receivable to $\Sigma'$.
\end{proof} 
%%%%%%%%%%%%%%%%%
As $\dN \equiv \sN \mid H$ and
$\models \NLNWgood{\sN}{\Sigma}$,
the satisfaction relation of $\sN$ and $\Sigma$ remains
whenever action takes place.

Lemma \ref{lem:nsatisfiesM} says that, 
if the static part of a network satisfies a specification,
then the evolution of the static part still satisfies the corresponding evolution of the specification.  
\begin{LEM} \label{lem:nsatisfiesM} \rm
Assume $\dN \equiv \sN \mid H$ and $\models \NLNWgood{\sN}{\Sigma}$.
If $\dN \GTRANS{\ell} \dN' \equiv \sN' \mid H'$ 
and $\Sigma \TRANS{\ell} \Sigma'$,
then $\models \NLNWgood{\sN'}{\Sigma'}$.
\end{LEM}
\begin{proof}
Directly from Definition \ref{def:partial}.
\end{proof}

Finally,
Lemma \ref{lem:routing} states that, 
if a network conforms to a configurationally consistent configuration, 
then any evolution of the network conforms to the corresponding evolution of the configuration, 
which is still configurationally consistent. Lemma \ref{lem:routing} relier on the definition of routing table given below.
\begin{DEF}[Routing table] \label{def:routing} \rm.
\NI We define $\ri{\Sigma}$, {\em the routing table derived from
$\Sigma$}, as follows:
{\small 
$$\begin{array}{lcl}
  \ri{\alpha:\langle\Gamma;\Delta,s[\rr]:T\rangle,\Sigma}  = 
   s[\rr] \mapsto \alpha, \ri{\alpha:\langle\Gamma;\Delta\rangle,\Sigma}
\\
\ri{\alpha:\langle\Gamma,a:\II{T[\rr]}%^\capabilities
;\Delta\rangle,\Sigma} =  a \mapsto \alpha,\ri{\alpha:\langle\Gamma;\Delta\rangle,\Sigma} 
\\
\ri{\alpha:\langle\Gamma,a:\OO{T[\rr]}%^\capabilities
;\Delta\rangle,\Sigma} =  \ri{\alpha:\langle\Gamma;\Delta\rangle,\Sigma} 
\end{array}
$$} 
\LB{The routing table is used to observe inputs. Note that by Definition~\ref{def:consistent} (2), as long as $\Sigma$ is consistent, the existence of $a:\OO{T[\rr]}$ in $\Gamma$ implies that the corresponding $a:\II{T[\rr]}$ is also in $\Gamma$.}
%
%for $\ri{\alpha:\langle\Gamma,a:\OO{T[\rr]}}$, 
%because $a: \OO{T[\rr]}$ implies that $a: \II{T[\rr]}$ should exist in the network,  
%routing table should have contained the routing information for $a$. 
\end{DEF}

\begin{LEM} \label{lem:routing} \label{thm:sf} \rm
Assume configuration
$\Sigma; \GQ{\RInfo}{\GQueue}$
is configurationally consistent,
and network $\dN\Cong \sN|\GQ{\RInfo}{\GQueue}$
%$\dN\Cong (\nu\ n)(\sN_0|\GQ{\RInfo}{\GQueue})$ 
conforms to configuration
$\Sigma; \GQ{\RInfo}{\GQueue}$.
%% , which is configurationally consistent.
Then for any $\ell$, whenever we have 
$\dN \GTRANS{\ell} \dN'$ such that
$\Sigma; \GQ{\RInfo}{\GQueue} \GTRANS{\ell} \Sigma'; \GQ{\RInfo'}{\GQueue'}$,
it holds that 
$\Sigma'; \GQ{\RInfo'}{\GQueue'}$ is configurationally consistent and
that $\dN'$ conforms to $\Sigma'; \GQ{\RInfo'}{\GQueue'}$.
\end{LEM}

%Now we prove session fidelity:
\begin{proof}
Assume $\dN$ conforms to $\Sigma; \GQ{\RInfo}{\GQueue}$, which
is configurationally consistent.  We prove the statement by
inspection of each case. 
\begin{description}
\item[(Sel)] Let $\ell=s[\rr_1, \rr_2] ! l_j \ENCan{v}$, $\dN
  \GTRANS{\ell} \dN'$ and $\Sigma; \GQ{\ri{\Sigma}}{\GQueue}
  \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue \CD m}$,
  where $m = s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$.\\
         
         Then $\RInfo=\ri{\Sigma}=\ri{\Sigma'}$ 
         because there is no change to the elements in $\Sigma$ or to the routing table.
        \\
        
         Since $\Sigma$ allows $\ell$  and $\Sigma$ is consistent, then 
         $\exists \alpha_r, \alpha_s \in \Sigma$, and $\exists \GA$ well-formed of the form 
         $$\GA= \GInter{\rr_1}{\rr_2} \ASET{l_i (x_i: {S}_i) \ASET{A_i}. \GA_i}_{i \in I},$$
         such that 
         \[
         \begin{array}{rcl}
         \Delta_s(s[\rr_1]) &=&
         \Proj{\GA}{\rr_1} = 
         \rr_2! \ASET{l_i (x_i: {S}_i)\ASET{A_i}. \Proj{\GA_i}{\rr_1}}_{i \in I},\\
           
         \Delta_r(s[\rr_2])&=&
         \Proj{\GA}{\rr_2} = 
         \rr_1? \ASET{l_i (x_i: {S}_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}.
         \end{array}
         \] 
         $\Sigma \TRANS{\ell} \Sigma'$ implies
         $\Sigma'$ has 
         \[
         \begin{array}{rcl}
         \Delta_s(s[\rr_1])&=&\Proj{\GA_j}{\rr_1},\\ 
         \Delta_r(s[\rr_2])&=& \rr_1? \ASET{l_i (x_i: {S}_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}.
         \end{array}
         \]
  
         Case 1: $\GQueue$ is empty.
         By Lemma \ref{lem:emptyreceive}, 
         after receiving $m$, say $\Sigma' \TRANS{\ell}\Sigma''$, 
         $\Sigma''$ has $s[\rr_1]=\Proj{\GA_j}{\rr_1}$ and
         $s[\rr_2]=\Proj{\GA_j}{\rr_2}$, $\Sigma''$ is thus
         consistent by Definition \ref{def:consistent}.  By Definition
         \ref{def:congconsist}, $\Sigma'; \GQ{\RInfo}{m}$ is
         configurationally consistent, and $\models \NLNWgood{\sN'}{\Sigma'}$
         by Lemma \ref{lem:nsatisfiesM}, 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue \CD m}$. \\
 
         Case 2: $\GQueue$ is not empty. 
         Since $\Sigma; \GQ{\RInfo}{\GQueue}$ is configurationally consistent, 
         again, by Lemma \ref{lem:emptyreceive}, 
%          $\GQueue$ and $m$ are receivable to $\dN'$;
         after receiving messages in $\GQueue$ (but not $m$),
         say $\Sigma' \TRANS{\ell_0  \ldots \ell_n} \Sigma'_1$,
         where every action in $\ell_0  \ldots \ell_n$ 
         corresponds to each message in $\GQueue$,
         we have $\Sigma'_1; \GQ{\RInfo'}{m}$ is configurationally consistent.
         After $\Sigma'_1$ receives $m$, say 
         $\Sigma'_1 \TRANS{s[\p_1, \p_2]? l \ENCan{v}} \Sigma''$,
         where $s[\p_1, \p_2]? l \ENCan{v}$ is dual to $\ell$,
         with the same reasoning above, $\Sigma''$ has 
         $s[\rr_1]=\Proj{\GA'_j}{\rr_1}$ and $s[\rr_2]=\Proj{\GA'_j}{\rr_2}$,         
         so that $\Sigma''$ is consistent.
         By Definition \ref{def:congconsist}, 
         $\Sigma'; \GQ{\RInfo}{\GQueue \CD m}$ is configurationally consistent,
         and $\models \NLNWgood{\sN'}{\Sigma'}$ by Lemma \ref{lem:nsatisfiesM}, 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue \CD m}$. \\
         
 \item[(Bra)] Let $\ell=s[\rr_1, \rr_2] ? l_j \ENCan{v}$,
  $\dN \GTRANS{\ell} \dN'$ and 
  $\dN$ conforms to $\Sigma; \GQ{\ri{\Sigma}}{\GQueue}$.\\
  
         Case 1: $\GQueue$ is empty. Since 
         $\Sigma; \GQ{\ri{\Sigma}}{\emptyset} \not \GTRANS{\ell}$, 
         so this case never happens.\\
         
         Case 2: $\GQueue$ is not empty.
         Thus, $\dN \GTRANS{\ell} \dN'$ and 
         $$\Sigma; \GQ{\ri{\Sigma}}{\GQueue} 
         \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue / m},$$
         where $\GQueue / m $ means taking off message $m$ from $\GQueue$, where
         $m = s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$ \\
         
         We have $\RInfo=\ri{\Sigma}=\ri{\Sigma'}$ 
         because there is no change to the elements in $\Sigma$ or to the routing table.
         By Definition \ref{def:congconsist}, 
         after receiving all messages in $H$, $\Sigma$
         is consistent, thus $\Sigma'$, which has received message $m$
         is consistent after receiving all messages in $\GQueue / m$.  
         By Lemma \ref{lem:nsatisfiesM}, 
         we have $\models \NLNWgood{\sN'}{\Sigma'}$ 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue / m}$. \\
         
       \item[(Req)] Let $\ell=\AOUTPUT{a}{s}{\rr}{T}$.  
         $\dN\GTRANS{\ell} \dN'$ and 
         $$\Sigma; \GQ{\ri{\Sigma}}{\GQueue}
         \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue \CD m},$$
         where $m=\AOUTPUT{a}{s}{\rr}{T}$.
         Then $\RInfo=\ri{\Sigma}=\ri{\Sigma'}$
         because, by Definition \ref{def:routing}, nothing new is registered to the routing table.\\
        
         Since $\Sigma$ allows $\ell$ and $\Sigma$ is consistent,  
         by Definition \ref{def:consistent},
         $\exists \Gamma_i, \Gamma_j \in \Sigma$ such that
         $a: \Imode(T[\rr]) \in \Gamma_i$ and $a:\Omode(T[\rr]) \in \Gamma_j$.
         After $\Sigma \TRANS{\ell} \Sigma'$,
         by rule \mrule{Req} in the LTS of specifications, 
         %Fig. \ref{fig:LTS:spec:unlinked},
         %$a: T[\rr]^\capabilities \in \Gamma_i$ with $\mathtt{I} \in \capabilities$
         $a: \Imode(T[\rr])$ remains in $\Gamma'_i$,
         $a: \Omode(T[\rr])$ remains in $\Gamma'_j$,
         and thus they both remain in $\Sigma'$.\\
            
         Case 1: $\GQueue$ is empty. By Lemma~\ref{lem:emptyreceive},
          %  and also by [2] above, $m$ will be received by $\dN'$.  After
         after receiving $m$, 
         say $\Sigma' \TRANS{\AIN{a}{s}{\rr}{T}} \Sigma''$, 
         %$a:T[\rr]^\capabilities$ with $\mathtt{I} \in \capabilities$ 
         both $a:\Imode(T[\rr])$ and $a:\Omode(T[\rr])$ remain in $\Sigma''$, 
         satisfying Definition \ref{def:consistent},
         so that $\Sigma'; \GQ{\RInfo}{m}$ is configurationally consistent.
         By Lemma \ref{lem:nsatisfiesM}, we have $\models \NLNWgood{\sN'}{\Sigma'}$, 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue \CD m}$. \\

         Case 2: $\GQueue$ is not empty. The proof is similar to the
         one in (\textbf{Sel}) and omitted.
%          Since $\Sigma; \GQ{\ri{\Sigma}}{\GQueue}$ is configurationally consistent, 
%          by Lemma \ref{lem:emptyreceive}, 
%          $\GQueue$ and $m$ are receivable to $\dN'$,
%          and after receiving messages in $\GQueue$ (not yet $m$),
%          saying $\Sigma' \TRANS{\ell_0 \CD ... \CD \ell_n} \Sigma'_0$,
%          where $\forall \ell \in \ell_0 \CD ... \CD \ell_n$ 
%          corresponds to each message in $\GQueue$,
%          we have $\Sigma'_0; \GQ{\RInfo'}{m}$ is configurationally consistent.
%          After $\Sigma'_0$ receives $m$, saying $\Sigma'_0 \TRANS{\dual{\ell}} \Sigma''$,
%          with the same reasoning in [2], $\Sigma''$ has 
%          $a:T[\rr]^\capabilities$ with $\mathtt{I} \in \capabilities$,         
%          so that $\Sigma''$ is consistent.
%          By Definition \ref{def:congconsist}, $\Sigma'; \GQ{\RInfo}{\GQueue \CD m}$ is configurationally consistent
%          and $\dN'$ conforms to it.
         \\ 
         
 \item[(Acc)] Let $\ell=\AIN{a}{s}{\rr}{T}$.\\

         Case 1: $\GQueue$ is empty. Since 
         $\Sigma; \GQ{\ri{\Sigma}}{\emptyset} \not \GTRANS{\ell}$, 
         this case never happens.\\
         
         Case 2: $\GQueue$ is not empty. 
         If $\dN \GTRANS{\ell} \dN'$ and 
         $$\Sigma; \GQ{\ri{\Sigma}}{\GQueue} 
         \GTRANS{\ell} \Sigma'; \GQ{\RInfo'}{\GQueue / m},$$
         where $m=\AOUTPUT{a}{s}{\rr}{T}$.
         %%%%%%%
         Since there exists $\Delta \in \Sigma'$ s.t. $s[\rr] \in \Delta$,
         by Definition \ref{def:routing},
         $\RInfo'=\ri{\Sigma}, s[\rr] \mapsto \alpha=\ri{\Sigma'}.$\\
         
         For the same reasoning in (Bra), we have $\Sigma';
         \GQ{\RInfo}{\GQueue / m}$ is configurationally consistent.
         By Lemma \ref{lem:nsatisfiesM}, 
         we have $\models \NLNWgood{\sN'}{\Sigma'}$ 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue / m}$. \\
        
\end{description}
The proof for other cases is trivial.% $\blacksquare$
%Except actions $\newsLabel{\LA_i}{s}{\rr_j}{a_i}_{i\in I}$ and $\tau$,
%which do not change the network and its corresponding configuration,
%action $\ell=\newaLabel{a}{\LA}{\rr}$
%only changes the routing table by
%$\Sigma; \GQ{\ri{\Sigma}}{\GQueue} \TRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue}$.
%The new routing table $\RInfo$=$\ri{\Sigma}, a \mapsto \alpha=\ri{\Sigma'}$ 
%because $\Sigma'$ enables the input ($\mathtt{I}$) capability of channel $a$ in $\alpha$ 
%by $a:\INTp{T[\rr]}^{\capabilities\uplus \mathtt{I}}$.
\end{proof}

\begin{THM}[Session fidelity] \label{thm:sf:monitored} \rm
If $\mN$ is fully monitored and conforms to 
$\Sigma; \GQ{\RInfo}{\GQueue}$,
which is configurationally consistent,
then $\mN$ satisifies session fidelity.
\end{THM}
\begin{proof}
The proof is straightforward by Lemma \ref{thm:sf} and Definition \ref{def:sf}.
\end{proof}


\begin{PRO} \label{lem:gsimplysf} \rm
Whenever a network is fully monitored,  
global safety implies session fidelity. 
\end{PRO}
\begin{proof}
Simply by Definitions \ref{def:congconsist} and \ref{def:qconform} and
Corollary \ref{cor:cong:localtrans} and Theorems \ref{item:prop:globalsafe} and \ref{thm:sf}.
\end{proof}

%\paragraph*{\textbf{Proposition \ref{pro:fidelity}}}[\textbf{Session fidelity}] \rm
%%\begin{PRO}[Session Fidelity]
%  \rm Assume given a network $\dN$ conforming to a consistent $\Sigma$,
% s.t., $\dN\TRANS{\ell_1}\cdots\TRANS{\ell_n}\dN'$ and, correspondingly, 
% $\Sigma;\GQ{\ri{\Sigma}}{\emptyset} \TRANS{\ell_1}\cdots\TRANS{\ell_n} \Sigma';
% \GQ{\RInfo}{\GQueue}$.  Then the configuration $\Sigma';
% \GQ{\RInfo}{\GQueue}$ is configurationally consistent.
%%\end{PRO}

%\begin{proof}
%It is straightforward from Lemma \ref{lem:routing}. $\blacksquare$
%\end{proof}
