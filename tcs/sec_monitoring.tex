\section{The Monitored Network: Semantics and Equivalences}
\label{sec:monitoring}

In this section we formalise the specifications (based on local types)
used to guard the runtime behaviour of the principals in a network.
These specifications are the foundation of system monitors, each
wrapping a principal to ensure that the ongoing communication conforms
to the given specification. Then, we present a behavioural theory for
 monitored networks and their safety properties.

\subsection{Semantics of Global Specifications}
\label{sec:envs}
The specification of the (correct) behaviour of a principal consists
of an {\em assertion environment} $\AssEnv$, where $\Gamma$ is the
{\em shared environment} describing the behaviour on shared channels,
and $\Delta$ is the {\em session environment} representing the
behaviour on session channels \LB{(i.e., describing the sessions that
  the principal is currently participating in)}.  The syntax of
$\Gamma$ and $\Delta$ is given by:
\[
\begin{array}{rclcrcl}
\Gamma & ::= & \emptyset
\grmor
%\Gamma, a:\INTp{\LA[\rr]}
%\Gamma, a:\OUTp{\LA[\rr]}
\Gamma, a:\II{\LA[\rr]} \mid \Gamma, a:\OO{\LA[\rr]} 
&\quad&
%& ::= &
%\emptyset
%\mathtt{I}
%\mathtt{O}
%\quad\quad
\Delta
& ::= &
\emptyset
\grmor
\Delta, s[\rr]\!:\!\LA
\end{array}
\]
In $\Gamma$,
the assignment $a:\II{\LA[\rr]}$ (resp. $a:\OO{\LA[\rr]}$) states
that the principal can, through $a$, receive (resp. send)
invitations to play role $\rr$ in a session instance specified by
$\LA$.
%% More precisely, $\capabilities$ is a multiset whose elements
%% are $\mathtt{I}$ (i.e., the linear capability to receive an invitation
%% along $a$) and $\mathtt{O}$ (i.e., the linear capability to send an
%% invitation along $a$).
In $\Delta$, we write $s[\rr]\!:\!\LA$ when the principal is playing
role $\rr$ of session $s$ specified by $\LA$.  A network is monitored
with respect to \emph{collections of specifications} (later, just {\em
  specifications}) one for each principal in the network.  A
specification $\Sigma, \Sigma',\ldots$ is a finite map \LB{from
  principals to assertion environments:}
\begin{eqnarray*}
\Sigma
&\; ::=\; &
\emptyset
\grmor \Sigma, \AT{\alpha}{\AssEnv}
\end{eqnarray*}

%% How each multiparty conversation will unfold is specified in global
%% protocol descriptions in {\MPSA}: when it starts, its responsibilities
%% are allocated to each principal, transforming $\Sigma$.

\NI The semantics of $\Sigma$ is defined \LB{using the following labels:}
{ \small $$
\begin{array}{@{}l@{}c@{}l@{}}
\ell
&\ ::= \ &
\AOUT{a}{s}{\rr\!}{\!\LA}
\!\grmor\!%
%\ | \
\AIN{a}{s}{\rr\!}{\!\LA}
\!\grmor\!%
%\ | \
\LInter{s}{\rr_1,\rr_2}! \TZ{l}\ENCan{v}
\!\grmor\!%
%\ | \
\LInter{s}{\rr_1,\rr_2}? \TZ{l}\ENCan{v}
\!\grmor\!%
\tauact
\end{array}$$
}The first two labels are for {\em invitation} actions, the first is for
requesting and the second is for accepting.  Labels with
$\LInter{s}{\rr_1,\rr_2}$ indicate {\em interaction} actions for
sending ($!$) or receiving ($?$) messages within sessions.
%
The labelled transition relation for specifications is defined by the rules
in Fig.~\ref{fig:LTS:spec:unlinked}
\input{fig_transition_spec_unlinked}
Rule $\mrule{Req}$ allows $\alpha$ to send an invitation on a properly typed
shared channel $a$ (i.e., given that the shared environment maps $a$ to $\LA[\rr]$).  
Rule $\mrule{Acc}$ allows $\alpha$ to receive an invitation to be role $\rr$ in a new session $s$,  
on a properly typed shared channel $a$.
%
Rule \mrule{Bra} allows $\alpha$, participating to session $s$ as $\rr_2$, to receive a message with label $l_j$ from $\rr_1$,  
given that $A_j$ is satisfied after replacing $x_j$ with the received value $v$.
After the application of this rule the specification is $\LA_j$.
%
Rule \mrule{Sel} is the symmetric (output) counterpart of \mrule{Bra}.
We use $\eval$ to denote the evaluation of a logical assertion.
%
$\mrule{Spl}$ is the juxtaposition of two session environments. %% where
%% $\Delta_1 \brkin \Delta_2 $ composes two local types: $\Delta_1\brkin
%% \Delta_2 =\{ s[\rr]:(\LA_1 \PAR \LA_2) \midW \LA_i = \Delta_i(s[\rr]),
%% s[\rr]\in\dom{\Delta_1}\cap \dom{\Delta_2}\} \cup \dom{\Delta_1} /
%% \dom{\Delta_2} \cup \dom{\Delta_2} / \dom{\Delta_1}$.
%
$\mrule{Tau}$ says that the specification should be invariant under
reduction of principals.  $\mrule{Par}$ says if $\Sigma_1$ and
$\Sigma_3$ are composable, after $\Sigma_1$ becomes
as $\Sigma_2$, they are still composable.

\subsection{Semantics of Dynamic Monitoring}
\label{sec:RT} 
\label{subsec:monnetwork}

%%Run-time enforcement is realised by distributed run-time verification
%%through decentralised endpoint monitors.
%
The endpoint monitor $\M,\Mp,...$ for principal $\alpha$ is a specification $\AT{\alpha}{\AssEnv}$ 
used to {\em dynamically} ensure that the messages to and from $\alpha$ are legal with respect to $\Gamma$ 
and $\Delta$. 
%
%\LB{Namely, monitors preserve the good actions and discard the bad ones according to the corresponding specifications.}
%$\M,\Mp,...$ range over monitors. 
A {\em monitored network} $\mN$ is a network $\dN$
with monitors, obtained by extending the syntax of networks as:
\[
\mN\ :: =\ N
\PAR 
\M \hspace{0.5cm}
\grmor \hspace{0.5cm}
 \mN \PAR \mN
\hspace{0.5cm} \grmor \hspace{0.5cm}
 (\nu s) \mN
\hspace{0.5cm} \grmor \hspace{0.5cm}
(\nu a) \mN
\]
%Hereafter we assume A {\em monitored process}, written $\namedP{\Monitor}{P}{\alpha}$, is a process $P$ running under a principal $\alpha$ monitored by $\M$.
The reduction rules for monitored networks are given 
in Fig. \ref{fig:network:red} and use, in the premises, the labelled transitions of monitors. 
The labelled transitions of a monitor are the labelled transitions of its corresponding specification (given in \S~\ref{sec:envs}).
%
\input{fig_transition_monitoredPrincipal}
%
The first four rules model reductions that are allowed by the monitor 
(i.e., in the premise). 
%
Rule \monrule{Req} inserts an invitation in the global queue. Rule \monrule{Acc} is
symmetric and updates the router so that all messages for role $\rr$ in session $s$ will be routed to $\alpha$. 
Similarly, \monrule{Bra} (resp. \monrule{Sel}) extracts (resp. introduces) messages from (resp. in) the global queue.
%
The error cases for \monrule{Req} and \monrule{Sel}, namely \monrule{ReqEr} and \monrule{SelEr}, 
`skip' the current action (removing it from the process), and do not modify the queue, the router nor the state of the monitor. 
%
The error cases for \monrule{Acc} and \monrule{Bra}, namely
\monrule{AccEr} and \monrule{BraEr}, do not affect the process, which
remains ready to perform the action, and remove the violating message
from the queue.








\begin{example}[ATM: a monitored network] \label{ex:monnetwork}
We illustrate the monitored network for the ATM scenario, where the routing table is defined as
\small \[\RInfo = a \mapsto \alpha, b \mapsto \beta, c \mapsto \gamma, s[\Ser]\mapsto \alpha, s[\Cli]\mapsto \beta, s[\Aut]\mapsto \gamma\]
\normalsize
%
\noindent We consider the fragment of session where the authentication has occurred, the process of 
$\Cli$ (resp. $\Ser$) is $P'_{\Cli}$ (resp. $P'_{\Ser}$) from Example~\ref{ex_process}, and the process of $\Aut$ is $\INACT$. 
\[
\small
\begin{array}{l}
\mN_{\Ser} \! = \! \namedP{\M_{\Ser}}{P'_{\Ser}}{\alpha}
                    \!  = \! \namedP{\M_{\Ser}}{s[\Ser, \Cli]!\ \lab{Account}{} \ENCan{100} ; P''_{\Ser}}{\alpha}\ \text{(assuming $getBalance()=100$)}\\
\mN_{\Cli} \! = \! \namedP{\M_{\Cli}}{P'_{\Cli}}{\beta}
                    \!  = \! \namedP{\M_{\Cli}}{s[\Ser,\Cli]?\ \lab{Account}{}(x_b). P''_{\Cli}}{\beta}\\
\mN_{\Aut} \! = \!
%\namedP{\M_{\Aut}}{P_{\Aut}}{\gamma}
                      \namedP{\gamma: \langle
c: \LA_\Aut[\Aut] \ ; \
s[\Aut]:\ \kend \rangle}{\INACT}{\gamma}\normalsize
\end{array}\]

\noindent where \small $\M_{\Ser} = \alpha: \langle {a}:\ {\LA_{\Ser}[\Ser]} \ ; \            {s[\Ser]}:\  \Cli!\ \lab{Account}{} 
(x_b:\mathsf{int}) \ASET{x_b\geq 0} . \LA'_{\Ser} \rangle$ \normalsize and \small $\M_{\Cli}$\normalsize  ~is dual.
%        \small$ \M_{\Cli} = \beta: \langle
%b: \LA_{\Cli}[\Cli] \ ; \
%s[\Cli]:\ \Ser ?\ \lab{Account}{}
%  (x_b: \mathsf{int})\ASET{x_b\geq 0}  . \LA'_{\Cli}\rangle$. \normalsize 
%
\small\[\begin{array}{lll}
\mN_1 \! \!  = \! \! & \! \!
                   \namedP{\M_{\Ser}}{s[\Ser, \Cli]!\ \lab{Account}{} \ENCan{100} ; P'_{\Ser}}{\alpha}  
                   %
                    \! \mid \!   
                   %
                   \namedP{\M_{\Cli}}{s[\Ser,\Cli]?\ \lab{Account}{}(x_b). P'_{\Cli}}{\beta} 
                \! \mid \!
                \mN_{\Aut} \! \mid \! \GQ{\RInfo}{\emptyset}\\[1mm]
%
&  \RED\RED
                   \namedP{\M'_{\Ser}}{P'_{\Ser}}{\alpha} %\\
%           & | &
\ | \
                   \namedP{\M'_{\Cli}}{P'_{\Cli} \SUBS{100}{x_b}}{\beta}
%\\
%           & | &
\ | \
\mN_{\Aut} \ | \ \GQ{\RInfo}{\emptyset}\\
\end{array}\]
where~~
\small
$ \M'_{\Ser}= \alpha: \langle \AT{a}{\LA_{\Ser}[\Ser]} \ ; \
                                     s[\Ser] :  \ \LA'_{\Ser}\rangle$\normalsize ~~and ~~
                                     \small 
$\M'_{\Cli} = \beta: \langle
b: \LA_{\Cli}[\Cli] \ ; \
s[\Cli]:\  \LA'_{\Cli} \rangle
$
\end{example}\normalsize
\noindent
Above, $x_b\geq 0$ is satisfied since $x_b=100$. If the server tried to communicate e.g., value $-100$
for $x_b$, then the monitor (by rule~\monrule{SelEr}) would drop the message.

\input{ex_monitor}

%
%On the other hand, if monitor $\M_{\Ser}$ is {\em erased}, then $\alpha$ becomes unmonitored and
%a violating transition with label $\ell=s[\Ser, \Cli] ! \lab{Account}{} \ENCan{-100}$
%can be made by $\namedPmo{s[\Ser,\Cli]!\ \lab{Account}(-100).P'_{\Ser}}{\beta}$.


%The dynamics of networks, in particular how messages travel from a
%local configuration through a network to another local configuration,
%is formalised by the reduction rules in
%Fig.~\ref{fig:network:red}.
%In each rule except \trule{Proc}, the
%(else) case is
%when the monitor detects a violation of the
%specification by the current action;
%we stipulate that $\M$ simply drops such message.
%Each rule corresponds to a rule for the monitor
%semantics defined in Fig.~\ref{fig:monitorLTS}.
%Rule \trule{New} creates a fresh (bound) shared channel,
%while rule \trule{NewS} creates a new session channel,
%together with an empty (global) queue
%used by all processes joining session $s$.
%Rule \trule{Join} creates a local queue for session $s$ and role $\p$.
%In rules \trule{Req,Acc}, monitor $\M$ checks outgoing and incoming
%invitations. Ideally, \trule{Req} forwards outgoing invitations, which
%are still local to a process, to the external environment (dually for
%\trule{Acc} with incoming invitations).  In \trule{Out}, $\M$ forwards
%a session messages from a local queue $s[\p]$ into the global queue
%$s$ (dually for \trule{In}).  Other rules are standard.
%
%
%An unmonitored network is obtained by substituting monitored processes
%$\namedP{\Monitor}{P}{\alpha}$ with {\em monitor-off processes}
%$\namedPmo{P}{\alpha}$
%where a gateway $\A$ is used instead of the corresponding monitor $\M$.
%where the interface of principal $\alpha$
%is used instead of the corresponding monitor $\M$.
%The semantics of $\namedPmo{P}{\alpha}\RED \namedPmo{P'}{\alpha}$
%is precisely as in
%$\namedP{\Monitor}{P}{\alpha}\RED \namedP{\Monitor'}{P'}{\alpha}$,
%except that we use
%$\namedPmo{}{\alpha} \TRANS{\ell} \namedPmo{}{\alpha}$ instead of
%$\namedP{\Monitor}{}{\alpha}\RED \namedP{\Monitor'}{}{\alpha}$.
\COMMENT{
\paragraph{\bf LTS for monitored processes.}
We now define the LTS for $\namedP{\Monitor}{P}{\alpha}$,
in order to allow the global observation of monitored networks, as follows.
\[
\namedP{\Monitor}{P}{\alpha}\RED \namedP{\Monitor'}{P'}{\alpha}
\text{ if }\Monitor \TRANS{\ell} \Monitor'
\text{ and }P\TRANS{\ell}P'
\]
where the LTS for $\M$ and $P$ are
defined in Fig.~\ref{fig:monitorLTS} and
Fig.~\ref{fig:process:lts}, respectively.


% Observe that, whenever a principal has a sequence of actions
% satisfying a specification,
% the monitor allows it, regardless of whether or not
% its behaviour as a whole
% satisfies the specification
% (i.e.



% Let $\seq$ be a sequence of actions, and
% $[P]_{\alpha} \TRANS{\seq}$ means that trace $\seq$ is allowed by $[P]_{\alpha}$,
% as well as $\Sigma \TRANS{\seq}$ and $\namedP{\M}{P}{\alpha} \TRANS{\seq}$.

% \begin{enumerate}
% \item If $[P]_{\alpha} \TRANS{\seq}$ and $\Sigma \TRANS{\seq}$,
%           then $\namedP{\M}{P}{\alpha} \TRANS{\seq}$.
% \item If $[P]_{\alpha} \TRANS{\seq \CD \ell \CD \seq'}$ and
%           $\Sigma \TRANS{\seq \CD \seq'}$ but $\Sigma \not \TRANS{\seq \CD \ell}$,
%           then $\namedP{\M}{P}{\alpha} \TRANS{\seq \CD \seq'}$.
% \end{enumerate}
% \end{DEF}
% }



\begin{EX}\label{ex:short2}
Let $P=s[\p_1,\p_2]!\ENCan{100};P'\PAR s[\p_1]:\emptyset$
be a process with an empty queue playing role $\p_1$ in $s$. Let also
$\M_1= \mon{ \Gamma, s[\p_1]^\bullet:\p_2!(x:\keyword{Int})\{x>0\}.\LA}$
be its local monitor.
The communication happens in two steps. First, the message is spawn into the local queue as $P\TRANS{\tau}P_2$ with $P_2=P'\PAR s[\p_1]:s[\p_1,\p_2]!\ENCan{100}$ hence, since $\M_1\TRANS{\tau}\M_1$, then
$\namedPmo{P}{\alpha}\TRANS{\tau}\namedPmo{P_2}{\alpha}$.
Second, the message is forwarded to the global queue as
$P_2\TRANS{\ell}P'\PAR  s[\p_1]:\emptyset$ with $\ell =s[\p_1,\p_2]!\ENCan{100}$;  hence since $\M_1\TRANS{\ell} \mon{\Gamma, s[\p_1]^\bullet:\LA}$ then
$\namedP{\Monitor_1}{P}{\alpha}\TRANS{\ell}
\namedP{\mon{\Gamma, s[\p_1]^\bullet:\LA}}{P'}{\alpha} \PAR  \emptyset$,
where $\emptyset$ represents the empty endpoint (internal) queue.
\end{EX}
}
%\begin{example}\label{ex:network}
%\begin{itemize}
%\item TO DO: maybe bigger example on networks?
%\end{itemize}
%\end{example}
%
%If $\ell = s[\Sell,\Buy]?\ENCan{100}$ which satisfies the specification of $\M$,
%then then action $\ell$ is allowed and the monitor will next allow the interactions specified by $\LA$.
%, called {\em erasure}.
% The result of such erasure, the monitor which `switches-off'
% the monitoring activity, acts simply as a gateway with
% routing information.
%including such as
%session endpoint $s[\p]$ and shared name $a$.
%We call this stripped-off gateway a {\em monitor-off}
%Thus the {\em monitor-off} process is simply a process driven by
%a principal defined before.
%%$\namedPmo{P}{\alpha}.$
%($\A,{\A}',\ldots$).
%The semantics of $\A$ is obtained by erasing
%the co-domain from each monitor.
%premises and substitutions for interaction variables, if any,
%We write $\erase{\M}$ for the
%monitor-off obtained through this erasure of
%% by erasing capability/assertion information from
%$\M$.
%Hereafter we denote $\erase{\M}$ as $\A$.

\begin{comment}
\begin{EX}[A Monitored and A Non-monitored Network]
%$\namedP{\M}{P}{\alpha}$ vs $\namedPmo{P}{\alpha}$]
Let $\M = \AT{\alpha}{\Gamma;\Delta}$ with
$\Delta
\DEFEQ
s[\rr_1]:\rr_2!(x:\keyword{Int}) \REFF{x>0}.\LA$
and
$\ell = s[\rr_1,\rr_2]!\ENCan{-10}$.
Then $\namedPmo{P}{\alpha} \TRANS{\ell}$, namely $\namedPmo{P}{\alpha}$ allows this action;
but
$ \namedP{\M}{P}{\alpha}
\not \TRANS{\ell}$ since the value $-10$ does not satisfy assertion $x>0$
prescribed by the local type.
If $\ell = s[\rr_1,\rr_2]!\ENCan{5}$ which satisfies the specification of $\M$,
then then action $\ell$ is allowed and the monitor will next allow the interactions specified by $\LA$.
%Similarly, for type violations, if
%$\M_2\DEFEQ
%s[\rr_1]:\rr_2!(\AT{x}{\keyword{String}})\{\truek \ENCan{\emptyset}\}.\LA$
%and $\ell = s[\rr_1,\rr_2]!\ENCan{10}$ then $\A_2\TRANS{\ell} $
%but $\M_2\not \TRANS{\ell}$.
\end{EX}
\end{comment}

\COMMENT{
\paragraph{Storage in network: operational semantics of store}

In an actual distributed store, both read and write operations are
done through communication. This is true for all actions on store,
including creation and deletion of a key-value pair, and those related
to mutual exclusion such as lock and unlock.

%However, in the present theory, we treat read and write (and later,
%read/write on a set of attributes) as primitives, for concision of
%representation. On this basis, we can have three kinds of read/write
%semantics:
%\begin{enumerate}
%\item Atomic variable semantics: i.e. each variable's read and write
%  are atomic, but not collectively.
%\item Atomic semantics for the same principal: i.e. the read (write)
%  of variables from the same principal are atomic.
%\item Transactional semantics: i.e. all variables in an assertion
%  (resp. in an effect) are read (resp. written) at one time.
%\end{enumerate}
%We take here the third approach.

%In the following, given an specification $\Gamma$, write
%$\mon{\Gamma}$ for the monitor which represents the monitor based on
%the specification $\Gamma$.
}
\begin{comment}

\begin{EX} Assume given the following local type.
\begin{equation*}
  \LA
  \;\DEFEQ\;
  \lsumIn{\q}{}{x:\kw{info}}{\rr.\kw{credit}\geq 100 \AND
  \rr.\kw{counter}\leq 10,000}{}{\LA'}
\end{equation*}
Now we show how a endpoint monitor retrieves latest
values of $\rr.\kw{credit}$ and $\rr.\kw{counter}$. This is done through
the use of the routing information.

Below we annotate $\rr$ with $\alpha$,
which we assume to take place when the ``join'' command
is executed. Below we assume $\RInfo$ is such that $\Phi(s[\rr])=\alpha$.
We also set a pending message $m$ to be
$s[\rr_2, \rr_1]!\ENCan{\text{``abc''}}$.
\[
\begin{array}{ll}
&
    \monLeft
    \kw{credit} \mapsto 120 \PAR
    \kw{counter}\mapsto 999,
    \Gamma, \\
    &
    \JSESS{s[\rr_1]}: \rr_2 ? (x: \kw{info})
    \Reffect{
      \kw{credit}\geq 100\ \AND\
      \kw{counter}\leq 10,000}{}.
      \LA'
  \monRight {[P]}_{\alpha} \PAR\\
&
\GQ{\RInfo}{m^\beta\,\CD\,H}
\\
&\LITEQ \\
&
\namedP
{\alpha}
{
\mon
{
  \Gamma, \JSESS{s[\rr_1]}:
  \rr_2 ? (x: \kw{info})
  \Reffect{
    \RInfo(\rr_1).\kw{credit} \geq 100\ \AND\ \RInfo(\rr_1).\kw{counter}\leq 10,000
  }{}.\LA'
}
}
{P'\ \PAR \ h}
\PAR \\
&
  \alpha.\kw{credit} \mapsto 120
  \PAR
  \alpha.\kw{counter} \mapsto 999
  \PAR
  \GQ{\RInfo}{m^\beta\,\CD\,H}\\
&\LITEQ \\
&
\namedP
{\alpha}
{
\mon
{
  \Gamma, \JSESS{s[\rr_1]}:
  \rr_2? (x: \kw{info})
  \Reffect{
    \alpha.\kw{credit} \geq 100\ \AND\ \alpha.\kw{counter}\leq 10,000
  }{} .
  \LA'
}
}
{P'\ \PAR \ h}
\PAR \\
&
  \alpha.\kw{credit} \mapsto 120
  \PAR
  \alpha.\kw{counter} \mapsto 999
  \PAR
  \GQ{\RInfo}{m^\beta\,\CD\,H}
\\
&\LITEQ \\
&
\namedP
{\alpha}
{
\mon
{
  \Gamma, \JSESS{s[\rr_1]}:
  \rr_2? \Reffect{120 \geq 100\ \AND\ 999 \leq 10,000}{} . \LA'
}
}
{P'\PAR h}
\PAR \\
& \alpha.\kw{credit} \mapsto 120 \PAR \alpha.\kw{counter} \mapsto 999
\PAR \GQ{\RInfo}{m^\beta\,\CD\,H}\\
&\LITEQ \\
&
\namedP
{\alpha}
{
\mon
{
  \Gamma, \JSESS{s[\rr_1]}:
  \ASET{\truek} . \LA'
}
}
{P'\PAR h}
\PAR \\
& \alpha.\kw{credit} \mapsto 120 \PAR \alpha.\kw{counter} \mapsto 999\\
&\PAR \GQ{\RInfo}{m^\beta\,\CD\,H}\\
&\RED \\
&
\namedP
{\alpha}
{
\mon
{
  \Gamma, \JSESS{s[\rr_1]}:
  \LA'\MSUBS{\text{``abc''}}{x}
}
}
{P'\PAR h\CD m}
\PAR \\
& \alpha.\kw{credit} \mapsto 120 \PAR \alpha.\kw{counter} \mapsto 999\\
& \PAR \GQ{\RInfo}{H}\\
\end{array}
\]
\end{EX}


\NI We can similarly consider non-transactional semantics, which we omit.\\

\subsubsection{Creating a store with principal}
For simplicity, we assume a new store is generated once a process is
generated, and never increased or deleted. Then:
$$
P\quad::=\quad ...\;\ | \;\
\newNPD{\alpha}{\Monitor}{P}{Q}{D}
$$
which reduces as:
$$
\newNPD{\alpha}{\Monitor}{P}{Q}{D}
\quad\RED\quad
(\newname{\beta})(Q\PAR h\CD \newm{\alpha:\Monitor[P], D})
$$
as well as
$$
\namedP{\beta}{\Monitor'}{Q\PAR\newm{\beta:\Monitor[P], D}\CD h}
\quad\RED\quad
\namedP{\beta}{\Monitor'}{Q\PAR h}\PAR
\namedP{\alpha}{\Monitor}{P\PAR \emptyset}\PAR
D
$$
as required.
\end{comment}




%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% Local IspellDict: british
%%% Local IspellPersDict: ./.ispell
%%% End:
