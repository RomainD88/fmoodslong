\label{app:fidelity}

The theorem of session fidelity states that, 
whenever a network conforms to specifications, 
i.e., its all local processes (static network) conform to specifcations, 
all of its derivatives conform to specifications.
To prove the property of {\em session fidelity}, 
we need to formally illustrate the dynamic movements 
of messages in the network 
(i.e. messages leave the global queue and enter partial network, and visa versa).
Thus we firstly define 
the rules of labelled transition relation of configurations, 
routing tables,
and the properties of consistency and conformance of specifications,
then we state
{\em  receivability, consistency and conformance} of configurations.
Through them, 
we establish and prove session fidelity at the end. 

The LTS of configurations is defined in Figure \ref{fig:LTSCF}.
All rules are straightforward from 
the LTS of specifications and the one of networks.
% has been introduced in the LTS for monitored networks, 
% defined in Figure \ref{fig:monitor:network:LTS}.
\input{fig_LTSConfiguration}

\begin{comment}
\begin{DEF}[Full satisfaction (network satisfaction)]
\label{def:full}
  A relation $\CAL{R}$ from specifications to networks is a
  {\em full satisfaction} if, whenever
  $\OSPECi{0}\CAL{R}\dN_0$, the following conditions hold:
  \begin{enumerate}
  \item\label{case:satisfd:one} If $\OSPECi{0}\TRANS{\ell}\OSPECi{1}$ for
    an output $\ell$, $\dN_0\TAUTRANS{\TZC{\dual(\ell)}}\dN_1$ s.t.
    $\OSPECi{1}\CAL{R}\dN_1$.
  \item \label{case:satisfd:two}
If $\dN_0\TRANS{\ell}\dN_1$ for an output
%%%%% input
    $\ell$, $\destination{\ell}\in\dom{\OSPECi{0}}$
    and $\OSPECi{0}\TAUTRANS{\TZC{\dual(\ell)} }\OSPECi{1}$ s.t.
    $\OSPECi{1}\CAL{R}\dN_1$.
  \item If $\dN_0\TRANS{\tauact}\dN_1$ then $\OSPECi{0}\CAL{R}\dN_1$.
  \end{enumerate}
  When $\OSPEC\CAL{R}\dN$ for a full satisfaction $\CAL{R}$,
  we say {\em $\dN$ satisfies $\OSPEC$}, writing $\models
  \NWgood{\OSPEC}{\dN}$
\end{DEF}

Note that $\Sigma \TRANS{\ell} \Sigma'$ corresponds to 
$\GQ{\RInfo}{\GQueue} \TRANS{\dual(\ell)} \GQ{\RInfo'}{\GQueue'}$
because the viewpoints of local and global are dual:
when a local process (which obeys local specification, i.e. $\Sigma$) 
{\em outputs} a message {\em to} the global queue,
from the viewpoint of global queue, it {\em inputs} a message.
Similarly, when a local process {\em inputs} a message {\em from} the global queue,
from the viewpoint of global queue, it {\em outputs} a message.

To simplify and clarify the relationship above, we use {\em configuration}, 
the pair of $\Sigma$ and $\GQ{\RInfo}{\GQueue}$,
to illustrate the basic idea of session fidelity.
Configuration is written $\Phi=\Sigma; \GQ{\RInfo}{\GQueue}$, 
which guides and captures the behaviours {\em inside} 
the network.  
The labelled transition relations of $\Phi$ (configurations)
are defined in Figure \ref{fig:LTSCF}.  
\end{comment}

\begin{DEF}[Configuration] \label{monitor:def:configuration}
A configuration is denoted by $\Phi=\Sigma; \GQ{\RInfo}{\GQueue}$, 
in which the group of monitors correspond to $\GQueue$.
In other words, all messages corresponding to the actions
guarded by $\Sigma$ are in $\GQueue$.
\end{DEF}
A $\Phi$ thus guides and captures the behaviours in the network.  
Let $\principals{\Phi}$ be the set of prinicipals involving in $\Phi$. 
We define the composition of configurations as follows.
\begin{DEF}[Parallel composition of configurations] \label{def:CFComposable} \rm
Assume $\Phi_1=\Sigma_1\ ;\ \GQ{\RInfo_1}{\GQueue_1}$ and 
$\Phi_2=\Sigma_2\ ;\ \GQ{\RInfo_2}{\GQueue_2}$, 
we say $\Phi_1$ and $\Phi_2$ are {\em composable} whenever
$\principals{\Phi_1}\cap\principals{\Phi_2}=\emptyset$ and 
the union of their routing tables remains a function. 
%and $\Sigma_1$ and $\Sigma_2$ are composable.  %(they are always composable) 
If $\Phi_1$ and $\Phi_2$ are composable, we define the composition of $\Phi_1$ and $\Phi_2$ as:
$\Phi_1 \odot \Phi_2 = 
\Sigma_1, \Sigma_2\ ;\ 
\GQ{\RInfo_1\cup \RInfo_2}{\GQueue_1\CD \GQueue_2}$.
\end{DEF}

The behaviours of each principal in a network are guided by the
$\Sigma$ (specification), and are observed by the
$\GQ{\RInfo}{\GQueue}$ (global transport).  Except rules
$\mbox{[Acc]}$ and $\mbox{[Par]}$, all rules are straightforward
from the LTS of specifications 
(defined in Section \ref{sec:envs})
%(Figure \ref{fig:LTS:spec:unlinked})
and the one of dynamic networks (Figure \ref{fig:NW:LTS}). 
\begin{enumerate}
\item 
Rule $\mbox{[Acc]}$ indicates that, only when the invitation has been
(internally) accepted by a principal in the network, the routing
information registers $s[\rr] \mapsto \alpha$.  When we observe the
global transport (externally), we only observe that an invitation is
moved out from the global queue (which implies that it has been
accepted). However, we do not know {\em who} accepts it.  Only
$\Sigma$ tells which principal accepts this invitation, 
so that we can register it in the routing information using $\alpha$. 
\item
Rule $\mbox{Par}$ says if
$\Phi_1$ and $\Phi_3$ are composable 
(Definition \ref{def:CFComposable}), after $\Phi_1$ becomes as $\Phi_2$, they are
still composable.
\end{enumerate}

\begin{DEF}[Consistency, Coherence] \label{def:consistent} \label{monitor:def:groupMconsistent} 
$\Sigma = \{\AT{\alpha_i}{\langle\Gamma_i;\Delta_i\rangle}\}_{i \in I}$ is {\em consistent} when
\begin{enumerate}
\item there is \TZ{one and only one} 
  %at most one 
  $i$ such that $\Gamma_i \vdash
  a:\Imode(T[\rr])$, and
  
\item as long as $a:\Omode(T[\rr])$ exists in some $\Gamma_i$,
$\exists \Gamma_j$ such that $a:\Imode(T[\rr]) \in \Gamma_j$; and
% ^{\capabilities}$ with $\mathtt{I}\in\capabilities$,
%  $\capabilities = \mathtt{I}
%  \uplus \capabilities'$,
\item for any $s$ appearing in any $\Delta_j$, 
if $\{ s[\rr_k]:T_k \}_{1 \leq k \leq n}$ is a collection 
appeared in $\{ \Delta_i \}_{i \in I}$, 
there exists well-formed 
  $\GA$ such that $\ROLES{\GA}=\ASET{\rr_1, .., \rr_n}$
and $\Proj{\GA}{\rr_i}=T_i$. 
\end{enumerate}
Two specifications $\Sigma_1$ and $\Sigma_2$ are {\em coherent} when
their union is a consistent specification.
\end{DEF}

\begin{DEF}[Routing table] \label{def:routing} \rm
\NI We define $\ri{\Sigma}$, {\em the routing table derived from
$\Sigma$}, as follows:
\small 
$$\begin{array}{lcl}
  \ri{\alpha:\langle\Gamma;\Delta,s[\rr]:T\rangle,\Sigma}  = 
  \TZ{s[\rr] \mapsto \alpha}, \ri{\alpha:\langle\Gamma;\Delta\rangle,\Sigma}
\\
\ri{\alpha:\langle\Gamma,a:\II{T[\rr]}%^\capabilities
;\Delta\rangle,\Sigma} =  \TZ{a \mapsto \alpha},\ri{\alpha:\langle\Gamma;\Delta\rangle,\Sigma} 
\\
\ri{\alpha:\langle\Gamma,a:\OO{T[\rr]}%^\capabilities
;\Delta\rangle,\Sigma} =  \ri{\alpha:\langle\Gamma;\Delta\rangle,\Sigma} 
\end{array}
\normalsize$$ 
for $\ri{\alpha:\langle\Gamma,a:\OO{T[\rr]}}$, 
because $a: \OO{T[\rr]}$ implies that $a: \II{T[\rr]}$ should exist in the network,  
routing table should have contained the routing information for $a$. 
\end{DEF}

In the follows, we formally define {\em receivability, consistency and conformance} 
based on LTS of configurations and dynamic networks.
\begin{DEF}[Receivable Configuration]
Define $\Sigma;\GQ{\RInfo}{\GQueue}$ is {\em receivable} 
by the following induction:
\begin{enumerate}
\item If $\GQueue$ is empty then $\Sigma;\GQ{\RInfo}{\GQueue}$ is
  receivable. %% to $\Sigma$,
\item If % $\GQueue$ is non-empty and 
  $\GQueue\LITEQ m\CD \GQueue'$, 
  then $\Sigma;\GQ{\RInfo}{\GQueue}$ is receivable
  when we have $\Sigma; \GQ{\RInfo}{m\CD \GQueue'}
  \GTRANS{\ell} \Sigma';\GQ{\RInfo'}{\GQueue'}$, 
  where $\ell$ corresponding to $m$, 
  and $\Sigma';\GQ{\RInfo'}{\GQueue'}$ is receivable. %% to $\Sigma'$.
\end{enumerate}
\end{DEF}

  A configuration $\Sigma;\GQ{\RInfo}{\GQueue}$ 
  is {\em configurationally consistent} if all of its 
  multi-step global input transition derivatives 
  are receivable and the resulting specifications $\Sigma$ is consistent.
  The consistency of specifications is defined in 
  Definition \ref{monitor:def:groupMconsistent}.

\begin{DEF}[Configurational Consistency] \label{def:congconsist} \rm 
  A configuration $\Phi=\Sigma; \GQ{\RInfo}{\GQueue}$ is
  {\em configurationally consistent} whenever
\begin{enumerate}
\item If $\GQueue$ is empty and $\Sigma$ is consistent, or
\item $\GQueue$ is not empty, 
  the sequence of messages in $\GQueue$ are
  receivable to $\Sigma$, and after receiving all messages in $\GQueue$ 
  with $\Sigma \TRANS{\ell_1  \ldots \ell_n} \Sigma'$, 
  where $\ell_i, i=\ASET{1, ..., n}$ are inputs
  and, $\forall \globalm \in \GQueue$,  
  $\exists \ell \in \ell_1  \ldots \ell_n$
  such that $\ell$ corresponds to $\globalm$, 
  we have $\Sigma'$ is consistent.
\end{enumerate}
\end{DEF}
\NI In other words, $\Sigma;\GQ{\RInfo}{\GQueue}$ 
  is configurationally consistent if, in each of its derivatives, 
  all messages in the transport can be ``received'' 
  by some monitors in $\Sigma$ and, after absorbing all these messages,
  the resulting $\Sigma'$ is still consistent.
 

\begin{DEF}[Conformance to a Configuration]\label{def:qconform} \rm
  Assume a network 
  $\dN\Cong \sN \mid \GQ{\RInfo}{\GQueue}$ is given.
  Define
  $\dN$ conforms to $\Sigma;\GQ{\RInfo}{\GQueue}$ when:

  \begin{enumerate}
  \item $\GQueue$ is empty, 
   $\models \NLNWgood{\sN}{\Sigma}$ and 
   $\Sigma$ is consistent, or 

   \item $\GQueue$ is not empty, and the following conditions hold
   \begin{enumerate}
   \item $\models \NLNWgood{\sN}{\Sigma}$, 
   \item all messages in $\GQueue$ are receivable to $\sN$, and  
   \item as $\Sigma; \GQ{\RInfo}{\GQueue} 
      \GTRANS{{\ell_1}  \ldots {\ell_n}} 
      \Sigma' ; \GQ{\RInfo'}{\emptyset}$ so that
      $\sN \mid {\GQueue} \GTRANS{\ell_1  \ldots \ell_n}
      \sN' \mid {\emptyset}$ 
    where each $\ell_i, i=\ASET{1, ..., n}$ is an input,  
    $\Sigma'$ is consistent.
   \end{enumerate}
\end{enumerate}
\end{DEF}


\begin{comment}
Corresponding to the transitions of configurations,
we re-state the network transition as global transition, 
$\GTRANS{\ell}$,
which directly describes the overall behaviours of local processes in network, 
as a dual to the the LTS defined in Figure \ref{fig:NW:LTS}.

\begin{DEF}[Global network transition] \label{def:gtrans}\rm 
  Let $\dN \Cong \sN | \GQ{\RInfo}{\emptyset}$ 
  conform to $\Sigma$.  
  \TZC{
  If $\dN \TAUTRANS{\hat{\ell_1}} \cdots \dN_{n-1} \TAUTRANS{\hat{\ell_n}} \dN_n$,
  then the transitions (some of the actions can be $\tauact$)
  have the corresponding transitions from configuration transition which is written
  $\Sigma;\GQ{\RInfo}{\emptyset} 
  \GTRANS{\dual(\ell_1)}\cdots \GTRANS{\dual(\ell_n)}
  \Sigma_n;\GQ{\RInfo'}{\GQueue'}$.
Then we write
$\dN
\GTRANS{\dual(\ell_1)}
\cdots
\dN_{n-1}
\GTRANS{\dual(\ell_n)}
\dN_n$,
}
which we call {\em global transitions}.
\end{DEF}
\end{comment}

The following session fidelity theorem states: 
assume network $\dN \Cong \sN|\GQ{\RInfo}{\emptyset}$ is given, 
suppose that $\sN$ satisfies
$\Sigma$.  If $\Sigma$ is consistent and if $\RInfo=\ri{\Sigma}$, then
we say $\dN$ {\em conforms to} $\Sigma$.  
If this holds, then, with the messages which $\dN$ exchanges follow the specification, 
the dynamics of the network witnesses the validity of specifications.
{\em In the follows, we always assume $\Sigma$ is consistent, unless
  otherwise stated.}
%The proof of Theorem~\ref{thm:fidelity} (session fidelity) is straightforward from Lemma \ref{lem:routing}. 
%We prove Lemma \ref{lem:routing} below after a few auxiliary definitions and lemmas.

\begin{THM}[Session Fidelity] \label{lem:routing} \rm
Assume configuration
$\Sigma; \GQ{\RInfo}{\GQueue}$
is configurationally consistent.
and network $\dN\Cong \sN|\GQ{\RInfo}{\GQueue}$
%$\dN\Cong (\nu\ n)(\sN_0|\GQ{\RInfo}{\GQueue})$ 
conforms to configuration
$\Sigma; \GQ{\RInfo}{\GQueue}$.
%% , which is configurationally consistent.
Then for any $\ell$, whenever we have 
$\dN \GTRANS{\ell} \dN'$ s.t.
$\Sigma; \GQ{\RInfo}{\GQueue} \GTRANS{\ell} \Sigma'; \GQ{\RInfo'}{\GQueue'}$,
it holds that 
$\Sigma'; \GQ{\RInfo'}{\GQueue'}$ is configurationally consistent and
that $\dN'$ conforms to $\Sigma'; \GQ{\RInfo'}{\GQueue'}$.
\end{THM}

Before proving the property of session fidelity, 
we first prove the following lemmas.
\paragraph*{\textbf{Lemma} \ref{lem:emptyreceive}} \rm
Assume a network $\dN \Cong \sN|\GQ{\RInfo}{\GQueue}$ 
conforming to $\Sigma; \GQ{\RInfo}{\GQueue}$
which is configurationally consistent,
if $\dN \GTRANS{\ell} \dN'$ such that
$\ell$ is an output and
$\Sigma;\GQ{\RInfo}{\GQueue} \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue \CD m}$
%$\dN' \Cong \sN' | \GQ{\RInfo}{\GQueue \CD m}$,
then $\GQueue\cdot m$ is receivable to $\Sigma'$.
\begin{proof}
When $\ell = \AOUTfree{a}{s}{\rr}{\LA}$,
since $\Sigma$ is consistent, by Definitions \ref{def:consistent}, 
there exists $a:\Imode(T[\rr])$ in some $\Gamma$ of $\Sigma$.
Because $\ell$ does not affect the existence of $a:\Imode(T[\rr])$,
it remains in $\Gamma$ of $\Sigma'$, thus
invitation $m = \AOUTfree{a}{s}{\rr}{\LA}$ is receivable to $\Sigma'$.

Let $\alpha_i = \ENCan{\Gamma_i, \Delta_i}$.
When $\ell = s[\rr_1, \rr_2] ! l_j \ENCan{v}$, 
by Definitions \ref{def:consistent} and \ref{def:qconform},
since $\models \NLNWgood{\sN}{\Sigma}$
and $\Sigma$ is consistent, 
$\exists \alpha_s, \alpha_r \in \Sigma$, 
$\exists \GA$ is well-formed and $s$ obeys to $\GA$,
$$\GA = \GInter{\rr_1}{\rr_2}: \ASET{l_i(x_i: \sort_i)\ASET{A_i}. \GA_i}_{i \in I}$$
such that
\begin{eqnarray} \label{eqglobalproject}
\Delta_s(s[\rr_1]) &=& 
\Proj{\GA}{\rr_1} = 
\rr_2 ! \ASET{l_i(x_i: \sort_i)\ASET{A_i}. \Proj{\GA_i}{\rr_1}}_{i \in I} \nonumber
\\
\Delta_r(s[\rr_2]) &=& 
\Proj{\GA}{\rr_2} =
\rr_1 ? \ASET{l_i(x_i: \sort_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}
\end{eqnarray}
As action $s[\rr_1, \rr_2] ! l_j \ENCan{v}$ fires,
Equation \ref{eqglobalproject} changes to 
\begin{eqnarray} \label{eqglobalprojectfires}
\Delta_s(s[\rr_1]) &=& 
\Proj{\GA_j}{\rr_1} \nonumber
\\
\Delta_r(s[\rr_2]) &=& 
\Proj{\GA}{\rr_2} =
\rr_1 ? \ASET{l_i(x_i: \sort_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}
\end{eqnarray}
the receiving capability of $\rr_1?$ still remains in $\Delta_r(s[\rr_2])$, 
where $\alpha_r \in \Sigma'$, 
thus $m = s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$ is receivable to $\Sigma'$.
\end{proof} 
%%%%%%%%%%%%%%%%%
As $\dN \equiv \sN \mid H$ and
$\models \NLNWgood{\sN}{\Sigma}$,
the satisfaction relation of $\sN$ and $\Sigma$ remains
whenever action takes place.
\paragraph*{\textbf{Lemma} \ref{lem:nsatisfiesM}} \rm
Assume $\dN \equiv \sN \mid H$ and $\models \NLNWgood{\sN}{\Sigma}$.
If $\dN \GTRANS{\ell} \dN' \equiv \sN' \mid H'$ 
and $\Sigma \TRANS{\ell} \Sigma'$,
then $\models \NLNWgood{\sN'}{\Sigma'}$.

\begin{proof}
Directly from Definition \ref{def:partial}.
\end{proof}
Now we prove session fidelity:
\begin{proof}
Assume $\dN$ conforms to $\Sigma; \GQ{\RInfo}{\GQueue}$, which
is configurationally consistent.  We prove the statement by
inspection of each case. 
\begin{description}
\item[(Sel)] Let $\ell=s[\rr_1, \rr_2] ! l_j \ENCan{v}$, $\dN
  \GTRANS{\ell} \dN'$ and $\Sigma; \GQ{\ri{\Sigma}}{\GQueue}
  \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue \CD m}$,
  where $m = s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$.\\
         
         Then $\RInfo=\ri{\Sigma}=\ri{\Sigma'}$ 
         because there is no change to the elements in $\Sigma$ or to the routing table.
        \\
        
         Since $\Sigma$ allows $\ell$,  and $\Sigma$ is consistent, 
         $\exists \alpha_r, \alpha_s \in \Sigma$, $\exists \GA$ is well-formed, 
         $$\GA= \GInter{\rr_1}{\rr_2} \ASET{l_i (x_i: {S}_i) \ASET{A_i}. \GA_i}_{i \in I},$$
         such that 
         \[
         \begin{array}{rcl}
         \Delta_s(s[\rr_1]) &=&
         \Proj{\GA}{\rr_1} = 
         \rr_2! \ASET{l_i (x_i: {S}_i)\ASET{A_i}. \Proj{\GA_i}{\rr_1}}_{i \in I},\\
           
         \Delta_r(s[\rr_2])&=&
         \Proj{\GA}{\rr_2} = 
         \rr_1? \ASET{l_i (x_i: {S}_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}.
         \end{array}
         \] 
         $\Sigma \TRANS{\ell} \Sigma'$ implies
         $\Sigma'$ has 
         \[
         \begin{array}{rcl}
         s[\rr_1]&=&\Proj{\GA_j}{\rr_1},\\ 
         s[\rr_2]&=& \rr_1? \ASET{l_i (x_i: {S}_i)\ASET{A'_i}. \Proj{\GA_i}{\rr_2}}_{i \in I}.
         \end{array}
         \]
  
         Consider (case 1: $\GQueue$ is empty).
         By Lemma \ref{lem:emptyreceive}, 
         after receiving $m$, say $\Sigma' \TRANS{\ell}\Sigma''$, 
         $\Sigma''$ has $s[\rr_1]=\Proj{\GA_j}{\rr_1}$ and
         $s[\rr_2]=\Proj{\GA_j}{\rr_2}$, $\Sigma''$ is thus
         consistent by Definition \ref{def:consistent}.  By Definition
         \ref{def:congconsist}, $\Sigma'; \GQ{\RInfo}{m}$ is
         configurationally consistent, and $\models \NLNWgood{\sN'}{\Sigma'}$
         by Lemma \ref{lem:nsatisfiesM}, 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue \CD m}$. \\
 
         Consider (case 2: $\GQueue$ is not empty). 
         Since $\Sigma; \GQ{\RInfo}{\GQueue}$ is configurationally consistent, 
         again, by Lemma \ref{lem:emptyreceive}, 
%          $\GQueue$ and $m$ are receivable to $\dN'$;
         after receiving messages in $\GQueue$ (but not $m$),
         say $\Sigma' \TRANS{\ell_0  \ldots \ell_n} \Sigma'_1$,
         where every action in $\ell_0  \ldots \ell_n$ 
         corresponds to each message in $\GQueue$,
         we have $\Sigma'_1; \GQ{\RInfo'}{m}$ is configurationally consistent.
         After $\Sigma'_1$ receives $m$, say 
         $\Sigma'_1 \TRANS{s[\p_1, \p_2]? l \ENCan{v}} \Sigma''$,
         where $s[\p_1, \p_2]? l \ENCan{v}$ is dual to $\ell$,
         with the same reasoning above, $\Sigma''$ has 
         $s[\rr_1]=\Proj{\GA'_j}{\rr_1}$ and $s[\rr_2]=\Proj{\GA'_j}{\rr_2}$,         
         so that $\Sigma''$ is consistent.
         By Definition \ref{def:congconsist}, 
         $\Sigma'; \GQ{\RInfo}{\GQueue \CD m}$ is configurationally consistent,
         and $\models \NLNWgood{\sN'}{\Sigma'}$ by Lemma \ref{lem:nsatisfiesM}, 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue \CD m}$. \\
         
 \item[(Bra)] Let $\ell=s[\rr_1, \rr_2] ? l_j \ENCan{v}$,
  $\dN \GTRANS{\ell} \dN'$ and 
  $\dN$ conforms to $\Sigma; \GQ{\ri{\Sigma}}{\GQueue}$.\\
  
         Consider (case 1: $\GQueue$ is empty). Since 
         $\Sigma; \GQ{\ri{\Sigma}}{\emptyset} \not \GTRANS{\ell}$, 
         so this case never happens.\\
         
         Consider (case 2: $\GQueue$ is not empty).
         When $\GQueue$ is not empty. 
         $\dN \GTRANS{\ell} \dN'$ and 
         $$\Sigma; \GQ{\ri{\Sigma}}{\GQueue} 
         \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue / m},$$
         where $\GQueue / m $ means taking off message $m$ from $\GQueue$, where
         $m = s \ENCan{\rr_1, \rr_2, l_j \ENCan{v}}$ \\
         
         We have $\RInfo=\ri{\Sigma}=\ri{\Sigma'}$ 
         because there is no change to the elements in $\Sigma$ or to the routing table.
         By Definition \ref{def:congconsist}, 
         after receiving all messages in $H$, $\Sigma$
         is consistent, thus $\Sigma'$, which has received message $m$
         is consistent after receiving all messages in $\GQueue / m$.  
         By Lemma \ref{lem:nsatisfiesM}, 
         we have $\models \NLNWgood{\sN'}{\Sigma'}$ 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue / m}$. \\
         
       \item[(Req)] Let $\ell=\AOUTPUT{a}{s}{\rr}{T}$.  
         $\dN\GTRANS{\ell} \dN'$ and 
         $$\Sigma; \GQ{\ri{\Sigma}}{\GQueue}
         \GTRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue \CD m},$$
         where $m=\AOUTPUT{a}{s}{\rr}{T}$.
         Then $\RInfo=\ri{\Sigma}=\ri{\Sigma'}$
         because, by Definition \ref{def:routing}, nothing new is registered to the routing table.\\
        
         Since $\Sigma$ allows $\ell$ and $\Sigma$ is consistent,  
         by Definition \ref{def:consistent},
         $\exists \Gamma_i, \Gamma_j \in \Sigma$ such that
         $a: \Imode(T[\rr]) \in \Gamma_i$ and $a:\Omode(T[\rr]) \in \Gamma_j$.
         After $\Sigma \TRANS{\ell} \Sigma'$,
         by rule \mrule{Req} in the LTS of specifications, 
         %Figure \ref{fig:LTS:spec:unlinked},
         %$a: T[\rr]^\capabilities \in \Gamma_i$ with $\mathtt{I} \in \capabilities$
         $a: \Imode(T[\rr])$ remains in $\Gamma'_i$,
         $a: \Omode(T[\rr])$ remains in $\Gamma'_j$,
         and thus they both remain in $\Sigma'$.\\
            
         Consider (case 1: $\GQueue$ is empty): By Lemma
         \ref{lem:emptyreceive},
          %  and also by [2] above, $m$ will be received by $\dN'$.  After
         after receiving $m$, 
         say $\Sigma' \TRANS{\AIN{a}{s}{\rr}{T}} \Sigma''$, 
         %$a:T[\rr]^\capabilities$ with $\mathtt{I} \in \capabilities$ 
         both $a:\Imode(T[\rr])$ and $a:\Omode(T[\rr])$ remain in $\Sigma''$, 
         satisfying Definition \ref{def:consistent},
         so that $\Sigma'; \GQ{\RInfo}{m}$ is configurationally consistent.
         By Lemma \ref{lem:nsatisfiesM}, we have $\models \NLNWgood{\sN'}{\Sigma'}$, 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue \CD m}$. \\

         Consider (case 2: $\GQueue$ is not empty).
         The proof is similar to the one in (Sel) and ommitted.
%          Since $\Sigma; \GQ{\ri{\Sigma}}{\GQueue}$ is configurationally consistent, 
%          by Lemma \ref{lem:emptyreceive}, 
%          $\GQueue$ and $m$ are receivable to $\dN'$,
%          and after receiving messages in $\GQueue$ (not yet $m$),
%          saying $\Sigma' \TRANS{\ell_0 \CD ... \CD \ell_n} \Sigma'_0$,
%          where $\forall \ell \in \ell_0 \CD ... \CD \ell_n$ 
%          corresponds to each message in $\GQueue$,
%          we have $\Sigma'_0; \GQ{\RInfo'}{m}$ is configurationally consistent.
%          After $\Sigma'_0$ receives $m$, saying $\Sigma'_0 \TRANS{\dual{\ell}} \Sigma''$,
%          with the same reasoning in [2], $\Sigma''$ has 
%          $a:T[\rr]^\capabilities$ with $\mathtt{I} \in \capabilities$,         
%          so that $\Sigma''$ is consistent.
%          By Definition \ref{def:congconsist}, $\Sigma'; \GQ{\RInfo}{\GQueue \CD m}$ is configurationally consistent
%          and $\dN'$ conforms to it.
         \\ 
         
 \item[(Acc)] Let $\ell=\AIN{a}{s}{\rr}{T}$.\\

         Consider (case 1: $\GQueue$ is empty). Since 
         $\Sigma; \GQ{\ri{\Sigma}}{\emptyset} \not \GTRANS{\ell}$, 
         this case never happens.\\
         
         Consider (case 2: $\GQueue$ is not empty). 
         If $\dN \GTRANS{\ell} \dN'$ and 
         $$\Sigma; \GQ{\ri{\Sigma}}{\GQueue} 
         \GTRANS{\ell} \Sigma'; \GQ{\RInfo'}{\GQueue / m},$$
         where $m=\AOUTPUT{a}{s}{\rr}{T}$.
         %%%%%%%
         Since there exists $\Delta \in \Sigma'$ s.t. $s[\rr] \in \Delta$,
         by Definition \ref{def:routing},
         $\RInfo'=\ri{\Sigma}, s[\rr] \mapsto \alpha=\ri{\Sigma'}.$\\
         
         For the same reasoning in (Bra), we have $\Sigma';
         \GQ{\RInfo}{\GQueue / m}$ is configurationally consistent.
         By Lemma \ref{lem:nsatisfiesM}, 
         we have $\models \NLNWgood{\sN'}{\Sigma'}$ 
         thus $\dN'$ conforms to $\Sigma' ; \GQ{\RInfo}{\GQueue / m}$. \\
        
\end{description}
The proof for other cases are trivial.% $\blacksquare$
%Except actions $\newsLabel{\LA_i}{s}{\rr_j}{a_i}_{i\in I}$ and $\tau$,
%which do not change the network and its corresponding configuration,
%action $\ell=\newaLabel{a}{\LA}{\rr}$
%only changes the routing table by
%$\Sigma; \GQ{\ri{\Sigma}}{\GQueue} \TRANS{\ell} \Sigma'; \GQ{\RInfo}{\GQueue}$.
%The new routing table $\RInfo$=$\ri{\Sigma}, a \mapsto \alpha=\ri{\Sigma'}$ 
%because $\Sigma'$ enables the input ($\mathtt{I}$) capability of channel $a$ in $\alpha$ 
%by $a:\INTp{T[\rr]}^{\capabilities\uplus \mathtt{I}}$.
\end{proof}


%\paragraph*{\textbf{Proposition \ref{pro:fidelity}}}[\textbf{Session fidelity}] \rm
%%\begin{PRO}[Session Fidelity]
%  \rm Assume given a network $\dN$ conforming to a consistent $\Sigma$,
% s.t., $\dN\TRANS{\ell_1}\cdots\TRANS{\ell_n}\dN'$ and, correspondingly, 
% $\Sigma;\GQ{\ri{\Sigma}}{\emptyset} \TRANS{\ell_1}\cdots\TRANS{\ell_n} \Sigma';
% \GQ{\RInfo}{\GQueue}$.  Then the configuration $\Sigma';
% \GQ{\RInfo}{\GQueue}$ is configurationally consistent.
%%\end{PRO}

%\begin{proof}
%It is straightforward from Lemma \ref{lem:routing}. $\blacksquare$
%\end{proof}
